/**
 * $Id: ServerSettings.java$
 *
 * Copyright (C) 2011, INRIA
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

package fr.inrialpes.exmo.rdfserver;

import java.net.URL;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemLongClickListener;

public class ServerSettings extends ListActivity {
	private final int PORT_DIALOG = 1;
	private final int ABOUT_DIALOG = 2;
	private final int DOMAIN_DIALOG = 3;
	private final int LANGUAGE_DIALOG = 4;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
			
		ArrayList<Item> items = new ArrayList<Item>();							//create the array of items
		items.add(new Item(getResources().getString(R.string.port_item_id_txt), getResources().getString(R.string.port_item_txt)));
		items.add(new Item(getResources().getString(R.string.about_item_id_txt), getResources().getString(R.string.about_item_txt)));
		items.add(new Item(getResources().getString(R.string.domain_item_id_txt), getResources().getString(R.string.domain_item_txt)));
		items.add(new Item(getResources().getString(R.string.language_item_id_txt), getResources().getString(R.string.language_item_txt)));
		ItemsAdapter itemsAdapter = new ItemsAdapter(this, items);
		setListAdapter(itemsAdapter);
		
		ListView list = getListView();
		list.setOnItemLongClickListener(new OnItemLongClickListener() {
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) { 
				Toast.makeText(ServerSettings.this, "Item in position " + position + " clicked", Toast.LENGTH_LONG). show();
				return true;
			}
		});
		
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Object o = this.getListAdapter().getItem(position);
		Item item = new Item();
		
		if(o instanceof Item) { 
			item = (Item)o;
			String itemId = item.getItemId();
			Toast.makeText(this, "Selected: " + item.getItemId(), Toast.LENGTH_LONG).show();
			
			if(itemId.equals(getResources().getString(R.string.port_item_id_txt)))
				showDialog(PORT_DIALOG);
			if(itemId.equals(getResources().getString(R.string.about_item_id_txt)))
				showDialog(ABOUT_DIALOG);
			if(itemId.equals(getResources().getString(R.string.domain_item_id_txt)))
				showDialog(DOMAIN_DIALOG);
			if(itemId.equals(getResources().getString(R.string.language_item_id_txt)))
				showDialog(LANGUAGE_DIALOG);
		}
		else { 	
			String keyword = o.toString();
			Toast.makeText(this, "Selected: " + keyword, Toast.LENGTH_LONG).show();
		}
		/*
		String item = (String) getListAdapter().getItem(position);
		Toast.makeText(this, item + " selected", Toast.LENGTH_LONG).show();
		*/
	}
    
    protected Dialog onCreateDialog(int id) {
    	Dialog dialog = null;
    	
	    switch(id) {
	    case PORT_DIALOG :
	    	AlertDialog.Builder portAlertBox = new AlertDialog.Builder(this);
	    	portAlertBox.setTitle(getResources().getString(R.string.port_item_id_txt));
	    	final EditText portEditText = new EditText(this);
	    	portAlertBox.setView(portEditText);
	    	
	    	portAlertBox.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
				@Override
				public void onClick(DialogInterface dialog, int which) {
					int port = 8080;
					String portNumber = portEditText.getText().toString();										//get the value of the port number

					if((portNumber == null) || (portNumber.equals("")))											//default port
						port = 8080;
					else {
						try {
							 port = Integer.parseInt(portNumber);
						} catch (NumberFormatException nfe) {
							 port = 8080;
						}
					}
					//send the preference to the HTTP server
					SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
				    Editor editor = prefs.edit();
				    editor.putInt(getResources().getString(R.string.prefs_portNumber), port);
				    editor.commit();
					Toast.makeText(getApplicationContext(), "OK button pressed", Toast.LENGTH_LONG).show();
				}
			});
	    	portAlertBox.setNeutralButton("Cancel", new DialogInterface.OnClickListener() { 
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Toast.makeText(getApplicationContext(), "OK button pressed", Toast.LENGTH_LONG).show();
				}
			});
	    	//portAlertBox.show();
	    	dialog = portAlertBox.create();
			break;
	    case DOMAIN_DIALOG:
	    	AlertDialog.Builder domainAlertBox = new AlertDialog.Builder(this);
	    	domainAlertBox.setTitle(getResources().getString(R.string.domain_item_id_txt));
	    	final EditText domainEditText = new EditText(this);
	    	domainAlertBox.setView(domainEditText);
	    	
	    	domainAlertBox.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
				@Override
				public void onClick(DialogInterface dialog, int which) {
					String domainName = domainEditText.getText().toString();									//get the value of the domain name
					if((domainName == null) || (domainName.equals("")))
						domainName = getResources().getString( R.string.domanin_name_txt );
					//send the preference to the HTTP server
					SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
					try {
						URL u = new URL(domainName); 															// this would check for the protocol
						u.toURI(); 																				// does the extra checking required for validation of URI 
					} catch(Exception e) {
						domainName = getResources().getString(R.string.domanin_name_txt);						//set the domain name to the default one
					}
				    Editor editor = prefs.edit();
				    editor.putString(getResources().getString(R.string.prefs_domainName), domainName);
				    editor.commit();
					Toast.makeText(getApplicationContext(), "OK button pressed", Toast.LENGTH_LONG).show();
				}
			});
	    	domainAlertBox.setNeutralButton("Cancel", new DialogInterface.OnClickListener() { 
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Toast.makeText(getApplicationContext(), "OK button pressed", Toast.LENGTH_LONG).show();
				}
			});
	    	dialog = domainAlertBox.create();
	    	break;
	    case ABOUT_DIALOG:
	    	String version = "Non recoverable version";
			try {
			    version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			} catch ( NameNotFoundException nnfex ) {
			    nnfex.printStackTrace();
			}
	    	AlertDialog.Builder aboutAlertBox = new AlertDialog.Builder(this);
	    	aboutAlertBox.setTitle(getResources().getString(R.string.about_item_id_txt));
	    	aboutAlertBox.setMessage(getString(R.string.about_txt).replace( "@@VERS@@", version ));
	    	aboutAlertBox.setPositiveButton("OK", null);	    	
	    	dialog = aboutAlertBox.create();
	    	break;
	    case LANGUAGE_DIALOG:
	    	AlertDialog.Builder languageAlertBox = new AlertDialog.Builder(this);
	    	languageAlertBox.setTitle(getResources().getString(R.string.language_item_id_txt));
	    	final EditText languageEditText = new EditText(this);
	    	languageAlertBox.setView(languageEditText);
	    	
	    	languageAlertBox.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
				@Override
				public void onClick(DialogInterface dialog, int which) {
					String languageName = languageEditText.getText().toString();
					if((languageName == null) || (languageName.equals("")))
						languageName = getResources().getString(R.string.language_item_id_txt);
					
					SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
					Editor editor = prefs.edit();
					editor.putString(getResources().getString(R.string.prefs_languageName), languageName);
					editor.commit();
					Toast.makeText(getApplicationContext(), "OK button pressed", Toast.LENGTH_LONG).show();
				}
			});
	    	languageAlertBox.setNeutralButton("Cancel", new DialogInterface.OnClickListener() { 
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Toast.makeText(getApplicationContext(), "OK button pressed", Toast.LENGTH_LONG).show();
				}
			});
	    	//portAlertBox.show();
	    	dialog = languageAlertBox.create();
	    	break;
	    default:
	        dialog = null;
	    }
	    return dialog;
	}
    
    
    public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_about:
				showDialog(ABOUT_DIALOG);
				break;
			case R.id.menu_quit:
				this.finish();
				break;
			default: 
		}
		return super.onOptionsItemSelected(item);
	}
    
}