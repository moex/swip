/**
 * $Id: MyHttpHandler.java$
 *
 * Copyright (C) 2011, INRIA
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

package fr.inrialpes.exmo.rdfserver;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Locale;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.MethodNotSupportedException;
import org.apache.http.RequestLine;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;

import fr.inrialpes.exmo.rdfprovider.RdfContentResolverProxy;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;


/*
 * The handler will manage all the connections
 */
public class MyHttpHandler implements HttpRequestHandler {
	private byte[] fileContent;
	private RdfContentResolverProxy resolver; 
	//private String target = null;
	private Context ctx = null;
	
	//The constructor
	public MyHttpHandler(RdfContentResolverProxy resolver, Context ctx) { 
		this.resolver = resolver;
		this.ctx = ctx;
	}
	
	//Handle the request
	public void handle(HttpRequest request, 
					   HttpResponse response,
					   HttpContext context) throws HttpException, IOException {
		String method = null;
		String uri = null;
		
		RequestLine requestLine = request.getRequestLine();									//the request line
		method = requestLine.getMethod().toUpperCase(Locale.ENGLISH);						//the method name
		//HttpEntity requestEntity = null;													//the request entity
		
		if(!method.equals("GET") && !method.equals("HEAD") && !method.equals("POST")) {		//method not supported
			throw new MethodNotSupportedException(method + " method not supported" );	
		}
		
		/*if(request instanceof HttpEntityEnclosingRequest) 									//enclosing request - HTTP specification defines two entity enclosing methods: POST and PUT 
			requestEntity = ((HttpEntityEnclosingRequest) request).getEntity();
		*/
		
		
		//headers[0].
		String path = uri=request.getRequestLine().getUri();	    							//process the request		
		uri = MyHttpHandler.internalizeURI(path);												//internalize the URI
		
		Log.d("URI", uri);
		
		if (uri == null)																	//the URI is not valid or cannot be parsed
			sendNotFoundResponse(response);
		else {
			Log.d("URI ", uri.toString());
			
			String mimeType = getRequestedFormat(request);
			fileContent = resolver.getRdf(Uri.parse(uri), getDomainName(),  getJENAFormat(mimeType) /*getLanguageName()*/);//retrieve the content of the file
			if (fileContent == null)														//404 - Not Found
				sendNotFoundResponse(response);
			else 																			//OK result
				sendOkResponse(response,mimeType);
		}
	}
	
	/**
	 * give the preferred according to the accept: header
	 * "text/plain" by default
	 * @param request
	 * @return
	 */
	private static String getRequestedFormat(HttpRequest request) {
		Header[] headers = request.getHeaders("Accept");
		for (Header h : headers) {
			String[] values = h.getValue().split(" *; *");
			for (String v : values) {
				if (v.matches("application/.*")) {
					return "application/rdf+xml";
				}
				else if (v.matches("text/.*")) {
					return "text/plain";
				}
			}
		}
		return "text/plain";
	}
	
	/**
	 * Perhaps put it in resolver because it is JENA dependent....
	 * @param mimeType
	 * @return
	 */
	private static String getJENAFormat(String mimeType) {
		if ("application/rdf+xml".equals(mimeType)) {
			return "RDF/XML";
		}
		return "RDF/XML";
	}
	
	
	
	//send the requested file as a response
	public void sendOkResponse(HttpResponse response, String mimeType) { 
		response.setStatusCode(HttpStatus.SC_OK);
		response.addHeader("Content-Type", mimeType);
		ByteArrayEntity body = new ByteArrayEntity(fileContent);
        response.setEntity(body);
	}
	
	//send file not found response
	public void sendNotFoundResponse(HttpResponse response) {
		response.setStatusCode(HttpStatus.SC_NOT_FOUND);
		response.addHeader("Content-Type", "text/plain");
		HttpEntity body = null;
		try {
			body = new StringEntity(ctx.getResources().getString(R.string.http_error_txt), "UTF-8");
		} catch(UnsupportedEncodingException ex) {	/*ex.printStackTrace();*/	}
		response.setEntity(body);
	}
	
	private String getDomainName() { 
		SharedPreferences prefs = ctx.getSharedPreferences("X", 0);
		String domainName = prefs.getString(ctx.getResources().getString(R.string.prefs_domainName), ctx.getResources().getString(R.string.domanin_name_txt));
		return domainName;
	}
	
	private String getLanguageName() {
		SharedPreferences prefs = ctx.getSharedPreferences("X", 0);
		String languageName = prefs.getString(ctx.getResources().getString(R.string.prefs_languageName), "RDF/XML");
		return languageName;
	}
	
	//Translate the external URI into an internal one
	/**
	 * parse an internal uri from the path information of the requested URL
	 * format /authority/path -> content://authority/path
	 */
	private static String internalizeURI(String path) {
		return "content://"+path.substring(1);
		/*boolean found = false;
		String aux = null;
		
		if(target != null) {
			aux = new String(target);
			if(aux.contains("/content/")) {
				aux = aux.replaceAll("/content/", "content://");
				found = true;
			}
			if(aux.contains("com/")) {
				aux = aux.replaceAll("com/", "com.");
				found = true;
			}
			if(aux.contains("android/")) {
				aux = aux.replaceAll("android/", "android.");
				found = true;
			}
		}
		
		if(found)
			return aux;
		else
			return null;*/
	}
	
	// Print the content of the retrieved file -> only for debug
    public void printRetrievedFile(Uri uri) { 
    	fileContent = resolver.getRdf(uri, getDomainName(),  getLanguageName());
    	if (fileContent == null)
    		; //Log.d("MYHTTP HANDLER", "ERROR: the content of the file is null");
    	else { 
	        BufferedReader reader = new BufferedReader(new InputStreamReader (new ByteArrayInputStream(fileContent))); //print the byte array
	        String s=null;
	        try {
				while ((s=reader.readLine())!=null) {
					System.out.println(  s);
				}
			} catch (IOException e) {					
				e.printStackTrace();
			} 
    	}	//return fileContent;
    }
    
}	//end MyHttpHandler