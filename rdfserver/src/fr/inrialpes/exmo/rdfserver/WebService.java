/**
 * $Id: WebService.java$
 *
 * Copyright (C) 2011, INRIA
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

package fr.inrialpes.exmo.rdfserver;

import fr.inrialpes.exmo.rdfprovider.RdfContentResolverProxy;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class WebService extends Service {
	private HttpServer httpServer;														//instance of the HttpServer
	//private Context context = this.getApplicationContext();
	//private Thread serverThread;														//the server thread
	private RdfContentResolverProxy resolver;
		
	@Override
	public void onCreate() {
		super.onCreate();
		resolver = RdfContentResolverProxy.getInstance(this);
		httpServer = new HttpServer(resolver, this);									//create the HttpServer
	}
	
	@Override
	public boolean onUnbind(Intent intent) {
		return super.onUnbind(intent);
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		httpServer.startServer();														//start the httpServer
		/*
		serverThread = new Thread(httpServer);
		serverThread.start();															//start the httpServer
		*/
		
		Log.d("SERVER STARTED", "server started");
		
		return Service.START_STICKY;
	}
	
	@Override
	public void onDestroy() {
		httpServer.stopServer();														//stop the server
		//serverThread.stop();															//DANGEROUS!!! Do not use it!
		Log.d("SERVER STARTED", "server started");
		super.onDestroy();
	}
	
}

