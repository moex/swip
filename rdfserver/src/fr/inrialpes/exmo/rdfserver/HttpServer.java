/**
 * $Id: HttpServer.java$
 *
 * Copyright (C) 2011, INRIA
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

package fr.inrialpes.exmo.rdfserver;

import java.net.ServerSocket;
import java.net.Socket;

import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.impl.DefaultHttpResponseFactory;
import org.apache.http.impl.DefaultHttpServerConnection;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.BasicHttpProcessor;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandlerRegistry;
import org.apache.http.protocol.HttpService;
import org.apache.http.protocol.ResponseConnControl;
import org.apache.http.protocol.ResponseContent;
import org.apache.http.protocol.ResponseDate;
import org.apache.http.protocol.ResponseServer;

import android.content.Context;
import android.content.SharedPreferences;

import fr.inrialpes.exmo.rdfprovider.RdfContentResolverProxy;

//The web server
public class HttpServer extends Thread {
	private ServerSocket serverSocket;
	private BasicHttpProcessor httpproc;	
	private HttpContext context;
    private HttpService httpService;
    private HttpParams params;
    private HttpRequestHandlerRegistry registry;
    byte[] fileContent;
    private boolean isRunning;			/* You cannot forcibly stop a thread using Thread.stop() because it is not safe. Therefore we keep track of a variable called isRunning to know 
    									   when the thread is running, or when the thread is stopped */
    
    private Context ctx;
 
    public HttpServer(RdfContentResolverProxy resolver, Context ctx) {
    	this.ctx = ctx;
    	
    	isRunning = false;																//the server is not running
    		
		params = new BasicHttpParams();													//set the parameters i.e. define the runtime behavior of a component
		params.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, 60000);					//timeout for waiting for data or, put differently, a maximum period inactivity between two consecutive data packets
		params.setIntParameter(CoreConnectionPNames.SOCKET_BUFFER_SIZE, 8*1024);		//size of the internal socket buffer used to buffer data while receiving / transmitting HTTP messages
		params.setBooleanParameter(CoreConnectionPNames.STALE_CONNECTION_CHECK, false);	//stale connection check is to be used
		params.setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);				//use Nagle's algorithm to optimize small packets problem
		params.setParameter(CoreProtocolPNames.ORIGIN_SERVER, "HttpComponents/1.1");
		
		httpproc = new BasicHttpProcessor();											//set up HTTP protocol processor
		httpproc.addInterceptor(new ResponseDate());									//adding day header to the outgoing response
		httpproc.addInterceptor(new ResponseServer());									//adding server header
		httpproc.addInterceptor(new ResponseContent());									//delimiting content length
		httpproc.addInterceptor(new ResponseConnControl());								//adding Connection header - essential for managing persistence of HTTP/1.0
		
		registry = new HttpRequestHandlerRegistry();									//set up request handlers
		registry.register("*", new MyHttpHandler(resolver, ctx));						//
		
		httpService = new HttpService(httpproc, new DefaultConnectionReuseStrategy(), new DefaultHttpResponseFactory());  //create httpService to handle requests for active HTTP connections
		httpService.setParams(params);
		httpService.setHandlerResolver(registry);
    }
    
	@Override
	public void run() {
		super.run();
		try {
			serverSocket = new ServerSocket(getPortNumber());
			serverSocket.setReuseAddress(true);
			
			while(isRunning) {
				try {
					final Socket client = serverSocket.accept();
					if(isRunning) {
						DefaultHttpServerConnection conn = new DefaultHttpServerConnection();
						conn.bind(client, params);
						context = new BasicHttpContext();
						
						httpService.handleRequest(conn, context);
						conn.shutdown();
					}
					client.close();
				} catch(Exception ex) { 
					ex.printStackTrace();
				}
			}
			serverSocket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	} 
	
	public int getPortNumber() { 
		SharedPreferences prefs = ctx.getSharedPreferences("X", 0);
        int port = prefs.getInt(ctx.getResources().getString(R.string.prefs_portNumber), 8080);
        return port;
    }
	
	// Start the server thread
	public synchronized void startServer() {
		isRunning = true;
		super.start();
	}
	
	// Stop the server thread
	public synchronized void stopServer() {
		isRunning = false;
	}
	
}