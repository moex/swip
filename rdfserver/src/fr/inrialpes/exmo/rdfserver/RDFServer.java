/**
* $Id: RDFServer.java$
*
* Copyright (C) 2011, INRIA
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 2.1 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
* USA.
*/

package fr.inrialpes.exmo.rdfserver;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.view.View.OnClickListener;
import android.view.ViewStub.OnInflateListener;
import android.widget.Button;

public class RDFServer extends Activity implements OnClickListener, OnInflateListener {
	private final int DIALOG_ABOUT = 1;
	private Button startServer=null;															//the start server button
	private Button stopServer=null;																//the stop server button
	private Button settingsButton = null;														//the settings button
	private Button aboutButton = null;															//the about button
	private boolean isStarted = false;															//the server is started or not
	byte[] fileContent = null;																	//the content file will be added here
	
	private boolean startS, stopS, settingsS;
	
	/** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
       
        startServer = (Button) findViewById(R.id.startServer);									//initialize the start/stop 
        stopServer = (Button) findViewById(R.id.stopServer);
        settingsButton = (Button) findViewById(R.id.settingsButton);
        aboutButton = (Button) findViewById(R.id.about);
        
        startServer.setOnClickListener(this);													//add listeners to the buttons
        stopServer.setOnClickListener(this);        
        settingsButton.setOnClickListener(this);
        aboutButton.setOnClickListener(this);
        stopServer.setSelected(true);
    } 
    
    //change the button states
    public void changeButtonsState(boolean startS, boolean stopS, boolean settingsS) {
    	this.startS = startS;
    	this.stopS = stopS;
    	this.settingsS = settingsS;
    	
    	startServer.setSelected(startS);
    	stopServer.setSelected(stopS);
		settingsButton.setSelected(settingsS);
    }
    
    public void onClick(View v) {
		int id = v.getId();																		//get the id of the pressed item
		Intent intent = null;
		
		switch(id) { 
			case R.id.settingsButton:
				if(!isStarted) {																//setting only for the offline mode of the server
					Intent settingsIntent = new Intent(RDFServer.this, ServerSettings.class);//show the file
					startActivityForResult(settingsIntent, 0);
				}
				break;
			case R.id.startServer:
				if(!isStarted) {
					if (isNetworkAvailable()) {													//there is an Internet connection available
						isStarted = true;
						intent = new Intent(RDFServer.this, WebService.class);
						startService(intent);
					}
					else { }																	//do nothing
						changeButtonsState(true, false, true);									//change the buttons state
				}
				break;
			case R.id.stopServer:
				if(isStarted) { 
					isStarted = false;	
					changeButtonsState(false, true, false);										//change the button states
					intent = new Intent(RDFServer.this, WebService.class);
					stopService(intent);
				}
				break;
			case R.id.about:
				showDialog(DIALOG_ABOUT);
				break;
			default:
				break;
		}
	}
    
    protected void onResume() { 
    	super.onResume();
	    SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
	    startS = prefs.getBoolean(getResources().getString(R.string.prefs_startS), false);
	    stopS = prefs.getBoolean(getResources().getString(R.string.prefs_stopS), true);
	    settingsS = prefs.getBoolean(getResources().getString(R.string.prefs_settingsS), false);
	    isStarted = prefs.getBoolean(getResources().getString(R.string.prefs_isStarted),false);
	    changeButtonsState(startS, stopS, settingsS);
    }
    
    @Override
	protected void onPause() {
	    super.onPause();
	    SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
    	Editor editor = prefs.edit();
    	editor.putBoolean(getResources().getString(R.string.prefs_startS), startS);
    	editor.putBoolean(getResources().getString(R.string.prefs_stopS), stopS);
    	editor.putBoolean(getResources().getString(R.string.prefs_settingsS), settingsS);
    	editor.putBoolean(getResources().getString(R.string.prefs_isStarted), isStarted);
    	editor.commit();
	}
    
    public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_about:
				showDialog(DIALOG_ABOUT);
				break;
			case R.id.menu_quit:
				finish();
				break;
			default: 
		}
		return super.onOptionsItemSelected(item);
	}
    
    public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.menu, menu);
	    return true;
	}
    
    protected Dialog onCreateDialog(int id) {
	    Dialog dialog;
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    switch(id) {
	    case DIALOG_ABOUT :
	    	String version = "Non recoverable version";
			try {
			    version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			} catch ( NameNotFoundException nnfex ) {
			    nnfex.printStackTrace();
			}
			builder.setTitle(getString(R.string.about_ttl));
			builder.setCancelable(true);
			builder.setMessage( getString(R.string.about_txt).replace( "@@VERS@@", version ) );
			builder.setPositiveButton(android.R.string.ok, null);
			builder.setIcon(android.R.drawable.ic_dialog_info);
			dialog = builder.create();
			break;
	    default:
	        dialog = null;
	    }
	    return dialog;
	}	
    
    // check if there is any network available
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }
    
	@Override
	public void onInflate(ViewStub arg0, View view) {
	}

	//Saving the application state  
	public void onSaveInstanceState(Bundle savedInstanceState) { 
	}
	
	//Recovering the application state 
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
	}

}