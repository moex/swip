/**
* $Id: ItemsAdapter.java$
*
* Copyright (C) 2011, INRIA
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 2.1 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
* USA.
*/

package fr.inrialpes.exmo.rdfserver;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ItemsAdapter extends ArrayAdapter<Item> {
	private final Context context;
	private ArrayList<Item> items;

	//constructor
	public ItemsAdapter(Context context, int textViewResourceId, ArrayList<Item> items) {
			super(context, textViewResourceId, items);
			this.context = context;
			this.items = items;
		}

	//constructor
	public ItemsAdapter(Context context, ArrayList<Item> items) {
		super(context, android.R.layout.simple_list_item_1, items);
		this.context = context;
		this.items = items;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) { 
		View rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.row_layout, null, true);
		}
		Item item = items.get(position);
		if(item != null) { 
			TextView itemId = (TextView) rowView.findViewById(R.id.itemId);
			TextView textId = (TextView) rowView.findViewById(R.id.itemText);
			
			if(itemId != null)
				itemId.setText(item.getItemId());
			if(textId != null)
				textId.setText(item.getItemText());
		}
		return rowView;
	}
}

class Item { 
	private String itemId;
	private String itemText;
	
	public Item(String itemId, String itemText) { 
		this.itemId = itemId;
		this.itemText = itemText;
	}
	
	public Item() { 
		this.itemId = null;
		this.itemText = null;
	}
	
	public void setItemId(String itemId) { 
		this.itemId = itemId;
	}
	
	public void setItemText(String itemText) {
		this.itemText = itemText;
	}
	
	public String getItemId() {
		return this.itemId;
	}
	
	public String getItemText() {
		return this.itemText;
	}
}


