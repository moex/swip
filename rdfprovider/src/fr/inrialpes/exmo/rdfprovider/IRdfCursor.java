/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /Users/jerome/Recherche/rdfprovider/eclipseprojects/RDFMapProvider/src/fr/inrialpes/exmo/rdfprovider/IRdfCursor.aidl
 */
package fr.inrialpes.exmo.rdfprovider;
import java.lang.String;
import android.os.RemoteException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Binder;
import android.os.Parcel;
public interface IRdfCursor extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements fr.inrialpes.exmo.rdfprovider.IRdfCursor
{
private static final java.lang.String DESCRIPTOR = "fr.inrialpes.exmo.rdfprovider.IRdfCursor";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an IRdfCursor interface,
 * generating a proxy if needed.
 */
public static fr.inrialpes.exmo.rdfprovider.IRdfCursor asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof fr.inrialpes.exmo.rdfprovider.IRdfCursor))) {
return ((fr.inrialpes.exmo.rdfprovider.IRdfCursor)iin);
}
return new fr.inrialpes.exmo.rdfprovider.IRdfCursor.Stub.Proxy(obj);
}
public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_hasNext:
{
data.enforceInterface(DESCRIPTOR);
boolean _result = this.hasNext();
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_next:
{
data.enforceInterface(DESCRIPTOR);
fr.inrialpes.exmo.rdfprovider.Statement _result = this.next();
reply.writeNoException();
if ((_result!=null)) {
reply.writeInt(1);
_result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_moveToFirst:
{
data.enforceInterface(DESCRIPTOR);
this.moveToFirst();
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements fr.inrialpes.exmo.rdfprovider.IRdfCursor
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
public boolean hasNext() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_hasNext, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public fr.inrialpes.exmo.rdfprovider.Statement next() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
fr.inrialpes.exmo.rdfprovider.Statement _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_next, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
_result = fr.inrialpes.exmo.rdfprovider.Statement.CREATOR.createFromParcel(_reply);
}
else {
_result = null;
}
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public void moveToFirst() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_moveToFirst, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_hasNext = (IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_next = (IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_moveToFirst = (IBinder.FIRST_CALL_TRANSACTION + 2);
}
public boolean hasNext() throws android.os.RemoteException;
public fr.inrialpes.exmo.rdfprovider.Statement next() throws android.os.RemoteException;
public void moveToFirst() throws android.os.RemoteException;
}
