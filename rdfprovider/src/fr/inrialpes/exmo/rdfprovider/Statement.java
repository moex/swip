/**
 *   Copyright 2007-2010 J�r�me David, J�r�me Euzenat, Universit� Pierre Mend�s France, INRIA
 *   
 *   Statement.java is part of rdfprovider.
 *
 *   rdfprovider is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   rdfprovider is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with rdfprovider; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.rdfprovider;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public final class Statement implements Parcelable {

	private String subject;
	private String predicate;
	private String object;
	
	public Statement(String s, String p, String o) {
		subject=s;
		predicate=p;
		object=o;
	}

	public Statement(Parcel in) {
		subject = in.readString();
		predicate = in.readString();
		object = in.readString();
	}

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeString(subject);
		out.writeString(predicate);
		out.writeString(object);
	}

	public static final Parcelable.Creator<Statement> CREATOR = new Parcelable.Creator<Statement>() {
		public Statement createFromParcel(Parcel in) {
			return new Statement(in);
		}

		public Statement[] newArray(int size) {
			return new Statement[size];
		}
	};
	
	public Uri getSubject() {
		return Uri.parse(subject);
	}
	
	public Uri getPredicate() {
		return Uri.parse(predicate);
	}
	
	public String getObject() {
		return object;
	}
	
	public String toString() {
		return subject+"\t"+predicate+"\t"+object;
	}
}
