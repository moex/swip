/**
 *   Copyright 2007-2010 J�r�me David, J�r�me Euzenat, Universit� Pierre Mend�s France, INRIA
 *   
 *   ArrayRdfAdapter.java is part of rdfprovider.
 *
 *   rdfprovider is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   rdfprovider is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with rdfprovider; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.rdfprovider;

import java.util.ArrayList;
import java.util.ListIterator;


public class ArrayRdfAdapter extends RdfCursor {
	
	private ArrayList<Statement> statementCollection;
	private ListIterator<Statement> it;

	public ArrayRdfAdapter(ArrayList<Statement> list) {
		statementCollection=list;
		it=statementCollection.listIterator();
	}
	
	public ArrayRdfAdapter() {
		this(new ArrayList<Statement>());
	}
	
	public boolean hasNext() {
		return it.hasNext();
	}

	public Statement next() {
		return it.next();
	}
	
	public void moveToFirst() {
		it=statementCollection.listIterator();
	}

	public void add(Statement s) {
		statementCollection.add(s);	
	}
}
