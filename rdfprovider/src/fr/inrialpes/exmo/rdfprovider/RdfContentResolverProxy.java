/**
 *   Copyright 2007-2010 J�r�me David, J�r�me Euzenat, Universit� Pierre Mend�s France, INRIA
 *   
 *   RdfContentResolverProxy.java is part of rdfprovider.
 *
 *   rdfprovider is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   rdfprovider is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with rdfprovider; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.rdfprovider;

import java.net.UnknownHostException;
import java.util.List;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import fr.inrialpes.exmo.rdfprovider.resolver.IRdfContentResolver;

public class RdfContentResolverProxy implements ServiceConnection {
	public static RdfContentResolverProxy INSTANCE=null;

	private Context context;
	private IRdfContentResolver resolver;
	
	public RdfContentResolverProxy(Context ctx) {
		context=ctx.getApplicationContext();
		bind();
	}
	
	protected void bind() {
		Intent i = new Intent();
		i.setAction("android.intent.action.ACTION_RESOLVE_RDFCP");
		context.bindService(i, RdfContentResolverProxy.this, Service.BIND_AUTO_CREATE);
		System.out.println("RdfContentResolverProxy instanci� : "+this);
		binded=true;	
	}
	
	protected void finalize() throws Throwable {
		System.err.println("hello");
		context.unbindService(this);
		super.finalize();
	}

	public static RdfContentResolverProxy getInstance(Context ctx) {
		if (INSTANCE==null) INSTANCE = new RdfContentResolverProxy(ctx);
		return INSTANCE;		
	}
	
	public RdfCursor get(Uri uri) {
		IRdfContentProvider p = acquireProvider(uri);
		if (p==null) return null;
		try {
            return new RdfAdapter(p.get(uri));
        } catch (RemoteException e) {
            return null;
        } catch (java.lang.Exception e) {
        	e.printStackTrace();
            return null;
        } finally {
            releaseProvider(p);
        }
	}
	
	public List<Uri> getOntologies(String authority) {
		IRdfContentProvider p = acquireProvider(Uri.parse("content://"+authority)); 
		if (p==null) return null;
		try {
            return p.getOntologies();
        } catch (RemoteException e) {
        	e.printStackTrace();
            return null;
        } catch (java.lang.Exception e) {
        	e.printStackTrace();
            return null;
        } finally {
            releaseProvider(p);
        }
	}
	
	public List<Uri> getTypes(Uri uri) {	
		IRdfContentProvider p = acquireProvider(uri); 
		if (p==null) return null;
		try {
            return p.getTypes(uri); 
        } catch (RemoteException e) {
        	e.printStackTrace();
            return null;
        } catch (java.lang.Exception e) {
        	e.printStackTrace();
            return null;
        } finally {
            releaseProvider(p);
        }
	}
	
	public byte[] getRdf(Uri uri, String baseUri, String lang) { 
		
		byte[] ba=null;
		try {
			ba = resolver.getRDF(uri, baseUri, lang);
		} catch (RemoteException e) {
			Log.d("ERROR", "INCORRECT URI!!!");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ba;
		
	}
	
	public List<Uri> getObjects(Uri uri) {	
		IRdfContentProvider p = acquireProvider(uri); 
		if (p==null) return null;
		try {
            return p.getObjects(uri);
        } catch (RemoteException e) {
        	e.printStackTrace();
            return null;
        } catch (java.lang.Exception e) {
        	System.err.println(p);
        	e.printStackTrace();
            return null;
        } finally {
            releaseProvider(p);
        }
	}
	
	public final IRdfContentProvider acquireProvider(Uri uri) {
		try {
			if (!binded) bind();
			if (resolver==null) {
				System.out.println("Resolver null");
				/*synchronized(this) {
					this.wait();
				}*/
			}
			
			// if resolver null throw Exception saying that the proxy as not been loaded..
			return IRdfContentProvider.Stub.asInterface(resolver.acquireProvider(uri));
		}
		catch (DeadObjectException ex) {
			bind();
			System.err.println("PB DeadObjects");
			return acquireProvider(uri);
		}
		catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
		
    }
	
	protected final boolean releaseProvider(IRdfContentProvider provider) {
		return true;
	}


	private boolean binded=false;
	public void onServiceConnected(ComponentName className, final IBinder service) {
		new Thread() {
			public void run() {
				resolver=IRdfContentResolver.Stub.asInterface(service);
				System.out.println("Resolver binded");
				/*synchronized(this) {
					this.notifyAll();
				}*/
			}
		}.start();
		
	}

	public void onServiceDisconnected(ComponentName className) {
		resolver=null;
		binded=false;
		System.out.println("Resolver unbinded");
	}
}
