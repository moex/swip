/**
 *   Copyright 2007-2010 J�r�me David, J�r�me Euzenat, Universit� Pierre Mend�s France, INRIA
 *   
 *   RdfContentProvider.java is part of rdfprovider.
 *
 *   rdfprovider is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   rdfprovider is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with rdfprovider; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.rdfprovider;

import java.util.List;

import fr.inrialpes.exmo.rdfprovider.resolver.IRdfContentResolver;


import android.app.Service;
import android.content.ComponentName;
import android.content.ContentProvider;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ProviderInfo;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;


public abstract class RdfContentProvider extends ContentProvider {
	
	protected String[] authorities;
	protected IRdfContentResolver manager;
	
	private ServiceConnection managerConnection = new ServiceConnection() {
			public void onServiceConnected(ComponentName className, IBinder service) {
				try {
					manager=IRdfContentResolver.Stub.asInterface(service);
					IBinder binder = RdfContentProvider.this.getIRdfContentProvider().asBinder();
					for (String authority : authorities) {
						manager.registerProvider(binder,authority);
					}
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
			public void onServiceDisconnected(ComponentName className) {
				manager=null;
			}
		};;
	
	public RdfContentProvider() {
		authorities=new String[1];
	}
	
	public void attachInfo(Context context, ProviderInfo info) {
		super.attachInfo(context, info);
		authorities[0]=info.authority;
		Intent i = new Intent();
		i.setAction("android.intent.action.ACTION_REGISTER_RDFCP");
		context.bindService(i,managerConnection, Service.BIND_AUTO_CREATE);
	}

	protected void finalize() throws Throwable {
		if (manager!=null)
			for (String authority : authorities)
				manager.unregisterProvider(authority);
		Context ctx = getContext();
		if(ctx!=null) 
			ctx.unbindService(managerConnection);
		super.finalize();
	}
	private Transport mTransport = new Transport();
	
	private class Transport extends IRdfContentProvider.Stub {

		@Override
		public IRdfCursor get(Uri uri) throws RemoteException {
			try { 
				IRdfCursor cursor = RdfContentProvider.this.get(uri).getIRdfCursor();
				return RdfContentProvider.this.get(uri).getIRdfCursor();
			} catch (Exception ex) { 
				return null;
			}
		}

		@Override
		public List<Uri> getQueryEntities() throws RemoteException {
			return RdfContentProvider.this.getQueryEntities();
		}

		@Override
		public List<Uri> getTypes(Uri uri) throws RemoteException {
			return RdfContentProvider.this.getTypes(uri);
		}

		@Override
		public List<Uri> getOntologies() throws RemoteException {
			return RdfContentProvider.this.getOntologies();
		}

		@Override
		public List<Uri> getObjects(Uri uri) throws RemoteException {
			return RdfContentProvider.this.getObjects(uri);
		}

	}

	public abstract RdfCursor get(Uri uri);
	
	public abstract List<Uri> getTypes(Uri uri);
	public abstract List<Uri> getOntologies();
	public abstract List<Uri> getQueryEntities();
	public abstract List<Uri> getObjects(Uri uri);
	
	//public abstract String getType(Uri uri);
	//add other methods
	
	
	public IRdfContentProvider getIRdfContentProvider() {
		return mTransport;
	}
}
