package fr.inrialpes.exmo.rdfprovider;

import android.net.Uri;
import fr.inrialpes.exmo.rdfprovider.IRdfCursor;

interface IRdfContentProvider {
	IRdfCursor get(in Uri uri);
	//IRdfCursor query(in String sparqlQuery);
	List<Uri> getQueryEntities();
	List<Uri> getOntologies();
	List<Uri> getTypes(in Uri uri);
	List<Uri> getObjects(in Uri uri); 
}
