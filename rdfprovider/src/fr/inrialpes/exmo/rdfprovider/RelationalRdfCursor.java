/**
 *   Copyright 2007-2010 Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   RelationalRdfCursor.java is part of rdfprovider.
 *
 *   rdfprovider is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   rdfprovider is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with rdfprovider; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.rdfprovider;

import java.util.Map;

import android.database.Cursor;
import android.net.Uri;

public class RelationalRdfCursor extends RdfCursor {
	public static final String defaultString = new String("");
	//private Context ctx = null;
	
	private final Map<String,Uri> col2Uris;
	private final Cursor cursor;
	private final Uri baseUri;
	
	private final int[] idx;
	private final  int idIdx;
	
	private int currentId=-1;
	private int currentIdx=-1;
	
	private Statement stmt;
	private boolean nextCalled = true;
	private boolean hasNext = false;
	
	public RelationalRdfCursor(Cursor cursor, Map<String,Uri> col2Uris, Uri baseUri, String idColumn) {
		this.cursor=cursor;
		this.col2Uris=col2Uris;
		this.baseUri=baseUri;
		
		int colCount = cursor.getColumnCount();
		int[] idx = new int[col2Uris.size()];
		
		int j=0;
		for (int i=0 ; i<colCount ; i++) {
			if (col2Uris.containsKey(cursor.getColumnName(i))) {
				idx[j++]=i;
				}
		}
		if (j<col2Uris.size()) {
			this.idx = new int[j];
			System.arraycopy(idx, 0, this.idx, 0, j);
		}
		else 
			this.idx=idx;
		idIdx = cursor.getColumnIndex(idColumn); 
	}
		
	public boolean hasNext() { 
		String cursorString = null;
		if(!nextCalled) return hasNext;
		nextCalled = false;
		
		if ( (!cursor.isAfterLast()) && (currentIdx<idx.length || !cursor.isLast())) {
			if (currentIdx==idx.length || currentIdx==-1) {
				if (cursor.moveToNext()) {
					currentIdx=0; 
					currentId = cursor.getInt(idIdx);
				} else { 
					return false;
				}
			} 
				
			try {
				cursorString = cursor.getString(idx[currentIdx]);
			} catch(Exception ex) {cursorString = defaultString; }

			if (cursorString == null) {
				stmt = new Statement(baseUri.toString()+"/"+currentId,col2Uris.get(cursor.getColumnName(idx[currentIdx])).toString(),"");
				currentIdx++;
				nextCalled = true;
				return hasNext();
			}
				
			cursorString = cursor.getString(idx[currentIdx]);
			String column = col2Uris.get(cursor.getColumnName(idx[currentIdx])).toString();	
			currentIdx++;		
			stmt = new Statement(baseUri.toString()+"/"+currentId,column,cursorString);
			hasNext = true;
			return hasNext;
		}
		hasNext = false;
		return hasNext;
	}

	public Statement next() {
		nextCalled = true;
		return stmt;
	} 

	public void moveToFirst() {
		cursor.moveToFirst();
		currentIdx=0;
		currentId=cursor.getInt(idIdx);
	}
}
