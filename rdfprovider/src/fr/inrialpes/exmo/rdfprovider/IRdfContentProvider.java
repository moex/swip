/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /Users/jerome/Recherche/rdfprovider/eclipseprojects/RDFMapProvider/src/fr/inrialpes/exmo/rdfprovider/IRdfContentProvider.aidl
 */
package fr.inrialpes.exmo.rdfprovider;
import java.lang.String;
import android.os.RemoteException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Binder;
import android.os.Parcel;
import android.net.Uri;
import java.util.List;
public interface IRdfContentProvider extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements fr.inrialpes.exmo.rdfprovider.IRdfContentProvider
{
private static final java.lang.String DESCRIPTOR = "fr.inrialpes.exmo.rdfprovider.IRdfContentProvider";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an IRdfContentProvider interface,
 * generating a proxy if needed.
 */
public static fr.inrialpes.exmo.rdfprovider.IRdfContentProvider asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof fr.inrialpes.exmo.rdfprovider.IRdfContentProvider))) {
return ((fr.inrialpes.exmo.rdfprovider.IRdfContentProvider)iin);
}
return new fr.inrialpes.exmo.rdfprovider.IRdfContentProvider.Stub.Proxy(obj);
}
public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_get:
{
data.enforceInterface(DESCRIPTOR);
android.net.Uri _arg0;
if ((0!=data.readInt())) {
_arg0 = android.net.Uri.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
fr.inrialpes.exmo.rdfprovider.IRdfCursor _result = this.get(_arg0);
reply.writeNoException();
reply.writeStrongBinder((((_result!=null))?(_result.asBinder()):(null)));
return true;
}
case TRANSACTION_getQueryEntities:
{
data.enforceInterface(DESCRIPTOR);
java.util.List<android.net.Uri> _result = this.getQueryEntities();
reply.writeNoException();
reply.writeTypedList(_result);
return true;
}
case TRANSACTION_getOntologies:
{
data.enforceInterface(DESCRIPTOR);
java.util.List<android.net.Uri> _result = this.getOntologies();
reply.writeNoException();
reply.writeTypedList(_result);
return true;
}
case TRANSACTION_getTypes:
{
data.enforceInterface(DESCRIPTOR);
android.net.Uri _arg0;
if ((0!=data.readInt())) {
_arg0 = android.net.Uri.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
java.util.List<android.net.Uri> _result = this.getTypes(_arg0);
reply.writeNoException();
reply.writeTypedList(_result);
return true;
}
case TRANSACTION_getObjects:
{
data.enforceInterface(DESCRIPTOR);
android.net.Uri _arg0;
if ((0!=data.readInt())) {
_arg0 = android.net.Uri.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
java.util.List<android.net.Uri> _result = this.getObjects(_arg0);
reply.writeNoException();
reply.writeTypedList(_result);
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements fr.inrialpes.exmo.rdfprovider.IRdfContentProvider
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
public fr.inrialpes.exmo.rdfprovider.IRdfCursor get(android.net.Uri uri) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
fr.inrialpes.exmo.rdfprovider.IRdfCursor _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((uri!=null)) {
_data.writeInt(1);
uri.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_get, _data, _reply, 0);
_reply.readException();
_result = fr.inrialpes.exmo.rdfprovider.IRdfCursor.Stub.asInterface(_reply.readStrongBinder());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
//IRdfCursor query(in String sparqlQuery);

public java.util.List<android.net.Uri> getQueryEntities() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
java.util.List<android.net.Uri> _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getQueryEntities, _data, _reply, 0);
_reply.readException();
_result = _reply.createTypedArrayList(android.net.Uri.CREATOR);
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public java.util.List<android.net.Uri> getOntologies() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
java.util.List<android.net.Uri> _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getOntologies, _data, _reply, 0);
_reply.readException();
_result = _reply.createTypedArrayList(android.net.Uri.CREATOR);
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public java.util.List<android.net.Uri> getTypes(android.net.Uri uri) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
java.util.List<android.net.Uri> _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((uri!=null)) {
_data.writeInt(1);
uri.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_getTypes, _data, _reply, 0);
_reply.readException();
_result = _reply.createTypedArrayList(android.net.Uri.CREATOR);
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public java.util.List<android.net.Uri> getObjects(android.net.Uri uri) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
java.util.List<android.net.Uri> _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((uri!=null)) {
_data.writeInt(1);
uri.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_getObjects, _data, _reply, 0);
_reply.readException();
_result = _reply.createTypedArrayList(android.net.Uri.CREATOR);
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
}
static final int TRANSACTION_get = (IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_getQueryEntities = (IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_getOntologies = (IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_getTypes = (IBinder.FIRST_CALL_TRANSACTION + 3);
static final int TRANSACTION_getObjects = (IBinder.FIRST_CALL_TRANSACTION + 4);
}
public fr.inrialpes.exmo.rdfprovider.IRdfCursor get(android.net.Uri uri) throws android.os.RemoteException;
//IRdfCursor query(in String sparqlQuery);

public java.util.List<android.net.Uri> getQueryEntities() throws android.os.RemoteException;
public java.util.List<android.net.Uri> getOntologies() throws android.os.RemoteException;
public java.util.List<android.net.Uri> getTypes(android.net.Uri uri) throws android.os.RemoteException;
public java.util.List<android.net.Uri> getObjects(android.net.Uri uri) throws android.os.RemoteException;
}
