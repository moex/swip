/**
 *   Copyright 2007-2010 J�r�me David, J�r�me Euzenat, Universit� Pierre Mend�s France, INRIA
 *   
 *   RDFContactProviderV2.java is part of rdfcontentresolver.
 *
 *   rdfcontentresolver is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   rdfcontentresolver is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with rdfcontentresolver; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

package fr.inrialpes.exmo.rdfprovider;

import java.util.Iterator;

import android.os.RemoteException;

public abstract class RdfCursor implements Iterator<Statement>{
	
	
	private Transport mTransport = new Transport();
	
	public RdfCursor() {
	}

	class Transport extends IRdfCursor.Stub {

		@Override
		public boolean hasNext() throws RemoteException {
			return RdfCursor.this.hasNext();
		}

		@Override
		public Statement next() throws RemoteException {
			return RdfCursor.this.next();
		}

		@Override
		public void moveToFirst() throws RemoteException {
			RdfCursor.this.moveToFirst();	
		}
	}
	
	public abstract boolean hasNext();
	
    public abstract Statement next();
    
	@Override
	public void remove() {
		throw new UnsupportedOperationException();	
	}
	
	public abstract void moveToFirst();
	
    public IRdfCursor getIRdfCursor() {
        return mTransport;
    }
    
    public RdfCursor andThen(final RdfCursor c) {
		return new RdfCursor() {
			public boolean hasNext() {
				return RdfCursor.this.hasNext()||c.hasNext();
			}

			public Statement next() {
				if (RdfCursor.this.hasNext()) return RdfCursor.this.next();
				return c.next();
			}

			public void moveToFirst() {
				RdfCursor.this.moveToFirst();
			}
			
		};
	}
    

}
