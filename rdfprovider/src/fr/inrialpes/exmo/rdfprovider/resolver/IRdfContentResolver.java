/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /Users/maria/Documents/[]Work/Pikoid and co/rdfcontentresolver/rdfcontentresolver/src/fr/inrialpes/exmo/rdfprovider/resolver/IRdfContentResolver.aidl
 */
package fr.inrialpes.exmo.rdfprovider.resolver;
public interface IRdfContentResolver extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements fr.inrialpes.exmo.rdfprovider.resolver.IRdfContentResolver
{
private static final java.lang.String DESCRIPTOR = "fr.inrialpes.exmo.rdfprovider.resolver.IRdfContentResolver";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an fr.inrialpes.exmo.rdfprovider.resolver.IRdfContentResolver interface,
 * generating a proxy if needed.
 */
public static fr.inrialpes.exmo.rdfprovider.resolver.IRdfContentResolver asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof fr.inrialpes.exmo.rdfprovider.resolver.IRdfContentResolver))) {
return ((fr.inrialpes.exmo.rdfprovider.resolver.IRdfContentResolver)iin);
}
return new fr.inrialpes.exmo.rdfprovider.resolver.IRdfContentResolver.Stub.Proxy(obj);
}
public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_registerProvider:
{
data.enforceInterface(DESCRIPTOR);
android.os.IBinder _arg0;
_arg0 = data.readStrongBinder();
java.lang.String _arg1;
_arg1 = data.readString();
this.registerProvider(_arg0, _arg1);
reply.writeNoException();
return true;
}
case TRANSACTION_unregisterProvider:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
this.unregisterProvider(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_acquireProvider:
{
data.enforceInterface(DESCRIPTOR);
android.net.Uri _arg0;
if ((0!=data.readInt())) {
_arg0 = android.net.Uri.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
android.os.IBinder _result = this.acquireProvider(_arg0);
reply.writeNoException();
reply.writeStrongBinder(_result);
return true;
}
case TRANSACTION_getRDF:
{
data.enforceInterface(DESCRIPTOR);
android.net.Uri _arg0;
if ((0!=data.readInt())) {
_arg0 = android.net.Uri.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
java.lang.String _arg1;
_arg1 = data.readString();
java.lang.String _arg2;
_arg2 = data.readString();
byte[] _result = this.getRDF(_arg0, _arg1, _arg2);
reply.writeNoException();
reply.writeByteArray(_result);
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements fr.inrialpes.exmo.rdfprovider.resolver.IRdfContentResolver
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
public void registerProvider(android.os.IBinder provider, java.lang.String authority) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder(provider);
_data.writeString(authority);
mRemote.transact(Stub.TRANSACTION_registerProvider, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void unregisterProvider(java.lang.String authority) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(authority);
mRemote.transact(Stub.TRANSACTION_unregisterProvider, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public android.os.IBinder acquireProvider(android.net.Uri uri) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
android.os.IBinder _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((uri!=null)) {
_data.writeInt(1);
uri.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_acquireProvider, _data, _reply, 0);
_reply.readException();
_result = _reply.readStrongBinder();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public byte[] getRDF(android.net.Uri uri, java.lang.String baseUri, java.lang.String lang) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
byte[] _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((uri!=null)) {
_data.writeInt(1);
uri.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
_data.writeString(baseUri);
_data.writeString(lang);
mRemote.transact(Stub.TRANSACTION_getRDF, _data, _reply, 0);
_reply.readException();
_result = _reply.createByteArray();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
}
static final int TRANSACTION_registerProvider = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_unregisterProvider = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_acquireProvider = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_getRDF = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
}
public void registerProvider(android.os.IBinder provider, java.lang.String authority) throws android.os.RemoteException;
public void unregisterProvider(java.lang.String authority) throws android.os.RemoteException;
public android.os.IBinder acquireProvider(android.net.Uri uri) throws android.os.RemoteException;
public byte[] getRDF(android.net.Uri uri, java.lang.String baseUri, java.lang.String lang) throws android.os.RemoteException;
}
