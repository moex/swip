package fr.inrialpes.exmo.rdfprovider.resolver; 

//import fr.inrialpes.exmo.rdfprovider.IRdfContentProvider.Stub;
import android.os.IBinder;
import android.net.Uri;

interface IRdfContentResolver {  
   void registerProvider(in IBinder provider, in String authority);
   void unregisterProvider(in String authority);
   IBinder acquireProvider(in Uri uri);
   byte[] getRDF(in Uri uri, in String baseUri, in String lang);
}