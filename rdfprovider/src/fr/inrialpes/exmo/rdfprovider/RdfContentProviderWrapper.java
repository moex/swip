/**
 *   Copyright 2007-2010 J�r�me David, J�r�me Euzenat, Universit� Pierre Mend�s France, INRIA
 *   
 *   RdfContentProviderWrapper.java is part of rdfprovider.
 *
 *   rdfprovider is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   rdfprovider is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with rdfprovider; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.rdfprovider;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

public abstract class RdfContentProviderWrapper extends RdfContentProvider {
	
	
	public RdfContentProviderWrapper() {
		authorities=new String[2];
		authorities[1]=getWrappedAuthority();
	}
	public abstract String getWrappedAuthority();
	
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		return getContext().getContentResolver().delete(uri, selection, selectionArgs);
	}

	public String getType(Uri uri)  {
		String s = null;
		try {
			s=getContext().getContentResolver().getType(Uri.parse("content://"+getWrappedAuthority()+uri.getPath()));
		}
		catch (Exception e) {e.printStackTrace();}
		return s;
	}

	public Uri insert(Uri uri, ContentValues values) {
		return getContext().getContentResolver().insert(Uri.parse("content://"+getWrappedAuthority()+uri.getPath()), values);
	}

	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		return getContext().getContentResolver().query(Uri.parse("content://"+getWrappedAuthority()+uri.getPath()), projection, selection, selectionArgs, sortOrder);
	}

	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		return getContext().getContentResolver().update(Uri.parse("content://"+getWrappedAuthority()+uri.getPath()), values, selection, selectionArgs);
	}

}
