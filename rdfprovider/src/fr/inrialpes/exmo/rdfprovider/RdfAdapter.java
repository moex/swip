/**
 *   Copyright 2007-2010 J�r�me David, J�r�me Euzenat, Universit� Pierre Mend�s France, INRIA
 *   
 *   RdfAdapter.java is part of rdfprovider.
 *
 *   rdfprovider is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   rdfprovider is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with rdfprovider; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.rdfprovider;

import android.os.RemoteException;

public class RdfAdapter extends RdfCursor {

	private IRdfCursor transport;
	
	public RdfAdapter(IRdfCursor transport) {
		this.transport=transport;
	}
	
	public boolean hasNext() {
		try {
			return transport.hasNext();
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
		
	}


	public void moveToFirst() {
		try {
			transport.moveToFirst();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public Statement next() {
		try {
			return transport.next();
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}

}
