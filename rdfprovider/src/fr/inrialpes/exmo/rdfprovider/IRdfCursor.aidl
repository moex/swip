package fr.inrialpes.exmo.rdfprovider;

import fr.inrialpes.exmo.rdfprovider.Statement;

interface IRdfCursor {
	boolean hasNext();
   	Statement next();
   	void moveToFirst();
}
