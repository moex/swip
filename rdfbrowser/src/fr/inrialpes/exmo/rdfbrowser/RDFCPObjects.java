/**
 *   Copyright 2007-2010 Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   RDFCPObjects.java is part of RDFBrowser.
 *
 *   RDFBrowser is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   RDFBrowser is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with RDFBrowser; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.rdfbrowser;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;
import fr.inrialpes.exmo.rdfprovider.RdfContentResolverProxy;

public class RDFCPObjects extends Activity implements OnClickListener {
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.cp_infos_basic);
	    
	    String providerClass = this.getIntent().getData().getAuthority();
	    
	    RdfContentResolverProxy cr = RdfContentResolverProxy.getInstance(this);//new RdfContentResolver(this);
	    
	    ViewGroup vg = (ViewGroup) findViewById(R.id.cp_infos_rootpanel);
	    
	    List<Uri> objects = cr.getObjects(this.getIntent().getData());
	    TextView tvTitle = new TextView(this);
	    tvTitle.setTextSize(22);
	    tvTitle.setText("Objects:");
    	vg.addView(tvTitle);
    	
	    for (Uri o : objects) { 
	    	TextView tv = new TextView(this);
	    	tv.setTextSize(18);
	    	tv.setPadding(5, 2, 0, 3);
	    	tv.setText(o.toString());
	    	tv.setOnClickListener(this);
	    	vg.addView(tv);
	    }
	    
	}

	@Override
	public void onClick(View view) {
		Intent i = new Intent();
		i.setAction("android.intent.action.VIEW_RDF");
		i.setData(Uri.parse(((TextView) view).getText().toString()));
		startActivity(i);
		
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.browser_menu, menu);
	    menu.removeItem(R.id.menu_viewData);
	    return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent i;
	    // Handle item selection
	    switch (item.getItemId()) {
	    /*case R.id.menu_openUri:
	    	i = new Intent();
	    	i.setClass(this, RDFBrowser.class);
	    	startActivity(i);
	        return true;*/
	    case R.id.menu_providerOnto:
	    	i = new Intent();
	    	i.setClass(this,RDFCPOntologies.class);
	    	i.setData(this.getIntent().getData());
	    	startActivity(i);
	    case R.id.menu_quit:
	    	this.finish();
	    	
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}
}
