/**
 *   Copyright 2007-2010 Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   RDFBrowser.java is part of RDFBrowser.
 *
 *   RDFBrowser is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   RDFBrowser is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with RDFBrowser; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.rdfbrowser;

import java.net.URI;
import java.util.HashMap;
import java.util.List;

import fr.inrialpes.exmo.rdfprovider.RdfCursor;
import fr.inrialpes.exmo.rdfprovider.RdfContentResolverProxy;
import fr.inrialpes.exmo.rdfprovider.Statement;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.view.View.OnClickListener;
import android.view.ViewStub.OnInflateListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;

// Stuff for getAlign
import java.util.Properties;
//import java.util.Hashtable;
//import java.util.Enumeration;
//import java.io.PrintStream;
//import java.io.File;
//import java.io.FileReader;
import java.io.FileInputStream;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.InputStream;

import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.Cell;
import fr.inrialpes.exmo.align.parser.AlignmentParser;
import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.Namespace;
import fr.inrialpes.exmo.align.impl.Annotations;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.MalformedURLException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URLConnection;


public class RDFBrowser extends Activity implements OnClickListener, OnInflateListener {
	 
    public final String DEFAULT_URI="content://fr.inrialpes.exmo.pikoid/pikoid/1";
    public final String ACTION_VIEW_RDF="android.intent.action.VIEW_RDF";
    private final int DIALOG_ACTIVITY_NOT_FOUND=1;
    private final int DIALOG_ABOUT=2;
    
    private View openView=null;  
    private LinearLayout vg; 
    private RdfContentResolverProxy resolver;
	
    /** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        openView =  findViewById(R.id.stub_import);
        ((ViewStub)openView).setOnInflateListener(this);
        resolver=RdfContentResolverProxy.getInstance(this);
        
        vg = new LinearLayout(this);
        vg.setOrientation(LinearLayout.VERTICAL);
        ((LinearLayout) findViewById(R.id.container)).addView(vg);
    }

    protected void onResume() {  
	super.onResume();
	vg.removeAllViewsInLayout();
	Intent i = this.getIntent();
	Uri objUri = i.getData();
	if (objUri==null) {
	    openView.setVisibility(View.VISIBLE);
	    ((EditText) findViewById(R.id.tfUri)).setText(DEFAULT_URI);
	}
	else {
	    showrdf(objUri);
	}
	
    }
	
	private void showrdf(Uri uri) {
		
		((TextView) findViewById(R.id.object)).setText(uri.toString());
		HashMap<Uri,LinearLayout> pred2Views = new HashMap();
		
	    RdfCursor r = resolver.get(uri);
	    if (r==null) {
	    	showDialog(DIALOG_ACTIVITY_NOT_FOUND);
	    }
	    LinearLayout lt=new LinearLayout(this);
		lt.setOrientation(LinearLayout.VERTICAL);
		TextView rdftype = new TextView(this);
		rdftype.setTextSize(18);
		rdftype.setPadding(10, 0, 0, 0);
		rdftype.setText("rdf:type");
		rdftype.setTextColor(Color.GREEN);
		lt.addView(rdftype);
	    List<Uri> types = resolver.getTypes(uri);
	    if (types!=null)
	    for (Uri u : types) {
	    	TextView t = new TextView(this);
        	t.setPadding(20, 0, 0, 0);
        	t.setTextSize(18);
        	t.setTypeface(Typeface.DEFAULT,Typeface.ITALIC);
        	t.setTextColor(Color.GREEN);
        	t.setText(u.toString());
        	lt.addView(t);
	    }
	    vg.addView(lt);
	    
        r.moveToFirst();
        while (r.hasNext()) {
        	Statement stmt = r.next();
        	LinearLayout l = pred2Views.get(stmt.getPredicate());
        	if (l==null) {
        		l=new LinearLayout(this);
        		l.setOrientation(LinearLayout.VERTICAL);
        	
        		TextView pred = new TextView(this);
        		pred.setTextSize(18);
        		pred.setPadding(10, 0, 0, 0);
        		pred.setText(stmt.getPredicate().toString());
        		l.addView(pred);
        		pred2Views.put(stmt.getPredicate(), l);
        		
        	}
        	
        	TextView obj = new TextView(this);
        	obj.setPadding(20, 0, 0, 0);
        	obj.setTextSize(18);
        	obj.setText(stmt.getObject());
        	obj.setTypeface(Typeface.DEFAULT,Typeface.ITALIC);
        	
        	l.addView(obj);
        	try {
        		URI u=new URI(stmt.getObject());
        		if (u.getScheme()!=null && u.getAuthority()!=null) {
	        		
	        		obj.setTextColor(Color.CYAN);
	        		obj.setOnClickListener(this);
        		}
        	}
        	catch (Exception e) {}
        	
        	
        	
        }
        for (View v : pred2Views.values()) {
        	vg.addView(v);
        }
	}

	public void onClick(View v) {
		String data=null;
		if (v.getId()==R.id.btGo) {
			data= ((EditText) findViewById(R.id.tfUri)).getText().toString();
			//showrdf(Uri.parse(uri));
		    openView.setVisibility(View.INVISIBLE);
		}
		else {
			data = ((TextView) v).getText().toString();	
		}
		Intent i = new Intent();
		i.setAction(ACTION_VIEW_RDF);
		i.setData(Uri.parse(data));

		try {
			this.startActivity(i);
		}
		catch (ActivityNotFoundException e) {
			e.printStackTrace();
			showDialog(DIALOG_ACTIVITY_NOT_FOUND);
		}
	}
	
	protected Dialog onCreateDialog(int id) {
	    Dialog dialog;
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    switch(id) {
	    case DIALOG_ACTIVITY_NOT_FOUND:
	    	builder.setMessage("Nothing to show for this URI")
	    	       .setCancelable(false)
	    	       .setNeutralButton("OK", new DialogInterface.OnClickListener() {
	    	           public void onClick(DialogInterface dialog, int id) {
	    	        	   dialog.cancel();
	    	           }
	    	       });
	    	dialog = builder.create();
	    	break;
	    case DIALOG_ABOUT :
	        // Create infoBox
			String version = "Non recoverable version";
			try {
			    version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			} catch ( NameNotFoundException nnfex ) {
			    nnfex.printStackTrace();
			}
			builder
			    .setTitle( getString(R.string.about_ttl) )
			    .setCancelable(true)
			    .setMessage( getString(R.string.about_txt).replace( "@@VERS@@", version ) )
			    .setPositiveButton(android.R.string.ok, null)
			    .setIcon(android.R.drawable.ic_dialog_info);
			dialog = builder.create();
			break;
	    default:
	        dialog = null;
	    }
	    return dialog;
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.browser_menu, menu);
	    return true;
	}

    public static final String HOST = "aserv.inrialpes.fr";
    public static final String HTML = "80";
    private String SERVUrl = null;

    private void listMethods() {
	Properties params = new Properties();
	if ( SERVUrl == null ) SERVUrl = "http://" + HOST + ":" + HTML;
	// Create the message (REST URI)/
	String message = "listmethods"+"?";
	// Send message
	HttpURLConnection connection = sendRESTMessage( message, params );
	printResult( connection );
    }

    private void listAligns() {
	Properties params = new Properties();
	if ( SERVUrl == null ) SERVUrl = "http://" + HOST + ":" + HTML;
	// Create the message (REST URI)/
	String message = "listalignments"+"?";
	// Send message
	HttpURLConnection connection = sendRESTMessage( message, params );
	printResult( connection );
    }

    private void getAlign() {
	Properties params = new Properties();
	if ( SERVUrl == null ) SERVUrl = "http://" + HOST + ":" + HTML;
	// Create the message (REST URI)
	params.setProperty( "id", "http://aserv.inrialpes.fr:8089/alid/1245070241053/824");
	params.setProperty( "method", "fr.inrialpes.exmo.align.impl.renderer.RDFRendererVisitor");
	String message = "retrieve"+"?";
	// Send message
	HttpURLConnection connection = sendRESTMessage( message, params );
	printAlign( connection );
    }

    // JE: taken from AlignmentClient
    public HttpURLConnection sendRESTMessage( String message, Properties param ) {
	HttpURLConnection httpConn = null;
	try {
	    // JE: suppressed the POST/loadfile part: useless here
	    //switch for return format : HTML or XML (by defaut)
	    String URLString = SERVUrl + "/rest/" +  message + "&return=XML";
	    for ( Object key : param.keySet() ) {
		URLString += "&"+(String)key+"="+param.getProperty( (String)key );
	    }
	    URL RESTUrl = new URL( URLString );
	    Toast.makeText(getApplicationContext(), "URL: "+RESTUrl, 15).show();	    
	    //Open a connection with RESTUrl
	    httpConn = (HttpURLConnection)(RESTUrl.openConnection());
	    httpConn.setRequestMethod( "GET" );
	    httpConn.setDoOutput(true);
	    httpConn.setDoInput(true);
	} catch ( MalformedURLException muex ) {
	    // Error from the program
	} catch ( IOException ioex ) {
	    // Error cannot connect to alignment server
	}
	return httpConn;
    }

    // JE: for testing: do it from Files!
    /*
    private void getFileAlign() {
	((EditText) findViewById(R.id.tfUri)).setText("IN 1");
	try {
	    printAlign( new FileInputStream("/sdcard/typical-saop.xml") );
	} catch ( IOException ioex) {
	    ((EditText) findViewById(R.id.tfUri)).setText("IOExp:"+ioex);
	}
    }
    */

    public void printAlign( HttpURLConnection httpConn ) {
	try {
	    printAlign( (InputStream)httpConn.getInputStream() );
	} catch (IOException ioex) {
	    ((EditText) findViewById(R.id.tfUri)).setText("IOExp: httpConn");
	}
    }

    public void printAlign( InputStream is ) {
	Log.i( "PrintAlign", "entering" );
	if ( is == null ) {
	    ((EditText) findViewById(R.id.tfUri)).setText("Echec");
	    return;
	}
	Log.i( "PrintAlign", "PA 0" );
	// Parse the result in this alignment
	try {
	    AlignmentParser parser = new AlignmentParser( 0 );
	    //parser.initAlignment( this );
	    parser.setEmbedded( true );
	    Alignment al = parser.parse( is );
	    String id = ((BasicAlignment)al).getExtension( Namespace.ALIGNMENT.uri, Annotations.ID );
	    String result = "";
	    if ( id != null ) result = id;
	    result += "\nNbCells: "+al.nbCells() +"\n\n";
	    for ( Cell c : al ) {
		result += c.getObject1AsURI().getFragment()+" == "+c.getObject2AsURI().getFragment() +"\n";
	    }
	    ((EditText) findViewById(R.id.tfUri)).setText( result );
	} catch (Exception ex) { // JE2009: To suppress in Version 4 (JE2011: maybe not)
	    Log.e( "Exception", "--" );
	    StringWriter sw = new StringWriter();
	    if ( ex.getCause() != null ) {
	    Log.e( "Exception", ex.getStackTrace().toString() );
	    Log.e( "Exception->Cause", ex.getCause().getStackTrace().toString() );
		ex.getCause().printStackTrace();
		ex.getCause().printStackTrace(new PrintWriter(sw));
	    } else {
	    Log.e( "Exception", ex.getStackTrace().toString() );
		ex.printStackTrace();
		ex.printStackTrace(new PrintWriter(sw));
	    }
	    //StringWriter sw = new StringWriter();
	    ex.printStackTrace(new PrintWriter(sw));
    	    ((EditText) findViewById(R.id.tfUri)).setText(((EditText)findViewById(R.id.tfUri)).getText()+" + "+sw.toString());
	}
    }

    // This works and only prints the content of the message
    public void printResult( HttpURLConnection httpConn ) {
	if ( httpConn == null ) {
	    ((EditText) findViewById(R.id.tfUri)).setText("Echec");
	    return;
	}
	try {
	    // Read the response
	    InputStreamReader isr = new InputStreamReader( httpConn.getInputStream() );
	    BufferedReader in = new BufferedReader(isr);
	    StringBuffer strBuff = new StringBuffer();
	    String line;
	    while ((line = in.readLine()) != null) {
		strBuff.append( line + "\n");
	    }
	    if ( in != null ) in.close();
	    String answer = strBuff.toString();
	    // Display the answer
	    //String ll = "length : "+answer.length();
	    // 28=UnRecognizeedAction
	    ((EditText) findViewById(R.id.tfUri)).setText(answer);
	} catch ( MalformedURLException muex ) {
	    // Error from the program
	    ((EditText)findViewById(R.id.tfUri)).setText("MalformedURLException");
	} catch ( ProtocolException pex ) {
	    // Should not occur
	    ((EditText) findViewById(R.id.tfUri)).setText("ProtocolException");
	} catch ( IOException ioex ) {
	    // Error cannot connect to alignment server
	    ((EditText) findViewById(R.id.tfUri)).setText(((EditText)findViewById(R.id.tfUri)).getText()+" + IOException");
	}
    }

	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
		/*
	    case R.id.menu_openUri:
	    	openView.setVisibility(View.VISIBLE);
			((EditText) findViewById(R.id.tfUri)).setText(DEFAULT_URI);
	        return true;
		*/
	    case R.id.menu_printAlign:
		getAlign();
	        return true;
	    case R.id.menu_getAligns:// This one must change the stuff
		listAligns();
	        return true;
	    case R.id.menu_getMethods:
		listMethods();
	        return true;
	    case R.id.menu_viewData:
	    	Intent i = new Intent();
	    	i.setAction(Intent.ACTION_VIEW);
	    	i.setData(this.getIntent().getData());
	    	try {
	    		startActivity(i);
	    	}
	    	catch (ActivityNotFoundException e) {
	    		e.printStackTrace();
				showDialog(DIALOG_ACTIVITY_NOT_FOUND);
	    	}
	    	return true;
	    case R.id.menu_providerOnto:
	    	i = new Intent();
	    	i.setClass(this,RDFCPOntologies.class);
	    	i.setData(this.getIntent().getData());
	    	startActivity(i);
	    	return true;
	    case R.id.menu_quit:
	    	this.finish();
	    	break;
	    case R.id.menu_about:
	    	showDialog(DIALOG_ABOUT);
	    	return true;
	    default:
	        return  super.onOptionsItemSelected(item);
	    }
	    return  super.onOptionsItemSelected(item);
	}

	@Override
	public void onInflate(ViewStub arg0, View view) {
		 Button btGo = (Button)findViewById(R.id.btGo);
		 btGo.setOnClickListener(this);
		 openView=view;//findViewById(R.id.panel_import);
		
	}
}
