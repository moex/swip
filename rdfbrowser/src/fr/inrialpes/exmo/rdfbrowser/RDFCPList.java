/**
 *   Copyright 2007-2010 Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   RDFCPList.java is part of RDFBrowser.
 *
 *   RDFBrowser is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   RDFBrowser is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with RDFBrowser; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.rdfbrowser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ContentProvider;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ProviderInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import dalvik.system.PathClassLoader;
import fr.inrialpes.exmo.rdfprovider.RdfContentProvider;
import fr.inrialpes.exmo.rdfprovider.RdfContentResolverProxy;

public class RDFCPList extends ListActivity {

	private static final int PROGRESS_DIALOG=1;
	
	private final static String RDF_CP_CLASSNAME = RdfContentProvider.class.getName();
	private ArrayAdapter<String> data;
	private final ArrayList<String> cpList=new ArrayList<String>();
	private final HashMap<String,String> class2authority = new HashMap<String,String>();
	
	private final static int CP_LOADED=1;
    private final class LoadProviders extends Thread {

        //private final Context context;
        private final Handler handler;
        
        public LoadProviders(Context ctx, Handler ha) {
        	//context = ctx;
        	handler = ha;
        }

        public void run() {
        	List<ProviderInfo> list = getPackageManager().queryContentProviders(null, 0, 0);
        	cpList.clear();
    		for (ProviderInfo pInfos : list) {
    			System.out.println(pInfos.name);
    			ClassLoader cl = new PathClassLoader(pInfos.applicationInfo.sourceDir,ClassLoader.getSystemClassLoader());
    			Class<?> providerClass;
    			try {
    				providerClass =cl.loadClass(pInfos.name);
    				try {
    					Class<?> sClass = providerClass.getSuperclass();
    					while (!sClass.equals(ContentProvider.class)) {
    						if (RDF_CP_CLASSNAME.equals(sClass.getName())) {
    							class2authority.put(providerClass.getName(), pInfos.authority);
    							//data.add(providerClass.getName());
    							cpList.add(providerClass.getName());
    							break;
    						}
    						sClass=sClass.getSuperclass();
    					}
    				}
    				catch (ClassCastException e) {
    					System.out.println("not a RdfContentProvider : " + providerClass.getName());
    				}
     
    			} catch (ClassNotFoundException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    		}
             Message msg = new Message();
             msg.what = CP_LOADED;
             handler.sendMessage(msg);
        }
    }
    
    
    
    private final Handler activityHandler = new Handler() {
        public void handleMessage(Message msg) {
             if (msg.what==CP_LOADED) {
            	 dismissDialog(PROGRESS_DIALOG);
            	 data.clear();
            	 for (String s : cpList) data.add(s); 
            	 data.notifyDataSetChanged();
             }
        }
   }; 
    
    
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//RdfContentResolverProxy cr = RdfContentResolverProxy.getInstance(this);
		data = new ArrayAdapter<String>(this,R.layout.cprow,R.id.cprow_tv);
		showDialog(PROGRESS_DIALOG);
		(new LoadProviders(this,activityHandler)).start();
		this.setListAdapter(data);
	}

	protected void onListItemClick(ListView l, View v, int position, long id) {
		String auth = class2authority.get(data.getItem(position));
		Intent i = new Intent();
		i.setClass(this,RDFCPObjects.class);
		i.setData(Uri.parse("content://"+auth));
		startActivity(i);
	}
	
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        	case PROGRESS_DIALOG:
        		ProgressDialog processDialog = new ProgressDialog(this);
        		processDialog.setMessage("Loading providers \n This may take a while...");
        		processDialog.setIndeterminate(true);
        		processDialog.setCancelable(true);
        		return processDialog;
        }
        return null;
    }
}
