/**
 *   Copyright 2007-2010 Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   RDFCPOntologies.java is part of RDFBrowser.
 *
 *   RDFBrowser is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   RDFBrowser is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with RDFBrowser; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.rdfbrowser;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.TextView;
import fr.inrialpes.exmo.rdfprovider.RdfContentResolverProxy;

public class RDFCPOntologies extends Activity {

	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.cp_infos_basic);
	    
	    if (getIntent().getData()==null) {
	    	this.showDialog(1);
	    }
	    else {
	    
		    String providerClass = getIntent().getData().getAuthority();
		    
		    RdfContentResolverProxy cr = RdfContentResolverProxy.getInstance(this);//new RdfContentResolver(this);
		    
		    ViewGroup vg = (ViewGroup) findViewById(R.id.cp_infos_rootpanel);
		    
		    List<Uri> ontos = cr.getOntologies(this.getIntent().getData().getAuthority());
		    TextView tvTitle = new TextView(this);
		    tvTitle.setTextSize(22);
		    tvTitle.setText("Ontologies:");
	    	vg.addView(tvTitle);
	    	
		    for (Uri o : ontos) { 
		    	TextView tv = new TextView(this);
		    	tv.setTextSize(18);
		    	tv.setPadding(5, 2, 0, 3);
		    	tv.setText(o.toString());
		    	vg.addView(tv);
		    }
	    }
	}

	protected Dialog onCreateDialog(int id) {
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setMessage("Nothing to show for this URI")
	       	.setCancelable(false)
	       	.setNeutralButton("OK", new DialogInterface.OnClickListener() {
 	           public void onClick(DialogInterface dialog, int id) {
 	        	  RDFCPOntologies.this.finish();
 	           }
 	       });
    	return builder.create();
	}
	
 
	
	

}