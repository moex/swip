# SWIP: Semantic web in your pocket

SWIP is a research framework for semanticizing Android, i.e. making it use semantic web technologies under the hood.

The documentation can be found at [https://swip.inrialpes.fr](https://swip.inrialpes.fr).

The project is not under active development.
