/**
 *   Copyright 2007-2010 Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   RDFContactProvider.java is part of rdfcontentresolver.
 *
 *   rdfcontentresolver is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   rdfcontentresolver is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with rdfcontentresolver; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.rdfprovider.androidproviders;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Contacts;
import android.provider.Contacts.ContactMethods;
import android.provider.Contacts.People;
import fr.inrialpes.exmo.rdfprovider.ArrayRdfAdapter;
import fr.inrialpes.exmo.rdfprovider.RdfContentProviderWrapper;
import fr.inrialpes.exmo.rdfprovider.RdfCursor;
import fr.inrialpes.exmo.rdfprovider.Statement;

public class RDFContactProvider extends RdfContentProviderWrapper {
	
	//public static final String AUTHORITY = Contacts.AUTHORITY;
	
	public static final Uri TYPE = Uri.parse("http://xmlns.com/foaf/0.1/Person");
	public static final List<Uri> ONTOLOGIES=new ArrayList<Uri>();
	
	static {
		ONTOLOGIES.add(Uri.parse("http://xmlns.com/foaf/0.1/"));
		ONTOLOGIES.add(Uri.parse("http://www.w3.org/2000/01/rdf-schema#"));
	}
	
	
	private final String[] projectionContact = new String[] {People.NAME,People.NUMBER,People.PRIMARY_EMAIL_ID,People.NOTES};
	//private final String[] projectionEmail = new String[] {};
	
	public RDFContactProvider() { 
		super();
	}
	
	public RdfCursor get(Uri uri) {
		String id = uri.getLastPathSegment();
		Uri uriperson = Uri.withAppendedPath(People.CONTENT_URI, id);
		
		ArrayRdfAdapter rdfadapter = new ArrayRdfAdapter();
		ContentResolver cr=getContext().getContentResolver();

		Cursor c = cr.query(uriperson, projectionContact, null, null, null);
		if (c.getCount() > 0) {
			c.moveToNext();
			if (!c.getString(0).equals("")) {
				c.getColumnName(0);
				Statement stmt1 = new Statement(uriperson.toString(),"http://xmlns.com/foaf/0.1/name",c.getString(0));
				rdfadapter.add(stmt1);
			
			}
			if (c.getString(1)!=null && !c.getString(1).equals("")) {
				Statement stmt2 = new Statement(uriperson.toString(),"http://xmlns.com/foaf/0.1/phone","tel:"+c.getString(1));
				rdfadapter.add(stmt2);
			}	
			if (c.getString(2)!=null && !c.getString(2).equals("")) {
				System.out.println(c.getString(2)); 
				Cursor c2=cr.query(Uri.withAppendedPath(ContactMethods.CONTENT_EMAIL_URI, c.getString(2)), new String[]{ContactMethods.DATA}, null, null, null);
				while (c2.moveToNext()) {
					Statement stmt3 = new Statement(uriperson.toString(),"http://xmlns.com/foaf/0.1/mbox",c2.getString(0));
					rdfadapter.add(stmt3);
				}
			}	
			if (c.getString(3)!=null && !c.getString(3).equals("")) {
				Statement stmt4 = new Statement(uriperson.toString(),"http://www.w3.org/2000/01/rdf-schema#comment",c.getString(3));
				rdfadapter.add(stmt4);
			}
			/*if (c.getString(4)!=null && !c.getString(4).equals("")) {
				Statement stmt5 = new Statement(uriperson.toString(),"foaf:"+c.getColumnName(4),c.getString(4));
				rdfadapter.add(stmt5);
			}
//			if (!c.getString(5).equals("")) {
//				Statement stmt6 = new Statement(uriperson.toString(),"foaf:"+c.getColumnName(6),c.getString(6));
//				rdfadapter.add(stmt6);
//			}
//			if (!c.getString(5).equals("")) {
//				Statement stmt6 = new Statement(uriperson.toString(),"foaf:"+c.getColumnName(6),c.getString(6));
//				rdfadapter.add(stmt6);
//			}
//			if (!c.getString(6).equals("")) {
//				Statement stmt7 = new Statement(uriperson.toString(),"foaf:"+c.getColumnName(7),c.getString(7));
//				rdfadapter.add(stmt7);
//			}
//			if (!c.getString(7).equals("")) {
//				Statement stmt8 = new Statement(uriperson.toString(),"foaf:"+c.getColumnName(8),c.getString(8));
//				rdfadapter.add(stmt8);
//			}
//			if (!c.getString(8).equals("")) {
//				Statement stmt9 = new Statement(uriperson.toString(),"foaf:"+c.getColumnName(9),c.getString(9));
//				rdfadapter.add(stmt9);
//			}
//			if (!c.getString(9).equals("")) {
//				Statement stmt10 = new Statement(uriperson.toString(),"foaf:"+c.getColumnName(10),c.getString(10));
//				rdfadapter.add(stmt10);
//			}*/
		}	
		
//		uriphoto = Uri.withAppendedPath(Photos.CONTENT_URI, id);
		
		return rdfadapter;
	}


	@Override
	public List<Uri> getOntologies() {
		return ONTOLOGIES;
	}

	@Override
	public List<Uri> getQueryEntities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Uri> getTypes(Uri uri) {
		return Collections.singletonList(TYPE);
	}

	@Override
	public boolean onCreate() {
		return false;
	}
	
	public List<Uri> getObjects(Uri uri) {
		String[] projectionEvent = new String[] {People._ID};
		ContentResolver cr=getContext().getContentResolver();
		Cursor c = cr.query(People.CONTENT_URI, projectionEvent, null, null, null);
		ArrayList<Uri> res = new ArrayList<Uri>(c.getCount());
		while (c.moveToNext()) {
			res.add(Uri.withAppendedPath(People.CONTENT_URI, c.getString(0)));
		}
		return res;
	}

	@Override
	public String getWrappedAuthority() {
		return "contacts";
	}

}
