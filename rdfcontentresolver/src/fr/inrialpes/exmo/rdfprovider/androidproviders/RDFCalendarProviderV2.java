/**
 *   Copyright 2007-2010 Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   RDFAgendaProviderV2.java is part of rdfcontentresolver.
 *
 *   rdfcontentresolver is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   rdfcontentresolver is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with rdfcontentresolver; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package fr.inrialpes.exmo.rdfprovider.androidproviders;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import fr.inrialpes.exmo.rdfprovider.ArrayRdfAdapter;
import fr.inrialpes.exmo.rdfprovider.RdfContentProviderWrapper;
import fr.inrialpes.exmo.rdfprovider.RdfCursor;
import fr.inrialpes.exmo.rdfprovider.RelationalRdfCursor;
import fr.inrialpes.exmo.rdfprovider.Statement;

public class RDFCalendarProviderV2 extends RdfContentProviderWrapper {

	private static final String AUTHORITY="com.android.calendar";
	private static final Uri EVENTS_URI=Uri.parse("content://"+AUTHORITY+"/events");
	
	
	public static final Uri TYPE = Uri.parse("http://www.w3.org/2002/12/cal#Vevent");
	public static final List<Uri> ONTOLOGIES=new ArrayList<Uri>();
	
	public static final HashMap<String,Uri> EVENT_PROPS = new HashMap<String,Uri>();
	
	static {
		ONTOLOGIES.add(Uri.parse("http://www.w3.org/2002/12/cal#"));
	
		//EVENT_PROPS.put("_id", Uri.parse(""));
		//EVENT_PROPS.put("calendar_id", Uri.parse(""));
		String NS="http://www.w3.org/2002/12/cal#";

		EVENT_PROPS.put("title", Uri.parse(NS+"summary"));
		EVENT_PROPS.put("eventLocation", Uri.parse(NS+"location"));
		EVENT_PROPS.put("description", Uri.parse(NS+"description"));
 
		EVENT_PROPS.put("dtstart", Uri.parse(NS+"dtstart"));
		EVENT_PROPS.put("dtend", Uri.parse(NS+"dtend"));
		EVENT_PROPS.put("eventTimezone", Uri.parse(NS+"tzid"));
		EVENT_PROPS.put("duration", Uri.parse(NS+"duration"));
		EVENT_PROPS.put("organizer", Uri.parse(NS+"organizer"));
		
		/*EVENT_PROPS.put("htmlUri", Uri.parse(NS));
		EVENT_PROPS.put("eventStatus", Uri.parse(NS));
		EVENT_PROPS.put("selfAttendeeStatus", Uri.parse(NS));
		EVENT_PROPS.put("commentsUri", Uri.parse(NS));
		
		EVENT_PROPS.put("allDay", Uri.parse(""));
		EVENT_PROPS.put("visibility", Uri.parse(""));
		EVENT_PROPS.put("transparency", Uri.parse(""));
		EVENT_PROPS.put("hasAlarm", Uri.parse(""));
		EVENT_PROPS.put("hasExtendedProperties", Uri.parse(""));
		EVENT_PROPS.put("rrule", Uri.parse(""));
		EVENT_PROPS.put("rdate", Uri.parse(""));
		EVENT_PROPS.put("exrule", Uri.parse(""));
		EVENT_PROPS.put("exdate", Uri.parse(""));*/

		
	}
	
	public RDFCalendarProviderV2() { 
		super();
	}
	
	public String getWrappedAuthority() {
		return AUTHORITY;
	}

	@Override
	public RdfCursor get(Uri uri) {
		//if (uri.getPath().startsWith("/events")) {
		ContentResolver cr=getContext().getContentResolver();
		Cursor cursor = cr.query(uri, null, null, null, null);
		RdfCursor rdfCursor  =  new RelationalRdfCursor(cursor,EVENT_PROPS,EVENTS_URI,"_id");
		
		return rdfCursor;
	}

	@Override
	public List<Uri> getObjects(Uri uri) {
		String[] projectionEvent = new String[] {"_id"};
		ContentResolver cr=getContext().getContentResolver();
		//if (uri.equals(TYPE)) {
			Cursor c = cr.query(EVENTS_URI, projectionEvent, null, null, null);
			ArrayList<Uri> res = new ArrayList<Uri>(c.getCount());
			while (c.moveToNext()) {
				res.add(Uri.withAppendedPath(EVENTS_URI, c.getString(0)));
			}
			return res;
		//}
		//return null;
	}

	@Override
	public List<Uri> getOntologies() {
		return ONTOLOGIES;
	}

	@Override
	public List<Uri> getQueryEntities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Uri> getTypes(Uri uri) {
		return Collections.singletonList(TYPE);
	}

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		return false;
	}

}
