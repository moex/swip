package fr.inrialpes.exmo.rdfprovider.androidproviders;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import fr.inrialpes.exmo.rdfprovider.ArrayRdfAdapter;
import fr.inrialpes.exmo.rdfprovider.RdfContentProviderWrapper;
import fr.inrialpes.exmo.rdfprovider.RdfCursor;
import fr.inrialpes.exmo.rdfprovider.Statement;

public class RDFAgendaProvider extends RdfContentProviderWrapper {
	
	//public static final String AUTHORITY = "calendar";
	public static final Uri TYPE = Uri.parse("http://www.w3.org/2002/12/cal#Vevent");
	public static final List<Uri> ONTOLOGIES=new ArrayList<Uri>();
	
	static {
		ONTOLOGIES.add(Uri.parse("http://www.w3.org/2002/12/cal#"));
	}
	
	public final Uri eventsUri = Uri.parse("content://calendar/events"); 
  
	@Override
	public RdfCursor get(Uri uri) {
		String id = uri.getLastPathSegment();
		Uri uriEvent = Uri.withAppendedPath(eventsUri, id);
		String[] projectionEvent = new String[] {"title","dtstart","dtend","eventTimezone", "eventLocation", };
		ArrayRdfAdapter rdfadapter2 = new ArrayRdfAdapter();
		ContentResolver cr=getContext().getContentResolver();	
		
		
		Cursor c = cr.query(uriEvent, projectionEvent, null, null, null);
		if(c.getCount()>0){
			c.moveToNext();
			if (!c.getString(0).equals("")) {
				Statement stmt1 = new Statement(uriEvent.toString(),"http://www.w3.org/2002/12/cal#summary",c.getString(0));
				rdfadapter2.add(stmt1);
				
				Statement stmt2 = new Statement(uriEvent.toString(),"http://www.w3.org/2002/12/cal#dtstart",c.getString(1));
				rdfadapter2.add(stmt2);
				
				stmt2 = new Statement(uriEvent.toString(),"http://www.w3.org/2002/12/cal#dtend",c.getString(2));
				rdfadapter2.add(stmt2);
			}
			/*if(!c.getString(3).equals("")){
				Statement stmt3= new Statement (uriEvent.toString(),"foaf:"+c.getColumnName(3),c.getString(3));
				rdfadapter2.add(stmt3);
			}*/
			if(!c.getString(4).equals("")){
				Statement stmt4= new Statement (uriEvent.toString(),"http://www.w3.org/2002/12/cal#location",c.getString(4));
				rdfadapter2.add(stmt4);
			}
		}
		return rdfadapter2;
	}

	public List<Uri> getOntologies() {
		return ONTOLOGIES;
	}

	public List<Uri> getQueryEntities() {
		return null;
	}

	public List<Uri> getTypes(Uri uri) {
		return Collections.singletonList(TYPE);
	}


	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Uri> getObjects(Uri uri) {
		ContentResolver cr=getContext().getContentResolver();	
		Cursor c = cr.query(eventsUri, null, null, null, null);
		System.out.println("uri : "+uri);
		if (c.moveToNext()) {
			for (int i =0 ; i<c.getColumnCount(); i++)
				System.out.println(c.getColumnName(i)+" : " +c.getString(i));
		}
		
		String[] projectionEvent = new String[] {"_id"};
		//ContentResolver cr=getContext().getContentResolver();
		c = cr.query(eventsUri, projectionEvent, null, null, null);
		ArrayList<Uri> res = new ArrayList<Uri>(c.getCount());
		while (c.moveToNext()) {
			res.add(Uri.withAppendedPath(eventsUri, c.getString(0)));
		}
		return res;
	}

	@Override
	public String getWrappedAuthority() {
		return "calendar";
	}

}
