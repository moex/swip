/**
 *   Copyright 2007-2010 J�r�me David, J�r�me Euzenat, Universit� Pierre Mend�s France, INRIA
 *   
 *   RDFContactProviderV2.java is part of rdfcontentresolver.
 *
 *   rdfcontentresolver is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   rdfcontentresolver is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with rdfcontentresolver; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package fr.inrialpes.exmo.rdfprovider.androidproviders;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.Contacts;
import android.util.Log;
import fr.inrialpes.exmo.rdfprovider.RdfContentProviderWrapper;
import fr.inrialpes.exmo.rdfprovider.RdfCursor;
import fr.inrialpes.exmo.rdfprovider.RelationalRdfCursor;

public class RDFContactProviderV2 extends RdfContentProviderWrapper {
	//public static final String AUTHORITY = Contacts.AUTHORITY;
	
	public static final Uri TYPE = Uri.parse("http://xmlns.com/foaf/0.1/Person");
	public static final List<Uri> ONTOLOGIES=new ArrayList<Uri>();
	public static final String defaultNameSpace="http://xmlns.com/foaf/0.1/";
	
	private static final HashMap<String,Uri> CONTACTS_PROPS = new HashMap<String,Uri>();
	private static final HashMap<String,Uri> NAME_PROPS = new HashMap<String,Uri>();
	private static final HashMap<String,Uri> PHONES_PROPS = new HashMap<String,Uri>();
	private static final HashMap<String,Uri> EMAIL_PROPS = new HashMap<String,Uri>();
	private static final HashMap<String,Uri> NOTE_PROPS = new HashMap<String,Uri>();
	private static final HashMap<String,Uri> HOMEPAGE_PROPS = new HashMap<String,Uri>();
	private static final HashMap<String,Uri> IM_PROPS = new HashMap<String,Uri>();
	private static final HashMap<String,Uri> ORGANIZATIONS_PROPS = new HashMap<String,Uri>();
	private static final HashMap<String,Uri> POSTAL_PROPS = new HashMap<String,Uri>();
	
	static {
		ONTOLOGIES.add(Uri.parse("http://xmlns.com/foaf/0.1/"));
		ONTOLOGIES.add(Uri.parse("http://www.w3.org/2000/01/rdf-schema#"));
		
		CONTACTS_PROPS.put(ContactsContract.Contacts.DISPLAY_NAME, Uri.parse(defaultNameSpace+"name"));
		
		NAME_PROPS.put(CommonDataKinds.StructuredName.GIVEN_NAME, Uri.parse(defaultNameSpace+"givenName"));
		NAME_PROPS.put(CommonDataKinds.StructuredName.FAMILY_NAME, Uri.parse(defaultNameSpace+"familyName"));
		/*
		NAME_PROPS.put(CommonDataKinds.StructuredName.MIDDLE_NAME, Uri.parse(defaultNameSpace+"middleName"));		
		NAME_PROPS.put(CommonDataKinds.StructuredName.PHONETIC_GIVEN_NAME, Uri.parse(defaultNameSpace+"phoneticGivenName"));
		NAME_PROPS.put(CommonDataKinds.StructuredName.PHONETIC_FAMILY_NAME, Uri.parse(defaultNameSpace+"phoneticFamilyName"));
		NAME_PROPS.put(CommonDataKinds.StructuredName.PHONETIC_MIDDLE_NAME, Uri.parse(defaultNameSpace+"phoneticMiddleName"));
		NAME_PROPS.put(CommonDataKinds.StructuredName.PREFIX, Uri.parse(defaultNameSpace+"prefixName"));
		NAME_PROPS.put(CommonDataKinds.StructuredName.SUFFIX, Uri.parse(defaultNameSpace+"suffixName"));
		*/
		
		PHONES_PROPS.put(CommonDataKinds.Phone.NUMBER, Uri.parse(defaultNameSpace+"phone"));
		
		EMAIL_PROPS.put(CommonDataKinds.Email.DATA, Uri.parse(defaultNameSpace+"mbox")); //
		
		NOTE_PROPS.put(CommonDataKinds.Note.NOTE, Uri.parse("http://www.w3.org/2000/01/rdf-schema#comment"));
		
		HOMEPAGE_PROPS.put(CommonDataKinds.Website.URL, Uri.parse(defaultNameSpace+"homepage"));
		
		IM_PROPS.put(CommonDataKinds.Im.DATA, Uri.parse(defaultNameSpace+"im"));
		/*
		ORGANIZATIONS_PROPS.put(CommonDataKinds.Organization.DATA, Uri.parse(defaultNameSpace+"orgnization"));
		ORGANIZATIONS_PROPS.put(CommonDataKinds.Organization.COMPANY, Uri.parse(defaultNameSpace+"organizationCompany"));
		ORGANIZATIONS_PROPS.put(CommonDataKinds.Organization.TITLE, Uri.parse(defaultNameSpace+"organizationTitle"));
		ORGANIZATIONS_PROPS.put(CommonDataKinds.Organization.DEPARTMENT, Uri.parse(defaultNameSpace+"organizationDepartment"));
		ORGANIZATIONS_PROPS.put(CommonDataKinds.Organization.JOB_DESCRIPTION, Uri.parse(defaultNameSpace+"organizationJobDescription"));
		ORGANIZATIONS_PROPS.put(CommonDataKinds.Organization.SYMBOL, Uri.parse(defaultNameSpace+"organizationSymbol"));
		ORGANIZATIONS_PROPS.put(CommonDataKinds.Organization.PHONETIC_NAME, Uri.parse(defaultNameSpace+"organizationPhonetcName"));
		ORGANIZATIONS_PROPS.put(CommonDataKinds.Organization.OFFICE_LOCATION, Uri.parse(defaultNameSpace+"organizationOfficeLocation"));
		
		POSTAL_PROPS.put(CommonDataKinds.StructuredPostal.POBOX, Uri.parse(defaultNameSpace+"postalPobox"));
		POSTAL_PROPS.put(CommonDataKinds.StructuredPostal.CITY, Uri.parse(defaultNameSpace+"postalCity"));
		POSTAL_PROPS.put(CommonDataKinds.StructuredPostal.STREET, Uri.parse(defaultNameSpace+"postalStreet"));
		POSTAL_PROPS.put(CommonDataKinds.StructuredPostal.REGION, Uri.parse(defaultNameSpace+"postalRegion"));
		POSTAL_PROPS.put(CommonDataKinds.StructuredPostal.POSTCODE, Uri.parse(defaultNameSpace+"postalPostCode"));
		POSTAL_PROPS.put(CommonDataKinds.StructuredPostal.STREET, Uri.parse(defaultNameSpace+"postalStreet"));
		POSTAL_PROPS.put(CommonDataKinds.StructuredPostal.COUNTRY, Uri.parse(defaultNameSpace+"postalCountry"));
		*/
	}
	
	public RDFContactProviderV2() { 
		super();
	}
	
	public RdfCursor get(Uri uri) {
		//if (!uri.getPath().startsWith("/contacts")) return null;
		
		ContentResolver cr=getContext().getContentResolver();
		Cursor cursor = cr.query(uri, null, null, null, null);
		RdfCursor rdfCursor  =  new RelationalRdfCursor(cursor,CONTACTS_PROPS,Contacts.CONTENT_URI,ContactsContract.Contacts._ID);
	
		String id = uri.getLastPathSegment();
		
		// name
		cursor = cr.query(ContactsContract.Data.CONTENT_URI, null,ContactsContract.Data.RAW_CONTACT_ID +" = ? AND "+ContactsContract.Data.MIMETYPE+"= ?", new String[]{id,CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE}, null);

		if (!cursor.isAfterLast())
			rdfCursor = rdfCursor.andThen(new RelationalRdfCursor(cursor,NAME_PROPS,Contacts.CONTENT_URI,CommonDataKinds.Phone.CONTACT_ID));
		
		// phone
		cursor = cr.query( CommonDataKinds.Phone.CONTENT_URI, null,CommonDataKinds.Phone.CONTACT_ID +" = ?", new String[]{id}, null);
		if (!cursor.isAfterLast())
			rdfCursor = rdfCursor.andThen(new RelationalRdfCursor(cursor,PHONES_PROPS,Contacts.CONTENT_URI,CommonDataKinds.Phone.CONTACT_ID));
		
		// mail
		cursor = cr.query( CommonDataKinds.Email.CONTENT_URI, null,CommonDataKinds.Email.CONTACT_ID +" = ?", new String[]{id}, null);
		if (!cursor.isAfterLast())
			rdfCursor = rdfCursor.andThen(new RelationalRdfCursor(cursor,EMAIL_PROPS,Contacts.CONTENT_URI,CommonDataKinds.Email.CONTACT_ID));
		
		//notes
		String noteWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?"; 
	    String[] noteWhereParams = new String[]{id, ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE}; 
		cursor = cr.query(ContactsContract.Data.CONTENT_URI, null, noteWhere, noteWhereParams, null);
		if (!cursor.isAfterLast())
			rdfCursor = rdfCursor.andThen(new RelationalRdfCursor(cursor,NOTE_PROPS,Contacts.CONTENT_URI,ContactsContract.Data.CONTACT_ID));
		
		// homepage
		cursor = cr.query( ContactsContract.Data.CONTENT_URI, null,ContactsContract.Data.RAW_CONTACT_ID +" = ? AND "+ContactsContract.Data.MIMETYPE+"= ?", new String[]{id,CommonDataKinds.Website.CONTENT_ITEM_TYPE}, null);
		//String[] names = cursor.getColumnNames();
		//for (String n : names) System.out.println(n);
		if (!cursor.isAfterLast())
			rdfCursor = rdfCursor.andThen(new RelationalRdfCursor(cursor,HOMEPAGE_PROPS,Contacts.CONTENT_URI,CommonDataKinds.Phone.CONTACT_ID));
		
		//instant messenger
		String imWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " =?";
		String[] imWhereParams = new String[]{id, ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE};
		cursor = cr.query(ContactsContract.Data.CONTENT_URI, null, imWhere, imWhereParams, null);
		if (!cursor.isAfterLast())
			rdfCursor = rdfCursor.andThen(new RelationalRdfCursor(cursor, IM_PROPS, Contacts.CONTENT_URI, ContactsContract.Data.CONTACT_ID));
		/*
		// organisation
		String orgWhere = ContactsContract.Data.CONTACT_ID + " =? AND " + ContactsContract.Data.MIMETYPE + " =?";
		String[] orgWhereParams = new String[]{id, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE};
		cursor = cr.query(ContactsContract.Data.CONTENT_URI, null, orgWhere, orgWhereParams, null);
		if (!cursor.isAfterLast())
			rdfCursor = rdfCursor.andThen(new RelationalRdfCursor(cursor, ORGANIZATIONS_PROPS, Contacts.CONTENT_URI, ContactsContract.Data.CONTACT_ID, getContext()));
		
		//postal address
		String addrWhere = ContactsContract.Data.CONTACT_ID + " =? AND " + ContactsContract.Data.MIMETYPE + " =?";
		String[] addrWhereParams = new String[]{id, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE};
		cursor = cr.query(ContactsContract.Data.CONTENT_URI, null, addrWhere, addrWhereParams, null);
		if (!cursor.isAfterLast())
			rdfCursor = rdfCursor.andThen(new RelationalRdfCursor(cursor, POSTAL_PROPS, Contacts.CONTENT_URI, ContactsContract.Data.CONTACT_ID, getContext()));
		*/
		return rdfCursor;
	}


	@Override
	public List<Uri> getOntologies() {
		return ONTOLOGIES;
	}

	@Override
	public List<Uri> getQueryEntities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Uri> getTypes(Uri uri) {
		return Collections.singletonList(TYPE);
	}

	@Override
	public boolean onCreate() {
		return false;
	}
	
	public List<Uri> getObjects(Uri uri) {
		String[] projectionEvent = new String[] {Contacts._ID};
		ContentResolver cr=getContext().getContentResolver();
		Cursor c = cr.query(Contacts.CONTENT_URI, projectionEvent, null, null, null);
		ArrayList<Uri> res = new ArrayList<Uri>(c.getCount());
		while (c.moveToNext()) {
			res.add(Uri.withAppendedPath(Contacts.CONTENT_URI, c.getString(0)));
		}
		return res;
	}

	@Override
	public String getWrappedAuthority() {
		return "com.android.contacts";
	}

}
