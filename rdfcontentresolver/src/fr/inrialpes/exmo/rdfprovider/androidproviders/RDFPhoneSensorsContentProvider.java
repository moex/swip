package fr.inrialpes.exmo.rdfprovider.androidproviders;

import java.util.List;

import fr.inrialpes.exmo.rdfprovider.ArrayRdfAdapter;
import fr.inrialpes.exmo.rdfprovider.Statement;
import fr.inrialpes.exmo.rdfprovider.RdfContentProvider;
import fr.inrialpes.exmo.rdfprovider.RdfCursor;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;

public class RDFPhoneSensorsContentProvider extends RdfContentProvider implements LocationListener {
	public static final String defaultNameSpace="http://www.w3.org/2003/01/geo/wgs84_pos#";				//the default name space
	private ArrayRdfAdapter myArrayRdfAdapter = new ArrayRdfAdapter();
	private LocationManager locationManager;
	private Geocoder geocoder;
	private double lon;
	private double lat;
	private String provider;
	
	//the constructor
	public RDFPhoneSensorsContentProvider() { 
		
	}
	
	//add the location information
	public void addLocationInformation() { 
		Statement myStatement;
		locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		provider = locationManager.getBestProvider(criteria, false);
		
		//initialize with the last known location
	    Location lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	    if (lastLocation != null) { 
	    	lon = lastLocation.getLongitude();
			lat = lastLocation.getLatitude();
			//add the statements to the provider
			myStatement = new Statement("_:bn", "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", defaultNameSpace+"Point");
			myArrayRdfAdapter.add(myStatement);
			myStatement = new Statement("_:bn", defaultNameSpace+"long", String.valueOf(lon));
			myArrayRdfAdapter.add(myStatement);
			myStatement = new Statement("_:bn", defaultNameSpace+"lat", String.valueOf(lat));
			myArrayRdfAdapter.add(myStatement);
		
	    } else {
			//add the statements to the provider
			myStatement = new Statement("_:bn", "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", defaultNameSpace+"Point");
			myArrayRdfAdapter.add(myStatement);
			myStatement = new Statement("_:bn", defaultNameSpace+"long", "Longitude not available");
			myArrayRdfAdapter.add(myStatement);
			myStatement = new Statement("_:bn", defaultNameSpace+"lat", "Latitude not available");
			myArrayRdfAdapter.add(myStatement);
		}
	}
	
	@Override
	public RdfCursor get(Uri uri) {
		System.out.println("Return the provider with phone sensors informations");
		addLocationInformation();
		myArrayRdfAdapter.moveToFirst();
		while (this.myArrayRdfAdapter.hasNext()) { 
			Statement st = this.myArrayRdfAdapter.next();
			System.out.println(st.toString());
		}
		return myArrayRdfAdapter;
	}
	
	@Override
	public void onLocationChanged(Location location) {
		lat = location.getLatitude();
		lon = location.getLongitude();
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Uri> getTypes(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Uri> getOntologies() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Uri> getQueryEntities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Uri> getObjects(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

}
