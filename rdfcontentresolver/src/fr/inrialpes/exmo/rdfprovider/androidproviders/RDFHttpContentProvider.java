/**
 * $Id: RDFHttpContentProvider.java$
 *
 * Copyright (C) 2011, INRIA
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

package fr.inrialpes.exmo.rdfprovider.androidproviders;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import fr.inrialpes.exmo.rdfprovider.ArrayRdfAdapter;
import fr.inrialpes.exmo.rdfprovider.RdfContentProvider;
import fr.inrialpes.exmo.rdfprovider.RdfCursor;
/*
 * This class is a Singleton in order to get read of the overhead created by
 * the usage of the Androjena library used to parse RDF files
 */
public class RDFHttpContentProvider extends RdfContentProvider {
	private static RDFHttpContentProvider myRDFHttpContentProvider;					//the HttpRdfContentProvider Singleton
	private static final String fileName = "/sdcard/RDFFile.rdf";					//where to be stored the RDF file
	private String AUTHORITY;														//the authority
	private ArrayRdfAdapter myArrayRdfAdapter;										//the array RDF adapter
	
	//private constructor
	private RDFHttpContentProvider() { 
	}
	
	//create the new Singleton object 
	public static RDFHttpContentProvider createNewHttpRdfContentProvider() { 
																					//check if the object is already created
		if (myRDFHttpContentProvider == null) { 									//if it not
			myRDFHttpContentProvider = new RDFHttpContentProvider();				//create a new object	
		}
		return myRDFHttpContentProvider;											//return the already created object	
	}
	
	@SuppressWarnings("finally")
	@Override
	public RdfCursor get(Uri uri) {
		//Retrieve the file
		URLConnection connexion = null;
		byte data[] = new byte[1024];
		try {
			int count;
			URL url = new URL(uri.toString());										//establish the connection s
			connexion = url.openConnection();
			connexion.addRequestProperty("accept", "application/rdf+xml");
			connexion.connect();
			InputStream input = new BufferedInputStream(url.openStream());
			OutputStream output = new FileOutputStream(fileName);
			while((count=input.read(data)) != -1) { 
				output.write(data, 0, count);
			}
			output.flush();
			output.close();
			input.close();
		} catch(Exception ex) { 
			ex.printStackTrace();
			myRDFHttpContentProvider = null;
			return null;
		}
		
		//Parse the file
		com.hp.hpl.jena.rdf.model.Model model = com.hp.hpl.jena.rdf.model.ModelFactory.createDefaultModel();
		try {
	    	myArrayRdfAdapter = new ArrayRdfAdapter();								//build the ArrayRdfAdapter
			InputStream in = new FileInputStream(fileName);
			if (in == null)
				return null;
			else {
				model.read(in, "");													//read the model
            			ArrayList<com.hp.hpl.jena.rdf.model.Statement> myStatements = (ArrayList<com.hp.hpl.jena.rdf.model.Statement>) 	//get the model's statements
				model.listStatements().toList();
                		for (com.hp.hpl.jena.rdf.model.Statement st : myStatements) {													//iterate through all the statements
                			com.hp.hpl.jena.rdf.model.Resource subj = st.getSubject();													//retrieve the subject, predicate, object from the statement
                    com.hp.hpl.jena.rdf.model.Property pred = st.getPredicate();
                    com.hp.hpl.jena.rdf.model.RDFNode obj =  st.getObject();
                    fr.inrialpes.exmo.rdfprovider.Statement myStatement = new fr.inrialpes.exmo.rdfprovider.Statement(subj.toString(),	//build the new statement and add it
                    													pred.toString(), obj.toString());
                    myArrayRdfAdapter.add(myStatement);
                } //for
                
                File file = new File(fileName);										//delete the file from the sdcard
                file.delete();
			 }//else
		} catch(Exception ex) {
			myRDFHttpContentProvider = null;
			return null;
			/*try { 																													//receive the error code from the remote server
				data = new byte[1024];
				StringBuffer buff = new StringBuffer();
				int responseCode = ((HttpURLConnection) connexion).getResponseCode();													//the response code returned by the HTTP remote server
				InputStream in = ((HttpURLConnection)connexion).getErrorStream();														//the input stream from the remove server
				int count = 0;
				while((count=in.read(data)) != -1) {
					buff.append(data);
				}
				in.close();																												//close the stream
			} catch (IOException e) {
				e.printStackTrace();
			}*/
        } finally { 
        	return myArrayRdfAdapter;
        }
	}

	@Override
	public List<Uri> getObjects(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Uri> getOntologies() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Uri> getQueryEntities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Uri> getTypes(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

}
