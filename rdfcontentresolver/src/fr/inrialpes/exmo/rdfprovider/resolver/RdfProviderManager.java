/**
 *   Copyright 2007-2010 J?r?me David, J?r?me Euzenat, Universit? Pierre Mend?s France, INRIA
 *   
 *   RdfProviderManager.java is part of rdfcontentresolver.
 *
 *   rdfcontentresolver is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   rdfcontentresolver is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with rdfcontentresolver; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.rdfprovider.resolver;
 
import java.io.ByteArrayOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import com.hp.hpl.jena.rdf.model.AnonId;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

import fr.inrialpes.exmo.rdfprovider.IRdfContentProvider;
import fr.inrialpes.exmo.rdfprovider.IRdfCursor;
import fr.inrialpes.exmo.rdfprovider.Statement;
import fr.inrialpes.exmo.rdfprovider.androidproviders.RDFHttpContentProvider;
import fr.inrialpes.exmo.rdfprovider.androidproviders.RDFPhoneSensorsContentProvider;
import fr.inrialpes.exmo.rdfprovider.resolver.IRdfContentResolver;

import android.app.Service;
import android.content.ContentProviderClient;
import android.content.Intent; 
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
 
public class RdfProviderManager extends Service {

	
	@Override
	public boolean onUnbind(Intent intent) {
		//System.err.println("onUnbind in "+this.getClass().getCanonicalName());
		return super.onUnbind(intent);
	}

	private final static HashMap<String,IRdfContentProvider> PROVIDERS = new HashMap<String,IRdfContentProvider>();
	
	public void registerProvider(IRdfContentProvider provider,String authority) {
		synchronized (PROVIDERS) {
			PROVIDERS.put(authority, provider);
		}
		//Log.i(RdfProviderManager.class.getSimpleName(),"Provider "+provider+" for "+authority+" registered");
	}
	
	public void unregisterProvider(String authority) {
		synchronized (PROVIDERS) {
			PROVIDERS.remove(authority);
			//Log.i(RdfProviderManager.class.getSimpleName(),"Provider unregistered for "+authority);
		}
	} 
	
	public void onDestroy() {
		//System.err.println("onDestroy in "+this.getClass().getCanonicalName());
		super.onDestroy();
	}

	public IRdfContentProvider acquireProvider(Uri uri) {
		String authority=uri.getAuthority();
		IRdfContentProvider provider = PROVIDERS.get(authority);
		
		if (uri.getScheme().startsWith("http")) {
			provider=RDFHttpContentProvider.createNewHttpRdfContentProvider().getIRdfContentProvider();
			//Log.d("PROVIDER", "http provider");
		} 
				
		if (provider==null) {
			//Log.e(RdfProviderManager.class.getSimpleName(),"provider null for : " + authority);
			//wake up the content provider
			ContentProviderClient o = getContentResolver().acquireContentProviderClient(authority);
			//System.err.println("provider : "+o);
			
			// Code useful for 1.6
			/*try {
				Intent i = new Intent();
				i.setData(Uri.parse("content://"+authority+"/events/1"));
				this.sendBroadcast(i);
			}
			catch (Exception e) {e.printStackTrace();}*/
		} 
		//Log.e(RdfProviderManager.class.getSimpleName(),"provider for : "+authority+" = "+provider);
		return provider;
	}
	
	public String externalizeResource(String resource, String baseUri/*, String uri*/) { 
		if(resource.startsWith("content://")) {
			return baseUri+resource.substring("content://".length());
		}
		return resource;
	}
	
	//Build the file that must be returned to the user
	public byte[] getRDF(Uri uri, String baseUri, String lang) {
		HashMap<String, Resource> blankNodes = new HashMap<String, Resource>();
		IRdfCursor rc = null;
		Statement st = null;
		IRdfContentProvider provider = null;
		
		try {
			provider = acquireProvider(uri);																														//acquire the provider
			if(provider == null) return null;
			
			try {
				rc = provider.get(uri);		
				if(rc == null) 	return null;
			} catch (Exception ex) { 	return null;	}
			
			try { 						rc.moveToFirst();
			} catch (Exception ex) {	return null;	}
			
			Model model = ModelFactory.createDefaultModel();																											//create a new model
			
			rc.moveToFirst();																																			//iterate among the model statements
			while(rc.hasNext()) { 
				st = rc.next();																													//retrieve the statement
				
				com.hp.hpl.jena.rdf.model.Resource subject = null;
				com.hp.hpl.jena.rdf.model.Property predicate = null;
				com.hp.hpl.jena.rdf.model.Statement stmt = null;
				String objUri = null;
				String subjectUri = st.getSubject().toString();
				String objectUri = st.getObject();
				
				if (subjectUri.startsWith("_:")) { 
					subject = blankNodes.get(subjectUri);
					if (subject == null) { 
						subject = model.createResource(new AnonId(subjectUri));
						blankNodes.put(objectUri, subject);
					}
				} else 
					subject =  model.createResource(externalizeResource(subjectUri, baseUri/*, uri.toString()*/));						//create the subject
				
				predicate =  model.createProperty(externalizeResource(st.getPredicate().toString(), baseUri/*, uri.toString()*/));		//create the predicate
				objUri = externalizeResource (objectUri, baseUri/*, uri.toString()*/);													//create the object
				
				boolean isValid = false;
				try {
					new URL(objUri);
					isValid = true;
				} catch (MalformedURLException e) {	/*e.printStackTrace();*/	}
				
				try {
					if(isValid) {																																		//if the object is a valid URI
						com.hp.hpl.jena.rdf.model.RDFNode object = model.createResource(st.getObject());																//create the object
						stmt = model.createStatement(subject, predicate, object);																						//create the statement
					}
					else {
						if (objectUri.startsWith("_:")) {		//anonymous node
							Resource res = null;
							if (blankNodes.containsKey(objectUri))
								res = blankNodes.get(objectUri);
							else {
								res = model.createResource(new AnonId(objectUri));
								blankNodes.put(objectUri, res);
							}
							stmt = model.createStatement(subject, predicate, res);
						}
						else
							stmt = model.createStatement(subject, predicate, st.getObject());											//create the statement
					}
					model.add(stmt);
				}
				catch (NullPointerException e) {
					//e.printStackTrace();
				}	//add the statement to the model
			}
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			model.write(baos, lang);
			return baos.toByteArray();	
		} catch (RemoteException e) {	/* e.printStackTrace(); */	}
		return null;
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}
	
    private final IRdfContentResolver.Stub binder = new IRdfContentResolver.Stub() {
		@Override
		public void registerProvider(IBinder provider, String authority) throws RemoteException {
			RdfProviderManager.this.registerProvider(IRdfContentProvider.Stub.asInterface(provider),authority);
		}

		@Override
		public void unregisterProvider(String authority) throws RemoteException {
			RdfProviderManager.this.unregisterProvider(authority);
		}

		@Override
		public IBinder acquireProvider(Uri uri) throws RemoteException {
			return RdfProviderManager.this.acquireProvider(uri).asBinder();
		}

		@Override
		public byte[] getRDF(Uri uri, String baseUri, String lang) throws RemoteException {
			return  RdfProviderManager.this.getRDF(uri, baseUri, lang);
		}
    };
}