/**
 *   Copyright 2007-2010 Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   RDFContactProviderV2.java is part of rdfcontentresolver.
 *
 *   rdfcontentresolver is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   rdfcontentresolver is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with rdfcontentresolver; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package fr.inrialpes.exmo.rdfprovider.androidproviders;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.Contacts;
import fr.inrialpes.exmo.rdfprovider.RdfContentProviderWrapper;
import fr.inrialpes.exmo.rdfprovider.RdfCursor;
import fr.inrialpes.exmo.rdfprovider.RelationalRdfCursor;

public class RDFContactProviderV2 extends RdfContentProviderWrapper {
	
	//public static final String AUTHORITY = Contacts.AUTHORITY;
	
	public static final Uri TYPE = Uri.parse("http://xmlns.com/foaf/0.1/Person");
	public static final List<Uri> ONTOLOGIES=new ArrayList<Uri>();
	
	private static final HashMap<String,Uri> CONTACTS_PROPS = new HashMap<String,Uri>();
	private static final HashMap<String,Uri> NAME_PROPS = new HashMap<String,Uri>();
	private static final HashMap<String,Uri> PHONES_PROPS = new HashMap<String,Uri>();
	private static final HashMap<String,Uri> EMAIL_PROPS = new HashMap<String,Uri>();
	private static final HashMap<String,Uri> NOTE_PROPS = new HashMap<String,Uri>();
	private static final HashMap<String,Uri> HOMEPAGE_PROPS = new HashMap<String,Uri>();
	
	static {
		ONTOLOGIES.add(Uri.parse("http://xmlns.com/foaf/0.1/"));
		ONTOLOGIES.add(Uri.parse("http://www.w3.org/2000/01/rdf-schema#"));
		
		CONTACTS_PROPS.put(ContactsContract.Contacts.DISPLAY_NAME, Uri.parse("http://xmlns.com/foaf/0.1/name"));
		
		NAME_PROPS.put(CommonDataKinds.StructuredName.GIVEN_NAME, Uri.parse("http://xmlns.com/foaf/0.1/givenName"));
		NAME_PROPS.put(CommonDataKinds.StructuredName.FAMILY_NAME, Uri.parse("http://xmlns.com/foaf/0.1/familyName"));
		
		PHONES_PROPS.put(CommonDataKinds.Phone.NUMBER, Uri.parse("http://xmlns.com/foaf/0.1/phone"));
		
		EMAIL_PROPS.put(CommonDataKinds.Email.DATA, Uri.parse("http://xmlns.com/foaf/0.1/mbox")); //
		
		NOTE_PROPS.put(CommonDataKinds.Note.NOTE, Uri.parse("http://www.w3.org/2000/01/rdf-schema#comment"));
		
		HOMEPAGE_PROPS.put(CommonDataKinds.Website.URL, Uri.parse("http://xmlns.com/foaf/0.1/homepage"));
	}
	
	
	//private final String[] projectionContact = new String[] {People.NAME,People.NUMBER,People.PRIMARY_EMAIL_ID,People.NOTES};
	//private final String[] projectionEmail = new String[] {};
	
	public RDFContactProviderV2() { 
		super();
	}
	
	public RdfCursor get(Uri uri) {
		
		//if (!uri.getPath().startsWith("/contacts")) return null;
		
		ContentResolver cr=getContext().getContentResolver();
		Cursor cursor = cr.query(uri, null, null, null, null);
		
		RdfCursor rdfCursor  =  new RelationalRdfCursor(cursor,CONTACTS_PROPS,Contacts.CONTENT_URI,ContactsContract.Contacts._ID);
		
		String id = uri.getLastPathSegment();
		
		// name
		cursor = cr.query( ContactsContract.Data.CONTENT_URI, null,ContactsContract.Data.RAW_CONTACT_ID +" = ? AND "+ContactsContract.Data.MIMETYPE+"= ?", new String[]{id,CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE}, null);
		//String[] names = cursor.getColumnNames();
		//for (String n : names) System.out.println(n);
		if (!cursor.isAfterLast())
			rdfCursor = rdfCursor.andThen(new RelationalRdfCursor(cursor,NAME_PROPS,Contacts.CONTENT_URI,CommonDataKinds.Phone.CONTACT_ID));
		
		// phone
		cursor = cr.query( CommonDataKinds.Phone.CONTENT_URI, null,CommonDataKinds.Phone.CONTACT_ID +" = ?", new String[]{id}, null);
		if (!cursor.isAfterLast())
			rdfCursor = rdfCursor.andThen(new RelationalRdfCursor(cursor,PHONES_PROPS,Contacts.CONTENT_URI,CommonDataKinds.Phone.CONTACT_ID));
		
		// mail
		cursor = cr.query( CommonDataKinds.Email.CONTENT_URI, null,CommonDataKinds.Email.CONTACT_ID +" = ?", new String[]{id}, null);
		if (!cursor.isAfterLast())
			rdfCursor = rdfCursor.andThen(new RelationalRdfCursor(cursor,EMAIL_PROPS,Contacts.CONTENT_URI,CommonDataKinds.Email.CONTACT_ID));
			
		//notes
		String noteWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?"; 
	    String[] noteWhereParams = new String[]{id, ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE}; 
		cursor = cr.query(ContactsContract.Data.CONTENT_URI, null, noteWhere, noteWhereParams, null);
		if (!cursor.isAfterLast())
			rdfCursor = rdfCursor.andThen(new RelationalRdfCursor(cursor,NOTE_PROPS,Contacts.CONTENT_URI,ContactsContract.Data.CONTACT_ID));
		
		// homepage
		cursor = cr.query( ContactsContract.Data.CONTENT_URI, null,ContactsContract.Data.RAW_CONTACT_ID +" = ? AND "+ContactsContract.Data.MIMETYPE+"= ?", new String[]{id,CommonDataKinds.Website.CONTENT_ITEM_TYPE}, null);
		//String[] names = cursor.getColumnNames();
		//for (String n : names) System.out.println(n);
		if (!cursor.isAfterLast())
			rdfCursor = rdfCursor.andThen(new RelationalRdfCursor(cursor,HOMEPAGE_PROPS,Contacts.CONTENT_URI,CommonDataKinds.Phone.CONTACT_ID));
		
		
		/*// postal 
		cursor = cr.query( CommonDataKinds.StructuredName.GIVEN_NAME, null,CommonDataKinds.Email.CONTACT_ID +" = ?", new String[]{id}, null);
		rdfCursor = rdfCursor.andThen(new RelationalRdfCursor(cursor,EMAIL_PROPS,Contacts.CONTENT_URI,CommonDataKinds.Email.CONTACT_ID));
		
		// organisation
		cursor = cr.query( CommonDataKinds.Email.CONTENT_URI, null,CommonDataKinds.Email.CONTACT_ID +" = ?", new String[]{id}, null);
		rdfCursor = rdfCursor.andThen(new RelationalRdfCursor(cursor,EMAIL_PROPS,Contacts.CONTENT_URI,CommonDataKinds.Email.CONTACT_ID));
		*/
		
		return rdfCursor;
	}


	@Override
	public List<Uri> getOntologies() {
		return ONTOLOGIES;
	}

	@Override
	public List<Uri> getQueryEntities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Uri> getTypes(Uri uri) {
		return Collections.singletonList(TYPE);
	}

	@Override
	public boolean onCreate() {
		return false;
	}
	
	public List<Uri> getObjects(Uri uri) {
		String[] projectionEvent = new String[] {Contacts._ID};
		ContentResolver cr=getContext().getContentResolver();
		Cursor c = cr.query(Contacts.CONTENT_URI, projectionEvent, null, null, null);
		ArrayList<Uri> res = new ArrayList<Uri>(c.getCount());
		while (c.moveToNext()) {
			res.add(Uri.withAppendedPath(Contacts.CONTENT_URI, c.getString(0)));
		}
		return res;
	}

	@Override
	public String getWrappedAuthority() {
		return "com.android.contacts";
	}

}
