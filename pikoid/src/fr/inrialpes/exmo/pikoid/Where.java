/**
 *   Copyright 2009-2011 Loic Martin, Yu Yang, Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   Where.java is part of pikoid.
 *
 *   pikoid is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   pikoid is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with pikoid; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.pikoid;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import fr.inrialpes.exmo.pikoid.listeners.ButtonListener;
import fr.inrialpes.exmo.pikoid.provider.PikoidItem;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Where extends Activity {
	
	private static final int MAP_VIEW = 1;
	//private static final int NEW_PLACE = 2;
	private static final int SAME_AS = 3;
	
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.where);
		
		// get the buttons
		Button bExif =(Button) findViewById(R.id.ButtonEXIFWhere);
		Button bMap =(Button) findViewById(R.id.ButtonMap);
		//Button bNewPlace=(Button) findViewById(R.id.ButtonNewPlace);
		Button bSameAs=(Button) findViewById(R.id.ButtonSameAsWhere);
		Button bHere=(Button) findViewById(R.id.ButtonHere);
		
		// set listeners on buttons
		ButtonListener listener = new ButtonListener(this);
		bExif.setOnClickListener(listener);
		bMap.setOnClickListener(listener);
		//bNewPlace.setOnClickListener(listener);
		bSameAs.setOnClickListener(listener);
		bHere.setOnClickListener(listener);
	
		// hide the exif button
		bExif.setVisibility(View.INVISIBLE);
	}
	
	
	public void exif(){
		
	}
	
	
	/** display a map view in order to pick an adress **/
	public void mapView(){
		Intent i = new Intent();
		i.setClassName("fr.inrialpes.exmo.pikoid", "fr.inrialpes.exmo.pikoid.GoogleMapView");
		startActivityForResult(i,MAP_VIEW);
	}
	
	
	public void newPlace(){
		
	}
	
	
	/** attribute the same information than an other image **/
	public void sameAs(){
		// put this extra information to distinct the grid's listener type in the GridListener class
		Intent i = new Intent();
		i.putExtra("fr.inrialpes.exmo.pikoid.SameAs", true);
		i.setClassName("fr.inrialpes.exmo.pikoid", "fr.inrialpes.exmo.pikoid.GridPictures");
		startActivityForResult(i,SAME_AS);
	}
	
	
	/** specify that the place of the image is here **/
	public void here(){
		// get the current gps position
		LocationManager myLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		Location myLocation = myLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		if (myLocation==null) {
			myLocation=myLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		}
		if (myLocation==null) {
			Toast.makeText(this, "No Localisation found", 2);
			return;
		}
		double latitude = myLocation.getLatitude();
		double longitude = myLocation.getLongitude();
		
		ContentValues cv = new ContentValues();
		cv.put(PikoidItem.GPS_LATITUDE, latitude);
		cv.put(PikoidItem.GPS_LONGITUDE, longitude);
		cv.put(PikoidItem.URI_WHERE, "");
		
		
		Geocoder geoCoder = new Geocoder(getBaseContext(), Locale.getDefault());
		List<Address> addresses;
		try {
			addresses = geoCoder.getFromLocation(latitude, longitude, 1);
			String adress = "";
			if (addresses.size() > 0) {
				for (int i = 0; i < addresses.get(0).getMaxAddressLineIndex(); i++)
					adress += addresses.get(0).getAddressLine(i) + "\n";
			}		
			
			
			cv.put(PikoidItem.ADRESS, adress);
		
			
		} 
		catch (IOException e) {
			e.printStackTrace();
			// display a pop up when an error occurs during the operation "find local position" 
			showDialog(1);
		}
		getContentResolver().update(Uri.withAppendedPath(PikoidItem.CONTENT_URI, this.getIntent().getData().getLastPathSegment()), cv, null, null);
		// finish and return to Pikoid activity
		setResult(Activity.RESULT_OK);
		finish();
	}
	
	
	public void onActivityResult(int reqCode, int resultCode, Intent data) {
		switch (reqCode) {
			case (MAP_VIEW) :
				if (resultCode == Activity.RESULT_OK) {
					// save gps coordinates in database of pikoid
					ContentValues cv = new ContentValues();
					
					cv.put(PikoidItem.GPS_LATITUDE, data.getDoubleExtra("GPS_latitude", 0));
					cv.put(PikoidItem.GPS_LONGITUDE, data.getDoubleExtra("GPS_longitude", 0));
					cv.put(PikoidItem.ADRESS, data.getStringExtra("adress"));
					cv.put(PikoidItem.URI_WHERE, "");
				
					getContentResolver().update(Uri.withAppendedPath(PikoidItem.CONTENT_URI, this.getIntent().getData().getLastPathSegment()), cv, null, null);
		
					// finish and return to Pikoid activity
					setResult(Activity.RESULT_OK);
					finish();
				}
			break;
			
			case (SAME_AS) :
				if (resultCode == Activity.RESULT_OK) {
					// get the information from the "same" image				
					Uri uriSameAs = Uri.withAppendedPath(PikoidItem.CONTENT_URI, data.getData().getLastPathSegment());
					String[] projection = new String[] { PikoidItem.URI_WHERE, PikoidItem.GPS_LATITUDE, PikoidItem.GPS_LONGITUDE, PikoidItem.ADRESS };
					Cursor cursor = getContentResolver().query(uriSameAs,projection,null,null,null);
					cursor.moveToFirst();
					ContentValues cv = new ContentValues();
					
					// if the uri Where of the "same" image is setted 
					if (!cursor.getString(0).equals("")){
						cv.put(PikoidItem.URI_WHERE, cursor.getString(0));
						cv.put(PikoidItem.GPS_LATITUDE, 0);
						cv.put(PikoidItem.GPS_LONGITUDE, 0);
						cv.put(PikoidItem.ADRESS, "");
					}
					else{
						cv.put(PikoidItem.GPS_LATITUDE, cursor.getDouble(1));
						cv.put(PikoidItem.GPS_LONGITUDE, cursor.getDouble(2));
						cv.put(PikoidItem.ADRESS, cursor.getString(3));						
						cv.put(PikoidItem.URI_WHERE, "");
					}
						
					getContentResolver().update(Uri.withAppendedPath(PikoidItem.CONTENT_URI, this.getIntent().getData().getLastPathSegment()), cv, null, null);
					
					// finish and return to Pikoid activity
					setResult(Activity.RESULT_OK);
					finish();
				}
				else{
					// nothing to do because the user canceled his action
				}
			break;
		}
	}
	
	
	/** display a pop up **/
	public Dialog onCreateDialog(int id) {
        switch (id) {
        	case 1:
        		return new AlertDialog.Builder(this)   
            		.setTitle(R.string.where_popup_error)
            		.setIcon(android.R.drawable.ic_dialog_alert)
            		.setPositiveButton(R.string.general_ok, new DialogInterface.OnClickListener() {public void onClick(DialogInterface dialog, int whichButton) {}})
            		.create();
        }
        return null;
    }
}
