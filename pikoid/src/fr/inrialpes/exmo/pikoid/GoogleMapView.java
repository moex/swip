/**
 *   Copyright 2009-2011 Loic Martin, Yu Yang, Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   GoogleMapView.java is part of pikoid.
 *
 *   pikoid is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   pikoid is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with pikoid; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.pikoid;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public final class GoogleMapView extends MapActivity {

	 private MapView mMapView;	 
	 private MapController mc;
	 private GeoPoint p;
	 private MapOverlay mapOverlay;

	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapview);
        mMapView=(MapView) findViewById(R.id.googleMapView);

        mMapView.setClickable(true);
        mMapView.setEnabled(true);
        mMapView.setBuiltInZoomControls(true);
        mMapView.displayZoomControls(true);
        mMapView.setSatellite(false);
        
//        LocationManager myLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
//		Location myLocation = myLocationManager.getLastKnownLocation(myLocationManager.GPS_PROVIDER);
//		double latitude = myLocation.getLatitude();
//		double longitude = myLocation.getLongitude();
        double latitude =45.19227 ;
		double longitude = 5.77142;
        p = new GeoPoint((int) (latitude * 1E6), (int) (longitude * 1E6));
        
        mc = mMapView.getController();
        mc.setZoom(14);
        mc.setCenter(p);

    
        
        mapOverlay = new MapOverlay(this);
        List<Overlay> listOfOverlays = mMapView.getOverlays();
        listOfOverlays.clear();
        listOfOverlays.add(mapOverlay); 
        mMapView.invalidate();
        
    }

    protected boolean isRouteDisplayed() { return false; }
    
    public class MapOverlay extends Overlay {
		public GoogleMapView googleMapView;
		public String add;
		List<Address> addresses;
		
		public MapOverlay(GoogleMapView gmv) {
			super();
			googleMapView = gmv;                                                                                                                                                                                                                                                 
		}
		@Override
		public boolean onTap(final GeoPoint p, final MapView mapView) {
			Geocoder geoCoder = new Geocoder(getBaseContext(), Locale.getDefault());
			try {
				addresses = geoCoder.getFromLocation(p.getLatitudeE6() / 1E6, p.getLongitudeE6() / 1E6, 1);

				add = "";
				if (addresses.size() > 0) {
					for (int i = 0; i < addresses.get(0).getMaxAddressLineIndex(); i++)
						add += addresses.get(0).getAddressLine(i) + "\n";
				}
				new AlertDialog.Builder(mapView.getContext())
				    .setMessage(add)
				    .setPositiveButton(R.string.general_ok,new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								Intent i = new Intent();
								i.putExtra("GPS_latitude",p.getLatitudeE6() / 1E6);
								i.putExtra("GPS_longitude",p.getLongitudeE6() / 1E6);
								i.putExtra("adress",add);
								googleMapView.setResult(RESULT_OK, i);
								googleMapView.finish();
								
							}
						}).setNegativeButton(R.string.general_cancel, null).show();
				

			} catch (IOException e) {
				e.printStackTrace();
			}
			return super.onTap(p, mapView);
		}
		public boolean draw(Canvas canvas, MapView mapView, boolean shadow, long when) 
		        {
		            super.draw(canvas, mapView, shadow);                   
		            //---translate the GeoPoint to screen pixels---
		            Point screenPts = new Point();
		            mapView.getProjection().toPixels(p, screenPts);
		 
		            //---add the marker---
		            Bitmap bmp = BitmapFactory.decodeResource(
		                getResources(), R.drawable.pushpin);            
		            canvas.drawBitmap(bmp, screenPts.x, screenPts.y-50, null);         
		            return true;
		        }
		
//		public boolean onTouchEvent(MotionEvent event, MapView mapView) {
//			if (event.getAction() == 1) {
//			p = mapView.getProjection().fromPixels((int) event.getX(), (int) event.getY());
//			return true;
//			} else return false;		  
//		}
		
	} 
    
	    /** Create the Options Menu **/
	    public boolean onCreateOptionsMenu(Menu m){
	    	m.add(0, 0, 0, R.string.where_satellite).setIcon(android.R.drawable.ic_menu_compass);
	    	m.add(0, 1, 1, R.string.where_plan).setIcon(android.R.drawable.ic_menu_mapmode);
			return true;
		}
	    
	    /** Action made by the option selected in the menu **/
	    public boolean onOptionsItemSelected(MenuItem mi){
	    	if(mi.getItemId()==0){
	    		mMapView.setSatellite(true);
	    		mMapView.setStreetView(true);
	    	}
	    	if(mi.getItemId()==1){
	    		mMapView.setSatellite(false);
	    		mMapView.setStreetView(false);
	    	}
	    	return false;
		}

}



