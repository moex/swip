/**
 *   Copyright 2009-2011 Loic Martin, Yu Yang, Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   PickEvent.java is part of pikoid.
 *
 *   pikoid is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   pikoid is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with pikoid; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.pikoid;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import fr.inrialpes.exmo.pikoid.listeners.ListViewListener;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

public class PickEvent extends ListActivity {
	
	private TextView mDateDisplay;
	private Button mPickDate;
	private ListView listView;
	private int mYear;
	private int mMonth; 
	private int mDay;
	public String[] eventsUriId;
	public Uri eventsUri = Uri.parse("content://calendar/events");	 
	static final int DATE_DIALOG_ID = 0;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.pickevent);
   
	    // get the views
	    mDateDisplay = (TextView) findViewById(R.id.dateDisplay);
	    mPickDate = (Button) findViewById(R.id.pickDate);
	    listView = getListView(); 
	    listView.setTextFilterEnabled(true);

	    // get the current date
	    final Calendar c = Calendar.getInstance();
	    mYear = c.get(Calendar.YEAR);
	    mMonth = c.get(Calendar.MONTH);
	    mDay = c.get(Calendar.DAY_OF_MONTH);

	    // display the current date and the list of events associated
	    updateDisplay();
	   
	    // add a click listener to the button
	    mPickDate.setOnClickListener(new View.OnClickListener() {
	    	public void onClick(View v) {showDialog(DATE_DIALOG_ID);}
	    });

	    // add a listener to the listView
	    ListViewListener l = new ListViewListener(this);
	    listView.setOnItemClickListener(l);
	}
	
	
	/** updates the date we display in the TextView and the events in the listView **/
    private void updateDisplay() {
        if(Locale.getDefault().equals(Locale.FRANCE)){
        	mDateDisplay.setText(new StringBuilder()
            	// Month is 0 based so add 1           	
            	.append(mDay).append("-")
            	.append(mMonth + 1).append("-")
            	.append(mYear).append(" "));

        }
        else{
        	mDateDisplay.setText(new StringBuilder()
            	// Month is 0 based so add 1
                .append(mMonth + 1).append("-")
                .append(mDay).append("-")
                .append(mYear).append(" "));
        
        }
    	String[] newList = getEvents();
        if(newList.length == 0)
        newList = new String[]{ this.getResources().getString(R.string.pickEvent_noEvent) };
        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, newList));
    }
    
    
    /** update the list of events and uriId in accordance with the date selected in the date picker **/
    private String[] getEvents(){
    	//convert in milliseconds the date selected with the date picker
    	Date d = new Date(mYear-1900,mMonth,mDay);
    	long time = d.getTime();
    	long t = 86399999; //represents the time 23:59:59 to add to the variable time in order to have the end of the day selected
        
    	// SQL query to get the events
        String[] projection = new String[] { "_id","title","dtstart","dtend" };
    	String where = "dtstart > "+time+" AND dtstart < "+(time+t)+ //if the event starts at the day selected
    				" OR dtstart < "+time+" AND dtend > "+time; //if the event starts before the day selected and finish at the day selected or later 
    	Cursor cursor = getContentResolver().query(eventsUri,projection, where, null, "title ASC");
        String [] newList = new String[cursor.getCount()];
        eventsUriId = new String[cursor.getCount()];
        int i = 0;
        while(cursor.moveToNext()){
    		newList[i] = cursor.getString(1);;
    		eventsUriId[i] = cursor.getString(0);
    		i++;
    	}
        cursor.close();
 	   	
    	return newList;
    }
      
    
    /** the callback received when the user "sets" the date in the dialog **/
    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, 
                                      int monthOfYear, int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                    updateDisplay();
                }
    };
    
    
    /** creates a pop up with the date picker **/
    protected Dialog onCreateDialog(int id) {
	    switch (id) {
	    case DATE_DIALOG_ID:
	        return new DatePickerDialog(this, mDateSetListener, mYear, mMonth, mDay);
	    }
	    return null;
	}  
    
    
    /** Create the Options Menu **/
    public boolean onCreateOptionsMenu(Menu m){
		m.add(0, 0, 0, R.string.menu_newEvent).setIcon(android.R.drawable.ic_menu_add);
		return true;
	}
    
    /** Action made by the option selected in the menu **/
    public boolean onOptionsItemSelected(MenuItem mi){
    	if(mi.getItemId()==0){
    		// call the class EditEvent to create an event
    		Intent i = new Intent();
    		i.setClassName("com.htc.calendar","com.htc.calendar.EditEvent");
    		i.setAction(Intent.ACTION_EDIT);
    		startActivityForResult(i,1);	
    	}
		return false;
	}
    
    
    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
		switch (reqCode) {
		    case (1) :
		    	// force to sleep 2 seconds to be sure that the insert is finish
		    	try {Thread.sleep(2000);} catch (InterruptedException e) {e.printStackTrace();}
		    	
				updateDisplay();
		    	break;
		  }
	}
}
