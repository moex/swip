/**
 *   Copyright 2009-2011 Loic Martin, Yu Yang, Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   When.java is part of pikoid.
 *
 *   pikoid is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   pikoid is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with pikoid; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.pikoid;

import java.util.Date;
import fr.inrialpes.exmo.pikoid.listeners.ButtonListener;
import fr.inrialpes.exmo.pikoid.provider.PikoidItem;
import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class When extends Activity{
	
	final static int SELECT_EVENT = 1;
	final static int NEW_EVENT = 2;
	final static int SAME_AS = 3;
	public Uri eventsUri = Uri.parse("content://calendar/events");	
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.when);
		
		//get the buttons
		Button bExif = (Button) findViewById(R.id.ButtonEXIFWhen);
		Button bCalendar = (Button) findViewById(R.id.ButtonCalendar);
		Button bNewEvent = (Button) findViewById(R.id.ButtonNewEvent);
		Button bSameAs = (Button) findViewById(R.id.ButtonSameAsWhen);
		Button bNow = (Button) findViewById(R.id.ButtonNow);

		//set listeners on buttons
		ButtonListener listener = new ButtonListener(this);
		bExif.setOnClickListener(listener);
		bCalendar.setOnClickListener(listener);
		bNewEvent.setOnClickListener(listener);
		bSameAs.setOnClickListener(listener);
		bNow.setOnClickListener(listener);

		// hide the exif button
		bExif.setVisibility(View.INVISIBLE);
	}
	
	
	public void exif(){
		
	}
	
	
	/** choose an event among events stored on the phone **/
	public void calendar(){
		//open the class PickEvent
		/*Intent i = new Intent();
		i.setAction(Intent.ACTION_INSERT);
		i.setData(Uri.parse("content://com.android.calendar/events"));//CalendarContract.Events.CONTENT_URI);
		startActivityForResult(i,SELECT_EVENT);*/
		/*Intent i = new Intent(this,PickEvent.class);	
		startActivityForResult(i,SELECT_EVENT);*/
		
		long startMillis=System.currentTimeMillis();
		Uri.Builder builder = Uri.parse("content://com.android.calendar/calendars").buildUpon();
		builder.appendPath("time");
		ContentUris.appendId(builder, startMillis);
		Intent intent = new Intent(Intent.ACTION_VIEW)
		    .setData(builder.build());
		startActivityForResult(intent,SELECT_EVENT);
		
		
	}
	
	
	public static final String[] EVENT_PROJECTION = new String[] {"_id"};
	Uri eventUri=null;
	/** create a new event by calling the edit action of the calendar **/
	public void newEvent(){
		
		Cursor cur = getContentResolver().query(Uri.parse("content://com.android.calendar/calendars"), EVENT_PROJECTION, null, null, null);
		cur.moveToFirst();
		int calId = cur.getInt(0);
		cur.close();
		//create event 
		/*Intent i = new Intent();
		i.setClassName("com.htc.calendar","com.htc.calendar.EditEvent");
		i.setAction(Intent.ACTION_EDIT);*/
		ContentValues values = new ContentValues();
		values.put("dtstart", System.currentTimeMillis());
		values.put("dtend", System.currentTimeMillis());
		values.put("calendar_id", calId);
		values.put("title", "evt");
		//values.put("description", "Group workout");
		values.put("eventTimezone", "Europe/Paris");

		
		
		eventUri = getContentResolver().insert(Uri.parse("content://com.android.calendar/events"), values);
		
		Intent i = new Intent();//Intent.ACTION_INSERT works but does not go back
		i.setAction(Intent.ACTION_EDIT);
		i.putExtra("title", "New Event");

		//i.addCategory(Intent.CA);
		System.out.println(eventUri);
		i.setData(eventUri/*Uri.parse("content://com.android.calendar/events")*/);
		startActivityForResult(i,NEW_EVENT);
	}
	
	
	/** attribute the same information than an other image **/
	public void sameAs(){
		// put this extra information to distinct the grid's listener type in the GridListener class
		Intent i = new Intent();
		i.putExtra("fr.inrialpes.exmo.pikoid.SameAs", true);
		i.setClassName("fr.inrialpes.exmo.pikoid", "fr.inrialpes.exmo.pikoid.GridPictures");
		startActivityForResult(i,SAME_AS);
	}
	
	
	/** specify that the moment of the image is now **/
	public void now(){		
		Date d = new Date();
		long now = d.getTime();
		                                                                                                                                                        
		// save the "now" information in pikoid database
		ContentValues cv = new ContentValues();
		cv.put(PikoidItem.DATE, now);
		cv.put(PikoidItem.URI_WHEN, "");
		getContentResolver().update(Uri.withAppendedPath(PikoidItem.CONTENT_URI, this.getIntent().getData().getLastPathSegment()), cv, null, null);
		
		// finish and return to Pikoid activity
		setResult(Activity.RESULT_OK);
		finish();
	}
	
	
	/** called when an activity, called by this class, is finished **/
	public void onActivityResult(int reqCode, int resultCode, Intent data) {
		switch (reqCode) {
			case (SELECT_EVENT) :
				if (resultCode == Activity.RESULT_OK) {
					Log.e("RESULTAT Select event", data.getDataString());
					
					// save the event's uri in pikoid database
					ContentValues cv = new ContentValues();
					cv.put(PikoidItem.URI_WHEN, data.getDataString());
					cv.put(PikoidItem.DATE, 0);
					getContentResolver().update(Uri.withAppendedPath(PikoidItem.CONTENT_URI, this.getIntent().getData().getLastPathSegment()), cv, null, null);
				
					// finish and return to Pikoid activity
					setResult(Activity.RESULT_OK);
					finish();
				}
				else{
					// nothing to do because the user canceled his action
				}
			break;
			
			case (NEW_EVENT) :
				if(resultCode==Activity.RESULT_OK){
					// the event has been inserted and the user has to select it with the calendar
					System.err.println("event uri : "+eventUri);
					
				}
				else{
					// nothing to do because the user canceled his action
				}
			break;
			
			case (SAME_AS) :
				if (resultCode == Activity.RESULT_OK) {
					// get the information from the "same" image				
					Uri uriSameAs = Uri.withAppendedPath(PikoidItem.CONTENT_URI, data.getData().getLastPathSegment());
					String[] projection = new String[] { PikoidItem.URI_WHEN, PikoidItem.DATE };
					Cursor cursor = getContentResolver().query(uriSameAs,projection,null,null,null);
					cursor.moveToFirst();
					ContentValues cv = new ContentValues();
					
					// if the uri When of the "same" image is setted 
					if (!cursor.getString(0).equals("")){
						cv.put(PikoidItem.URI_WHEN, cursor.getString(0));
						cv.put(PikoidItem.DATE, 0);
					}
					else{
						cv.put(PikoidItem.DATE, cursor.getString(1));
						cv.put(PikoidItem.URI_WHEN, "");
					}
						
					getContentResolver().update(Uri.withAppendedPath(PikoidItem.CONTENT_URI, this.getIntent().getData().getLastPathSegment()), cv, null, null);
					
					// finish and return to Pikoid activity
					setResult(Activity.RESULT_OK);
					finish();
				}
				else{
					// nothing to do because the user canceled his action
				}
			break;
		}
	}
}
