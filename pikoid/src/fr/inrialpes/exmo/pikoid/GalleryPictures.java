/**
 *   Copyright 2009-2011 Loic Martin, Yu Yang, Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   GalleryPictures.java is part of pikoid.
 *
 *   pikoid is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   pikoid is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with pikoid; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.pikoid;

import java.util.Vector;

import fr.inrialpes.exmo.pikoid.provider.PikoidItem;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.Gallery.LayoutParams;

public class GalleryPictures extends Activity implements OnItemClickListener{
 


	//static because we have to change the image background in activity Pikoid (activity translucent)
	static Gallery gallery;
	
	private PikoidImgAdapter imageAdapter;
	
    public void onCreate(Bundle savedInstanceState)
    {    
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery);
        
        //initialization of the gallery 
        gallery = (Gallery) findViewById(R.id.gallery);
        imageAdapter = new PikoidImgAdapter(this, new Gallery.LayoutParams(Gallery.LayoutParams.FILL_PARENT,Gallery.LayoutParams.FILL_PARENT));       
        gallery.setAdapter(imageAdapter);
        
        //start the gallery at the right image
        int position = getIntent().getIntExtra("fr.inrialpes.exmo.pikoid.Position", 0);
        gallery.setSelection(position);
        gallery.setOnItemClickListener(this);

        
    }
    
    /** Create the Options Menu **/
    public boolean onCreateOptionsMenu(Menu m){
		m.add(0, 0, 0, R.string.menu_annot).setIcon(android.R.drawable.ic_menu_edit);
		m.add(0, 1, 1, R.string.menu_properties).setIcon(android.R.drawable.ic_menu_info_details);
		return true;
	}
    
    /** Action made by the option selected in the menu **/
    public boolean onOptionsItemSelected(MenuItem mi){
    	if(mi.getItemId()==0){
    		int position = gallery.getSelectedItemPosition();
    		//ImageAdapterGallery iA = (ImageAdapterGallery) gallery.getAdapter();  		
    		Intent i = new Intent(this,Pikoid.class);
    		i.setData(Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, imageAdapter.getItemId(position)+""));  		
    		startActivity(i);	
    	}
    	else if(mi.getItemId()==1){
    		int position = gallery.getSelectedItemPosition();
    		//ImageAdapterGallery iA = (ImageAdapterGallery) gallery.getAdapter();
    		Intent i = new Intent(this,ImageProperties.class);
    		i.setData(Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, imageAdapter.getItemId(position)+""));
    		startActivity(i);	
    	}
    	return false;
	}

	public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {  		
		Intent i = new Intent(this,Pikoid.class);
		i.setData(Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, String.valueOf(id)));  		
		startActivity(i);
		
	}





}
