/**
 *   Copyright 2009-2011 Loic Martin, Yu Yang, Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   ButtonListener.java is part of pikoid.
 *
 *   pikoid is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   pikoid is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with pikoid; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.pikoid.listeners;

import fr.inrialpes.exmo.pikoid.Pikoid;
import fr.inrialpes.exmo.pikoid.R;
import fr.inrialpes.exmo.pikoid.Text;
import fr.inrialpes.exmo.pikoid.What;
import fr.inrialpes.exmo.pikoid.When;
import fr.inrialpes.exmo.pikoid.Where;
import fr.inrialpes.exmo.pikoid.Who;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

public class ButtonListener implements OnClickListener  {

	private Pikoid pikoid;
	private When when;
	private Who who;
	private Where where;
	private What what;
	private Text text;


	public final static int ACT_WHO = 1;
	public final static int ACT_WHEN = 2;
	public final static int ACT_WHAT = 3;
	public final static int ACT_WHERE = 4;
	public final static int ACT_TEXT = 5;
	
	public ButtonListener(Pikoid p){
		super();
		pikoid = p;
	}
	public ButtonListener(When w){
		super();
		when = w;
	}
	public ButtonListener(Who w){
		super();
		who = w;
	}

	public ButtonListener(Where w){
		super();
		where = w;
	}

	public ButtonListener(What w){
		super();
		what = w;
	}
	public ButtonListener(Text t){
		super();
		text = t;
	}

	public void onClick(View v) {
		
		//////////////////////////////////////
		//	Buttons of the activity Pikoid	//
		//////////////////////////////////////
		if (v.getId()==R.id.ButtonWho){ 
			Intent i = new Intent(v.getContext(), Who.class);
			i.setData(pikoid.uriAnnotedImage);
			pikoid.startActivityForResult(i,ACT_WHO);
		}
		else if (v.getId()==R.id.ButtonWhen){ 
			Intent i = new Intent(v.getContext(), When.class);
			i.setData(pikoid.uriAnnotedImage);
			pikoid.startActivityForResult(i,ACT_WHEN);
		}
		else if (v.getId()==R.id.ButtonWhat){ 
			Intent i = new Intent(v.getContext(), What.class);
			i.setData(pikoid.uriAnnotedImage);
			pikoid.startActivityForResult(i,ACT_WHAT);
		}
		else if (v.getId()==R.id.ButtonWhere){ 
			Intent i = new Intent(v.getContext(), Where.class);
			i.setData(pikoid.uriAnnotedImage);
			pikoid.startActivityForResult(i,ACT_WHERE);
		}
		else if (v.getId()==R.id.ButtonPrevious_pic){ 
			pikoid.previousImage();			
		}
		else if (v.getId()==R.id.ButtonText){ 
			Intent i = new Intent(v.getContext(), Text.class);
			i.setData(pikoid.uriAnnotedImage);
			pikoid.startActivityForResult(i,ACT_TEXT);
		}
		else if (v.getId()==R.id.ButtonNext_pic){ 
			pikoid.nextImage();
		}		
		
			
		//////////////////////////////////////
		//	Buttons of the activity Who		//
		//////////////////////////////////////
		else if (v.getId()==R.id.ButtonContactsbook){
			who.contactsBook(); 
		}
		else if (v.getId()==R.id.ButtonNewPerson){
			who.newPerson();
		}
		else if (v.getId()==R.id.ButtonSameAsWho){
			who.sameAs();
		}
		else if (v.getId()==R.id.ButtonMe){
			who.me();
		}
		
		
		//////////////////////////////////////
		//	Buttons of the activity When	//
		//////////////////////////////////////
		else if (v.getId()==R.id.ButtonEXIFWhen){
			when.exif();
		}
		else if (v.getId()==R.id.ButtonCalendar){
			when.calendar();
		}
		else if (v.getId()==R.id.ButtonNewEvent){
			when.newEvent();
		}
		else if (v.getId()==R.id.ButtonSameAsWhen){
			when.sameAs();
		}
		else if (v.getId()==R.id.ButtonNow){
			when.now();
		}
		
		
		//////////////////////////////////////
		//	Buttons of the activity Where	//
		//////////////////////////////////////
		else if (v.getId()==R.id.ButtonEXIFWhere){
			where.exif();
		}
		else if (v.getId()==R.id.ButtonMap){
			where.mapView();
		}
		/*
		else if (v.getId()==R.id.ButtonNewPlace){
			where.newPlace();
		}
		*/
		else if (v.getId()==R.id.ButtonSameAsWhere){
			where.sameAs();
		}
		else if (v.getId()==R.id.ButtonHere){
			where.here();
		}
		
		
		//////////////////////////////////////
		//	Buttons of the activity What	//
		//////////////////////////////////////
		else if (v.getId()==R.id.ButtonSameAsWhat){
			what.sameAs();
		}
		else if (v.getId()==R.id.ButtonOKWhat){
			what.ok();
		}
		
		
		//////////////////////////////////////
		//	Buttons of the activity Text	//
		//////////////////////////////////////
		else if (v.getId()==R.id.ButtonSameAsText){
			text.sameAs();
		}
		else if (v.getId()==R.id.ButtonOKText){
			text.ok();
		}
		
	}

}
