/**
 *   Copyright 2009-2011 Loic Martin, Yu Yang, Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   ListViewListener.java is part of pikoid.
 *
 *   pikoid is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   pikoid is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with pikoid; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.pikoid.listeners;

import fr.inrialpes.exmo.pikoid.PickEvent;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class ListViewListener implements OnItemClickListener{

	private PickEvent pickEvent;
	
	public ListViewListener(PickEvent p){
		pickEvent = p;
	}
	
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		Uri eventUri = Uri.withAppendedPath(pickEvent.eventsUri, pickEvent.eventsUriId[position]);
		Intent i = new Intent();
		i.setData(eventUri);
		pickEvent.setResult(Activity.RESULT_OK, i);
		pickEvent.finish();
	}
}
