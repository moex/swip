/**
 *   Copyright 2009-2011 Loic Martin, Yu Yang, Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   Pikoid.java is part of pikoid.
 *
 *   pikoid is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   pikoid is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with pikoid; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.pikoid;

import fr.inrialpes.exmo.pikoid.R;
import fr.inrialpes.exmo.pikoid.listeners.ButtonListener;
import fr.inrialpes.exmo.pikoid.provider.PikoidItem;
import fr.inrialpes.exmo.rdfprovider.RdfContentResolverProxy;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import android.widget.ImageButton;

public class Pikoid extends Activity{
	
	public Uri uriAnnotedImage;
	private ImageButton bPrevious; 
	private ImageButton bNext ;
	private Button bText ;
	private Button bWhen ;
	private Button bWhat ;
	private Button bWhere;
	private Button bWho;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        // Useful because resolver need some time to be registered
        RdfContentResolverProxy.getInstance(this);
        setContentView(R.layout.pikoid);
        
        //get the buttons
        bWho = (Button) findViewById(R.id.ButtonWho);
        bWhen = (Button) findViewById(R.id.ButtonWhen);
        bWhat = (Button) findViewById(R.id.ButtonWhat);
        bWhere = (Button) findViewById(R.id.ButtonWhere);
        bPrevious = (ImageButton) findViewById(R.id.ButtonPrevious_pic);
        bText = (Button) findViewById(R.id.ButtonText);
        bNext = (ImageButton) findViewById(R.id.ButtonNext_pic);
        
        //add image on buttons previous and next
        bPrevious.setImageResource(android.R.drawable.ic_media_previous);
        bNext.setImageResource(android.R.drawable.ic_media_next);
      
        //initialization of the button listener
        ButtonListener listener = new ButtonListener(this);
        
        //add the listener on buttons
        bWho.setOnClickListener(listener);
        bWhen.setOnClickListener(listener);
        bWhat.setOnClickListener(listener);
        bWhere.setOnClickListener(listener);
        bPrevious.setOnClickListener(listener);
        bText.setOnClickListener(listener);
        bNext.setOnClickListener(listener);

        //set the uri of the image being annotated 
        //the uri has this form : content://media/external/images/media/#
        uriAnnotedImage = getIntent().getData();
    	
        
    }
    /** Create the Options Menu **/
    public boolean onCreateOptionsMenu(Menu m){
    	m.add(0, 0, 0, R.string.menu_properties).setIcon(android.R.drawable.ic_menu_info_details);
    	m.add(0, 1, 1, R.string.menu_RDF_properties).setIcon(android.R.drawable.ic_menu_info_details);
		return true;
	}
    
    
    /** Action made by the option selected in the menu **/
    public boolean onOptionsItemSelected(MenuItem mi){
    	if(mi.getItemId()==0){
    		Intent i = new Intent(this,ImageProperties.class);
    		i.setData(uriAnnotedImage);
    		startActivity(i);
    	}
    	
    	else if(mi.getItemId()==1){
    		Intent i = new Intent();
    		i.setAction("android.intent.action.VIEW_RDF");//Intent.ACTION_VIEW);//this,RDFImageProperties.class);
    		System.out.println(Uri.withAppendedPath(PikoidItem.CONTENT_URI, uriAnnotedImage.getLastPathSegment()));
    		i.setData(Uri.withAppendedPath(PikoidItem.CONTENT_URI, uriAnnotedImage.getLastPathSegment()));
    		//System.out.println(i.getDataString());
    		startActivity(i);	
    	}
    	return false;
	}
	
    
    public void previousImage(){
    	int position = GalleryPictures.gallery.getSelectedItemPosition();
    	if(position > 0){
    		position--;    	    	
	  		GalleryPictures.gallery.setSelection(position);
	  		//ImageAdapterGallery iA = (ImageAdapterGallery) GalleryPictures.gallery.getAdapter();
	  		uriAnnotedImage = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, ""+GalleryPictures.gallery.getItemIdAtPosition(position)/*iA.imagesUriId.get(position)*/);
	    }
    }
    public void nextImage(){
    	int position = GalleryPictures.gallery.getSelectedItemPosition();
    	if(position < GalleryPictures.gallery.getCount()-1){
    	    position++;
			GalleryPictures.gallery.setSelection(position);
			//ImageAdapterGallery iA = (ImageAdapterGallery) GalleryPictures.gallery.getAdapter();
			uriAnnotedImage = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, ""+GalleryPictures.gallery.getItemIdAtPosition(position)/*iA.imagesUriId.get(position)*/);
    	}
    }
    
	

	@Override
	protected void onResume() {
		super.onResume();
		
		bWho.setVisibility(0);
		bWhen.setVisibility(0);
		bWhat.setVisibility(0);
		bWhere.setVisibility(0);
		bText.setVisibility(0);
		bPrevious.setVisibility(0);
		bNext.setVisibility(0);
			
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		
		bWho.setVisibility(4);//4 is a predefined constant which can hide the button.
		bWhen.setVisibility(4);
		bWhat.setVisibility(4);
		bWhere.setVisibility(4);
		bText.setVisibility(4);
		bPrevious.setVisibility(4);
		bNext.setVisibility(4);
	}
	
	
	/** called when an activity, called by this class, is finished **/
	public void onActivityResult(int reqCode, int resultCode, Intent data) {		
		if (resultCode == Activity.RESULT_OK){
			if (reqCode == ButtonListener.ACT_WHO)			showDialog(1);
			else if (reqCode == ButtonListener.ACT_WHEN)	showDialog(2);
			else if (reqCode == ButtonListener.ACT_WHAT)	showDialog(3);
			else if (reqCode == ButtonListener.ACT_WHERE)	showDialog(4);
			else if (reqCode == ButtonListener.ACT_TEXT)	showDialog(5);
		}		
	}
	
	
	/** display a pop up **/
	public Dialog onCreateDialog(int id) {
        switch (id) {
        	case 1:
        		return new AlertDialog.Builder(this)   
            		.setTitle(R.string.pikoid_popup_who)
            		.setIcon(R.drawable.icon_valid)
            		.setPositiveButton(R.string.general_ok, new DialogInterface.OnClickListener() {public void onClick(DialogInterface dialog, int whichButton) {}})
            		.create();
        		
        	case 2:
        		return new AlertDialog.Builder(this)   
            		.setTitle(R.string.pikoid_popup_when)
            		.setIcon(R.drawable.icon_valid)
            		.setPositiveButton(R.string.general_ok, new DialogInterface.OnClickListener() {public void onClick(DialogInterface dialog, int whichButton) {}})
            		.create();
        		
        	case 3:
        		return new AlertDialog.Builder(this)   
            		.setTitle(R.string.pikoid_popup_what)
            		.setIcon(R.drawable.icon_valid)
            		.setPositiveButton(R.string.general_ok, new DialogInterface.OnClickListener() {public void onClick(DialogInterface dialog, int whichButton) {}})
            		.create();
        		
        	case 4:
        		return new AlertDialog.Builder(this)   
            		.setTitle(R.string.pikoid_popup_where)
            		.setIcon(R.drawable.icon_valid)
            		.setPositiveButton(R.string.general_ok, new DialogInterface.OnClickListener() {public void onClick(DialogInterface dialog, int whichButton) {}})
            		.create();
        		
        	case 5:
        		return new AlertDialog.Builder(this)   
            		.setTitle(R.string.pikoid_popup_text)
            		.setIcon(R.drawable.icon_valid)
            		.setPositiveButton(R.string.general_ok, new DialogInterface.OnClickListener() {public void onClick(DialogInterface dialog, int whichButton) {}})
            		.create();
        }
        return null;
    }
}
