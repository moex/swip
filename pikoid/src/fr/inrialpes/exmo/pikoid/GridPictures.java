/**
 *   Copyright 2009-2011 Loic Martin, Yu Yang, Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   GridPictures.java is part of pikoid.
 *
 *   pikoid is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   pikoid is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with pikoid; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.pikoid;

import java.io.ByteArrayOutputStream;
import java.util.Vector;
import fr.inrialpes.exmo.pikoid.listeners.GridListener;
import fr.inrialpes.exmo.pikoid.listeners.GridSameAsListener;
import fr.inrialpes.exmo.pikoid.provider.PikoidItem;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class GridPictures extends Activity{
    
	private static final String TAG = "GridPictures";
	private static final int PROGRESS_DIALOG=1;
	private static final int NO_SDCARD=2;

	private PikoidImgAdapter imageAdapterGrid;
	
	// Gets called when a message is sent from the InitializationThread
    private Handler mainActivityHandler = new Handler() {
         public void handleMessage(Message msg) {
              if(msg.what==1) {
                   dismissDialog(PROGRESS_DIALOG);
                   displayGrid();
              }
         }
    }; 
	

    
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.grid);	
	
		
	}
    
    
	
	@Override
	protected void onResume() {
		super.onResume();
		String state = Environment.getExternalStorageState();
		if (!Environment.MEDIA_MOUNTED.equals(state)) {
			System.out.println(state);
			showDialog(NO_SDCARD);
		}
		
		showDialog(PROGRESS_DIALOG);
		InitializationThread t = new InitializationThread(this,mainActivityHandler);
		t.start();
	}



	/** display the pictures's grid on the screen	*/
    private void displayGrid(){
    	//initialization of the grid
		GridView g = (GridView) findViewById(R.id.myGrid);
		g.setAdapter(imageAdapterGrid);
		
		//listener for the grid used in the SameAs action
		if (getIntent().getBooleanExtra("fr.inrialpes.exmo.pikoid.SameAs", false)){
			GridSameAsListener listener = new GridSameAsListener(this);
			g.setOnItemClickListener(listener);
		}
		
		//listener for the grid of the begin of the application
		else{
			GridListener listener = new GridListener(this);
			g.setOnItemClickListener(listener);
		}
    }
    
	/** create a pop up for loading information */
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        	case PROGRESS_DIALOG:
        		ProgressDialog processDialog = new ProgressDialog(this);
        		processDialog.setMessage(this.getResources().getString(R.string.general_loading));
        		processDialog.setIndeterminate(true);
        		processDialog.setCancelable(true);
        		return processDialog;
        	case NO_SDCARD:
        		AlertDialog.Builder builder = new AlertDialog.Builder(this);
        		builder.setMessage("No sdcard found, Pikoid cannot load pictures")
        		       .setCancelable(false)
        		       .setNeutralButton("OK", new DialogInterface.OnClickListener() {
        		           public void onClick(DialogInterface dialog, int id) {
        		                GridPictures.this.finish();
        		           }
        		       });
        		return builder.create();
        }
        return null;

    } 
    
    /** Thread needed for initialize imageAdapterGrid during the display of the loading pop up*/
    public class InitializationThread extends Thread {

        private Context context;
        private Handler handler;

        public InitializationThread(Context ctx, Handler ha) {
        	context = ctx;
        	handler = ha;
        }

        @Override
        public void run() {
        	Display display = ((WindowManager) GridPictures.this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            imageAdapterGrid = new PikoidImgAdapter(context, new GridView.LayoutParams((display.getWidth()-20)/3,(display.getHeight()-60)/3) );  //  //new GridView.LayoutParams(display.getWidth()/3,display.getHeight()/3)
            Message msg = new Message();
            msg.what = 1;
            handler.sendMessage(msg);
        }
    }

    
    
}
