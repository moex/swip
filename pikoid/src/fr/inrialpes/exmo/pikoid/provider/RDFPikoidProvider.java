/**
 *   Copyright 2009-2011 Loic Martin, Yu Yang, Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   RDFPikoidProvider.java is part of pikoid.
 *
 *   pikoid is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   pikoid is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with pikoid; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.pikoid.provider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.inrialpes.exmo.rdfprovider.RdfContentProvider;
import fr.inrialpes.exmo.rdfprovider.RdfCursor;
import fr.inrialpes.exmo.rdfprovider.ArrayRdfAdapter;
import fr.inrialpes.exmo.rdfprovider.Statement;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Contacts.People;
import android.util.Log;


/**
 * To be removed, old class from YU
 * @author jerome
 *
 */
public class RDFPikoidProvider extends RdfContentProvider{ 

	public static final String AUTHORITY = "fr.inrialpes.exmo.pikoid";
	public static final Uri CONTENT_URI = Uri.parse("rdfcontent://"+ AUTHORITY +"/pikoid"); 
	
	private Uri uriPikoidItemColumn;
	private String predicate;;
	private RdfCursor r;
	private ArrayRdfAdapter rdfadapter3;
	private ContentResolver cr;
	private Cursor c;
	 
	
	public RdfCursor get(Uri arg0) {
		
		Uri uriPhoto = Uri.withAppendedPath(PikoidItem.CONTENT_URI, arg0.getLastPathSegment());
		rdfadapter3 = new ArrayRdfAdapter();
		String[] projection = new String[] {PikoidItem.URI_WHO, PikoidItem.ME, 
       										PikoidItem.URI_WHEN, PikoidItem.DATE, 
       										PikoidItem.URI_WHERE, PikoidItem.GPS_LATITUDE, 
       										PikoidItem.GPS_LONGITUDE, PikoidItem.ADRESS,
       										PikoidItem.CAPTION, PikoidItem.COMMENTS,
       										PikoidItem.TEXT }; 
		cr=getContext().getContentResolver();	
		c = cr.query(uriPhoto, projection, null, null, null);
		if (c.getCount() > 0) {
			c.moveToNext();
			if(!c.getString(0).equals("")){
				Statement stmt0 = new Statement(uriPhoto.toString(),c.getColumnName(0),c.getString(0));
				rdfadapter3.add(stmt0);
			}  
			if(!c.getString(1).equals("")){
				Statement stmt1 = new Statement(uriPhoto.toString(),c.getColumnName(1),c.getString(1));
				rdfadapter3.add(stmt1);
			} 
			if(!c.getString(2).equals("")){
				Statement stmt2 = new Statement(uriPhoto.toString(),c.getColumnName(2),c.getString(2));
				rdfadapter3.add(stmt2);
			} 
			if(!c.getString(3).equals("")){
				Statement stmt3 = new Statement(uriPhoto.toString(),c.getColumnName(3),c.getString(3));
				rdfadapter3.add(stmt3);
			}
			if(!c.getString(4).equals("")){
				Statement stmt4 = new Statement(uriPhoto.toString(),c.getColumnName(4),c.getString(4));
				rdfadapter3.add(stmt4);
			}
			if(!c.getString(5).equals("")){
				Statement stmt5 = new Statement(uriPhoto.toString(),c.getColumnName(5),c.getString(5));
				rdfadapter3.add(stmt5);
			}
			if(!c.getString(6).equals("")){
				Statement stmt6 = new Statement(uriPhoto.toString(),c.getColumnName(6),c.getString(6));
				rdfadapter3.add(stmt6);
			}
			if(!c.getString(7).equals("")){
				Statement stmt7 = new Statement(uriPhoto.toString(),c.getColumnName(7),c.getString(7));
				rdfadapter3.add(stmt7);
			}
			if(!c.getString(8).equals("")){
				Statement stmt8 = new Statement(uriPhoto.toString(),c.getColumnName(8),c.getString(8));
				rdfadapter3.add(stmt8);
			}
			if(!c.getString(9).equals("")){
				Statement stmt9 = new Statement(uriPhoto.toString(),c.getColumnName(9),c.getString(9));
				rdfadapter3.add(stmt9);
			}  
			if(!c.getString(10).equals("")){
				Statement stmt10 = new Statement(uriPhoto.toString(),c.getColumnName(10),c.getString(10));
				rdfadapter3.add(stmt10);
			}  
		}	
		return rdfadapter3; 
	}
	
	@Override
	public boolean onCreate() {
		return false;
	}

	@Override
	public int delete(Uri arg0, String arg1, String[] arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri arg0) {
		// TODO Auto-generated method stub
		return "toto";
	}

	@Override
	public Uri insert(Uri arg0, ContentValues arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Cursor query(Uri arg0, String[] arg1, String arg2, String[] arg3,
			String arg4) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int update(Uri arg0, ContentValues arg1, String arg2, String[] arg3) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Uri> getOntologies() {
		return Collections.singletonList(Uri.parse("pikoidonto"));
	}

	@Override
	public List<Uri> getQueryEntities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Uri> getTypes(Uri uri) {
		return Collections.singletonList(Uri.parse("pikoidobject"));
	}

	public List<Uri> getObjects(Uri uri) {
		String[] projectionEvent = new String[] {PikoidItem._ID};
		ContentResolver cr=getContext().getContentResolver();
		Cursor c = cr.query(PikoidItem.CONTENT_URI, projectionEvent, null, null, null);
		ArrayList<Uri> res = new ArrayList<Uri>(c.getCount());
		while (c.moveToNext()) {
			res.add(Uri.withAppendedPath(PikoidItem.CONTENT_URI, c.getString(0)));
		}
		return res;
	}


	

}
