/**
 *   Copyright 2009-2011 Loic Martin, Yu Yang, Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   PikoidProvider.java is part of pikoid.
 *
 *   pikoid is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   pikoid is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with pikoid; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.pikoid.provider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import fr.inrialpes.exmo.rdfprovider.ArrayRdfAdapter;
import fr.inrialpes.exmo.rdfprovider.RdfContentProvider;
import fr.inrialpes.exmo.rdfprovider.RdfCursor;
import fr.inrialpes.exmo.rdfprovider.Statement;
import android.app.Activity;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Contacts;
import android.text.TextUtils;
import android.util.Log;
 
public class PikoidProvider extends RdfContentProvider {
	
	/** Name of the database **/
	private static final String DATABASE_NAME="semanticannotation"; 
	/** Version of the database	**/
	private static final int DATABASE_VERSION = 5;

	/**	Name of the table **/
	private static final String PIKOID_TABLE_NAME="pikoid";
	
	/** Map used for the projection **/
    private static HashMap<String, String> sFeedsProjectionMap; 
	
    private static final int PIKOID = 1;
    private static final int PIKOID_ITEM = 2; 
    
    /** Manage database access **/
    private DatabaseHelper mOpenHelper;

	//private String[] cursorCols;

	//private MatrixCursor mc;

	private static class DatabaseHelper extends SQLiteOpenHelper {

		DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + PIKOID_TABLE_NAME + " ("
                    + PikoidItem._ID + " INTEGER PRIMARY KEY,"
                    + PikoidItem.URI_WHO + " TEXT,"
                    + PikoidItem.URI_WHEN + " TEXT,"
                    + PikoidItem.URI_WHERE + " TEXT,"
                    + PikoidItem.ME + " TEXT,"
                    + PikoidItem.DATE + " INTEGER,"
                    + PikoidItem.GPS_LATITUDE + " INTEGER,"
                    + PikoidItem.GPS_LONGITUDE + " INTEGER,"
                    + PikoidItem.ADRESS + " TEXT,"
                    + PikoidItem.CAPTION + " TEXT,"
                    + PikoidItem.COMMENTS + " TEXT,"
                    + PikoidItem.TEXT + " TEXT,"
                    + PikoidItem.THUMB + " BLOB"
                    + ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(DATABASE_NAME, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS "+PIKOID_TABLE_NAME);
            onCreate(db);
        }
	}
	

	@Override
	public boolean onCreate() {
		mOpenHelper = new DatabaseHelper(getContext());
//		qrdf=new QueryRDF(null);
		//cursorCols = new String[] { "Subject", "predicate", "object" };
		//mc = new MatrixCursor(cursorCols);
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		 SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		 qb.setProjectionMap(sFeedsProjectionMap);
//		 Log.e("URIPikoidProvider", uri.toString());// content://fr.inrialpes.exmo.pikoid.provider.semanticannotation/pikoid/4

	        switch (sUriMatcher.match(uri)) {
	        case PIKOID:
	            qb.setTables(PIKOID_TABLE_NAME);
	            break;
	            
	        case PIKOID_ITEM:
	            qb.setTables(PIKOID_TABLE_NAME);
	            qb.appendWhere(PikoidItem._ID + "=" + uri.getPathSegments().get(1));//uri.getPathSegments().get(1):4
	            
	            break;
	        
	        default:
	            throw new IllegalArgumentException("Unknown URI " + uri);
	        }

	        // If no sort order is specified use the default
	        String orderBy;
	        if (TextUtils.isEmpty(sortOrder)) {
	            orderBy = PikoidItem.DEFAULT_SORT_ORDER;
	        } else {
	            orderBy = sortOrder;
	        }
	        if(projection!= null || selection!= null || selectionArgs!= null || sortOrder!= null){
	        	
		     // Get the database and run the query
		        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, orderBy);
		        // Tell the cursor what uri to watch, so it knows when its source data changes
		        c.setNotificationUri(getContext().getContentResolver(), uri);
		        return c;
	        }else{
	        	// Get the database and run the query
		        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		        String[] projectionRDF = new String[] {PikoidItem.URI_WHO,PikoidItem.URI_WHEN,PikoidItem.GPS_LATITUDE,PikoidItem.GPS_LONGITUDE,PikoidItem.URI_WHERE,PikoidItem.CAPTION,PikoidItem.COMMENTS,};
		        Cursor c = qb.query(db, projectionRDF, null, null, null, null, null);
		        c.setNotificationUri(getContext().getContentResolver(), uri);
		        return c;
	}
	}
	
	public int delete(Uri uri, String where, String[] whereArgs) {
		SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		int count;
		switch (sUriMatcher.match(uri)) {
		case PIKOID_ITEM:
			Log.v(DATABASE_NAME, "delete "+uri);
			String itemId = uri.getPathSegments().get(1);
            count = db.delete(PIKOID_TABLE_NAME, PikoidItem._ID + "=" + itemId
                    + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
            break;
            
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
        return count;
	}

	@Override 
	public String getType(Uri uri) {
		switch (sUriMatcher.match(uri)) {
        case PIKOID:
            return PikoidItem.CONTENT_TYPE;

        case PIKOID_ITEM:
            return PikoidItem.CONTENT_ITEM_TYPE;
            
        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
	}

	@Override
	public Uri insert(Uri uri, ContentValues initialValues) {
		ContentValues values;
        if (initialValues != null) {
            values = new ContentValues(initialValues);
        } 
        else {
            values = new ContentValues();
        }

        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        
        Uri contentUri = null;
        long rowId=-1;
        switch (sUriMatcher.match(uri)) {
        case PIKOID:
        	rowId = db.insert(PIKOID_TABLE_NAME, PikoidItem.CAPTION, values);
        	contentUri = PikoidItem.CONTENT_URI;
        	break;
        	
        default:
        	throw new IllegalArgumentException("Unknown URI " + uri);	     		
        }

        if (rowId > 0) {
            Uri noteUri = ContentUris.withAppendedId(contentUri, rowId);
            getContext().getContentResolver().notifyChange(noteUri, null);
            return noteUri;
        }
 
        throw new SQLException("Failed to insert row into " + uri);
	}

	
	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count;
        String pikoidItemId;
        
        switch (sUriMatcher.match(uri)) {
        case PIKOID_ITEM:
        	pikoidItemId = uri.getPathSegments().get(1);
        	Cursor c = db.query(PIKOID_TABLE_NAME, null, "_id="+pikoidItemId, null, null, null, null);
        	if (!c.moveToFirst()) {
        		values.put(PikoidItem._ID, pikoidItemId);
        		db.insert(PIKOID_TABLE_NAME, "", values);
        		count=1;
        	}
        	else {
        		count = db.update(PIKOID_TABLE_NAME, values, PikoidItem._ID + "=" + pikoidItemId
                    + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
        		Log.e("PikoidItemId", pikoidItemId);
        	}
        	c.close();
            break;
        
        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
	}
	private static final UriMatcher sUriMatcher;
	static {
		
		// Matches between URI and attributes used to distinguish different types of URI
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(PikoidItem.AUTHORITY, "pikoid", PIKOID);
        sUriMatcher.addURI(PikoidItem.AUTHORITY, "pikoid/#", PIKOID_ITEM);
        

        // Matches to projected if one forgets, there may be exceptions with Invalid Column Description 
        sFeedsProjectionMap = new HashMap<String, String>();
        sFeedsProjectionMap.put(PikoidItem._ID, PikoidItem._ID);
        sFeedsProjectionMap.put(PikoidItem.URI_WHO, PikoidItem.URI_WHO);
        sFeedsProjectionMap.put(PikoidItem.URI_WHEN, PikoidItem.URI_WHEN);
        sFeedsProjectionMap.put(PikoidItem.URI_WHERE, PikoidItem.URI_WHERE);
        sFeedsProjectionMap.put(PikoidItem.ME, PikoidItem.ME);
        sFeedsProjectionMap.put(PikoidItem.DATE, PikoidItem.DATE);
        sFeedsProjectionMap.put(PikoidItem.GPS_LATITUDE, PikoidItem.GPS_LATITUDE);
        sFeedsProjectionMap.put(PikoidItem.GPS_LONGITUDE, PikoidItem.GPS_LONGITUDE);
        sFeedsProjectionMap.put(PikoidItem.ADRESS, PikoidItem.ADRESS);
        sFeedsProjectionMap.put(PikoidItem.CAPTION, PikoidItem.CAPTION);
        sFeedsProjectionMap.put(PikoidItem.COMMENTS, PikoidItem.COMMENTS);
        sFeedsProjectionMap.put(PikoidItem.TEXT, PikoidItem.TEXT);
        sFeedsProjectionMap.put(PikoidItem.THUMB, PikoidItem.THUMB);
        
    }
	
	
	
	// From old RDFPikoidProvider
	
	
	//private ArrayRdfAdapter rdfadapter3;
	private ContentResolver cr;
	private Cursor c;
	  
	
	public RdfCursor get(Uri uri) {
		Uri uriPhoto = Uri.withAppendedPath(PikoidItem.CONTENT_URI, uri.getLastPathSegment());
		ArrayRdfAdapter rdfadapter3 = new ArrayRdfAdapter();
		String[] projection = new String[] {PikoidItem.URI_WHO, PikoidItem.ME, 
       										PikoidItem.URI_WHEN, PikoidItem.DATE, 
       										PikoidItem.URI_WHERE, PikoidItem.GPS_LATITUDE, 
       										PikoidItem.GPS_LONGITUDE, PikoidItem.ADRESS,
       										PikoidItem.CAPTION, PikoidItem.COMMENTS,
       										PikoidItem.TEXT }; 
		//cr=getContext().getContentResolver();	
		c = query(uriPhoto, projection, null, null, null);
		if (c.getCount() > 0) {
			c.moveToNext();
			for (int i = 0; i < c.getColumnCount(); i++) {
				if (c.getString(i) != null && !"".equals(c.getString(i))) {

					Statement stmt0 = new Statement(uriPhoto.toString(),
							c.getColumnName(i), c.getString(i));
					rdfadapter3.add(stmt0);
				}
			}
		}	
		c.close();
		//System.out.println("hello :"+uri+" - "+rdfadapter3.hasNext() );
		return rdfadapter3; 
	}

	@Override
	public List<Uri> getOntologies() {
		return Collections.singletonList(Uri.parse("http://exmo.inrialpes.fr/pikoidonto"));
	}

	@Override
	public List<Uri> getQueryEntities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Uri> getTypes(Uri uri) {
		return Collections.singletonList(Uri.parse("http://exmo.inrialpes.fr/pikoidonto#pikoidpicture"));
	}

	public List<Uri> getObjects(Uri uri) {
		String[] projectionEvent = new String[] {PikoidItem._ID};
		ContentResolver cr=getContext().getContentResolver();
		Cursor c = cr.query(PikoidItem.CONTENT_URI, projectionEvent, null, null, null);
		ArrayList<Uri> res = new ArrayList<Uri>(c.getCount());
		while (c.moveToNext()) {
			res.add(Uri.withAppendedPath(PikoidItem.CONTENT_URI, c.getString(0)));
		}
		c.close();
		return res;
	}

}
