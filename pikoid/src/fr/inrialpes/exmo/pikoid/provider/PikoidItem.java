/**
 *   Copyright 2009-2011 Loic Martin, Yu Yang, Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   PikoidItem.java is part of pikoid.
 *
 *   pikoid is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   pikoid is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with pikoid; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.pikoid.provider;

import android.net.Uri;

public abstract class PikoidItem {
	
	public static final String AUTHORITY = "fr.inrialpes.exmo.pikoid";//provider.semanticannotation";
	
	/**	The content:// style URL for this table **/
	public static final Uri CONTENT_URI = Uri.parse("content://"+ AUTHORITY +"/pikoid");
	
	/** The MIME type of the directory of pikoid items **/
	public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.exmo.pikoid";

	/** The MIME type of a pikoid item **/
	public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.exmo.pikoid";
	
	/** The default sort order for this table **/
	public static final String DEFAULT_SORT_ORDER = "_id ASC";
	
	/**	Description of columns of the table pikoid **/
	public static final String _ID = "_id";
	public static final String URI_WHO = "uri_who";
	public static final String URI_WHEN = "uri_when";
	public static final String URI_WHERE = "uri_where";
	public static final String ME = "me";
	public static final String DATE = "date";
	public static final String GPS_LATITUDE = "gps_latitude";
	public static final String GPS_LONGITUDE = "gps_longitude";
	public static final String ADRESS = "adress";
	public static final String CAPTION = "caption";
	public static final String COMMENTS = "comments";
	public static final String TEXT = "text";
	public static final String THUMB = "thumb";
	
	
}
