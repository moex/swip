/**
 *   Copyright 2009-2011 Loic Martin, Yu Yang, Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   PikoidImgAdapter.java is part of pikoid.
 *
 *   pikoid is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   pikoid is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with pikoid; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.pikoid;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Thumbnails;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import fr.inrialpes.exmo.pikoid.provider.PikoidItem;

public class PikoidImgAdapter extends BaseAdapter {
	
	public static final String[] PROJECTION = new String[] {MediaStore.Images.Media._ID, MediaStore.Images.ImageColumns.TITLE};
	public static final String[] projectionPikoid = new String[] {PikoidItem._ID,PikoidItem.URI_WHO};

	private final Context mContext;
	private final LayoutParams layoutP;
	
	private Cursor picCursor;
	
	public PikoidImgAdapter(Context c, LayoutParams p) {
		mContext = c;        
		layoutP=p;
		//Get the uri id of images stored on the phone 
		//MediaStore.Images.Media.DATA+" LIKE '%DCIM%'"
        picCursor = mContext.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI ,PROJECTION,null,null,null);
	}
	
	public int getCount() {
		return picCursor.getCount();//imagesUriId.size();
	}

	public Object getItem(int position) {
		picCursor.move(position);
		return picCursor.getString(1);
	}

	public long getItemId(int position) {
		picCursor.move(position-picCursor.getPosition());
		return picCursor.getLong(0);
		//return imagesUriId.get(position);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
		if (convertView == null) {
			imageView = new ImageView(mContext);
			imageView.setLayoutParams(layoutP);
			imageView.setAdjustViewBounds(true);
			imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
			imageView.setPadding(5, 5, 5, 5);
		} 
		else 	imageView = (ImageView) convertView;
		
		Bitmap bm = Thumbnails.getThumbnail(mContext.getContentResolver(), getItemId(position), Thumbnails.MINI_KIND, null);
        imageView.setImageBitmap(bm);
		
        return imageView;
	}				

}
