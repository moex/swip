/**
 *   Copyright 2009-2011 Loic Martin, Yu Yang, Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   Who.java is part of pikoid.
 *
 *   pikoid is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   pikoid is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with pikoid; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.pikoid;

import fr.inrialpes.exmo.pikoid.listeners.ButtonListener;
import fr.inrialpes.exmo.pikoid.provider.PikoidItem;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Contacts.People;
import android.util.Log;
import android.widget.Button;

public class Who extends Activity {

	final static int SELECT_PERSON = 1;
	final static int NEW_PERSON = 2;
	final static int SAME_AS = 3;
	
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.who);
		
		//get the buttons
		Button bContactsbook = (Button) findViewById(R.id.ButtonContactsbook);
		Button bNewPerson = (Button) findViewById(R.id.ButtonNewPerson);
		Button bSameAs = (Button) findViewById(R.id.ButtonSameAsWho);
		Button bMe = (Button) findViewById(R.id.ButtonMe);
		
		//set listeners on buttons
		ButtonListener listener = new ButtonListener(this);
		bContactsbook.setOnClickListener(listener);
		bNewPerson.setOnClickListener(listener);
		bSameAs.setOnClickListener(listener);
		bMe.setOnClickListener(listener);
	
	}
	
	private boolean setWho(Uri who) {
		Uri picture=this.getIntent().getData();
		ContentValues cv = new ContentValues();
		cv.put(PikoidItem._ID, picture.getLastPathSegment());
		cv.put(PikoidItem.URI_WHO, who.toString());
		//getContentResolver().insert(PikoidItem.CONTENT_URI, cv);
		getContentResolver().update(Uri.withAppendedPath(PikoidItem.CONTENT_URI, this.getIntent().getData().getLastPathSegment()), cv, null, null);
		return true;
	}
	
	/** call the activity Contacts of the phone with the action "pick" **/
	public void contactsBook(){
		Intent i = new Intent();
		i.setAction(Intent.ACTION_PICK);
		i.setData(People.CONTENT_URI);
		Log.e("URI", People.CONTENT_URI.toString());
		//i.setAction(Intent.ACTION_PICK);
		startActivityForResult(i,SELECT_PERSON);
	}
	
	
	/** call the activity Contacts of the phone with the action "insert" **/
	public void newPerson(){
		Intent i = new Intent();
		i.setData(People.CONTENT_URI);
		i.setAction(Intent.ACTION_INSERT);
		startActivityForResult(i,NEW_PERSON);	
	}
	
	
	/** attribute the same information than an other image **/
	public void sameAs(){
		// put this extra information to distinct the grid's listener type in the GridListener class
		Intent i = new Intent();
		i.putExtra("fr.inrialpes.exmo.pikoid.SameAs", true);
		i.setClassName("fr.inrialpes.exmo.pikoid", "fr.inrialpes.exmo.pikoid.GridPictures");
		startActivityForResult(i,SAME_AS);
	}
	
	
	/** specify that the person on the image is me (the phone's user) **/
	public void me(){
		// save the "me" information in pikoid database
		ContentValues cv = new ContentValues();
		cv.put(PikoidItem.ME, this.getResources().getString(R.string.who_me));
		cv.put(PikoidItem.URI_WHO, "");
		getContentResolver().update(Uri.withAppendedPath(PikoidItem.CONTENT_URI, this.getIntent().getData().getLastPathSegment()), cv, null, null);
		
		
		// finish and return to Pikoid activity
		setResult(Activity.RESULT_OK);
		finish();
	}
	
	
	/** called when an activity, called by this class, is finished **/
	public void onActivityResult(int reqCode, int resultCode, Intent data) {
		switch (reqCode) {
			case (SELECT_PERSON) :
				if (resultCode == Activity.RESULT_OK) {
//					 save the contact's uri in pikoid database
					/*ContentValues cv = new ContentValues();
					cv.put(PikoidItem.URI_WHO, data.getDataString());
					Log.e("data.getDataString()", data.getDataString());// content://contacts/people/8
					cv.put(PikoidItem.ME, "");
					getContentResolver().update(Uri.withAppendedPath(PikoidItem.CONTENT_URI, this.getIntent().getData().getLastPathSegment()), cv, null, null);
					*/
					setWho(data.getData());
					// finish and return to Pikoid activity
					setResult(Activity.RESULT_OK);
					finish();
				}
				else{
					// nothing to do because the user canceled his action
				}
			break;
			
			case (NEW_PERSON) :
				if(resultCode==Activity.RESULT_OK){
					// PROBLEME SUR LE TELEPHONE MAIS PAS AVEC L'EMULATEUR 
					// PEUT ETRE CAR DIFFERENCES ENTRE APPLICATIONS ANDROID ET HTC...
					//=> le r�sultat retourn� (data) est null dans les deux cas (RESULT_OK ou RESULT_CANCELED)
					// donc faut faire une requete pour retrouver le dernier element ins�r� sinon ca va planter quand on va essayer d'exploiter la variable data
					// mais si on utilise la touche back pour annuler l'insertion ca fait entrer dans le cas RESULT_OK !!!?
					// et du coup ca assignerait le dernier contact alors que l'utilisateur voulait annuler son action !
					// donc ya deux solutions					
					
					
					//SOLUTION 1 : sauvegarder l'info si elle vient d'etre cr�� mais �a peut etre erron� si on utilise la touche back
					
					// force to sleep 1 second to be sure that the insert is finish
			    	try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
			    	
					// get the last contact inserted
					Cursor cursor = getContentResolver().query(People.CONTENT_URI,(new String[] { People._ID }), null, null, People._ID+" DESC");
					cursor.moveToFirst();
					
					// save the contact's uri in pikoid database
					ContentValues cv = new ContentValues();
					cv.put(PikoidItem.URI_WHO, People.CONTENT_URI +"/"+ cursor.getString(0));
					cv.put(PikoidItem.ME, "");
					getContentResolver().update(Uri.withAppendedPath(PikoidItem.CONTENT_URI, this.getIntent().getData().getLastPathSegment()), cv, null, null);
					
					
					
					
					//SOLUTION 2 : on sauvegarde pas, on fait que cr�er le contact l'avantage est qu'on risque pas de sauver une information fausse
					// mais du coup on reste dans l'activit� Who
					
				}
				else{
					// nothing to do because the user canceled his action
				}
			break;
			
			case (SAME_AS) :
				if (resultCode == Activity.RESULT_OK) {
					// get the information from the "same" image				
					Uri uriSameAs = Uri.withAppendedPath(PikoidItem.CONTENT_URI, data.getData().getLastPathSegment());
					String[] projection = new String[] { PikoidItem.URI_WHO, PikoidItem.ME };
					Cursor cursor = getContentResolver().query(uriSameAs,projection,null,null,null);
					cursor.moveToFirst();
					ContentValues cv = new ContentValues();
					
					// if the uri Who of the "same" image is setted 
					if (!cursor.getString(0).equals("")){
						cv.put(PikoidItem.URI_WHO, cursor.getString(0));
						cv.put(PikoidItem.ME, "");
					}
					else{
						cv.put(PikoidItem.ME, cursor.getString(1));
						cv.put(PikoidItem.URI_WHO, "");
					}
						
					getContentResolver().update(Uri.withAppendedPath(PikoidItem.CONTENT_URI, this.getIntent().getData().getLastPathSegment()), cv, null, null);
					
					cursor.close();
					
					// finish and return to Pikoid activity
					setResult(Activity.RESULT_OK);
					finish();
				}
				else{
					// nothing to do because the user canceled his action
				}
			break;
		}
	}
}
