/**
 *   Copyright 2009-2011 Loic Martin, Yu Yang, Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   Text.java is part of pikoid.
 *
 *   pikoid is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   pikoid is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with pikoid; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.pikoid;

import fr.inrialpes.exmo.pikoid.listeners.ButtonListener;
import fr.inrialpes.exmo.pikoid.provider.PikoidItem;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class Text extends Activity{
	
	final static int SAME_AS = 3;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.text);
		
		//get the buttons
		Button bSameAs = (Button) findViewById(R.id.ButtonSameAsText);
		Button bOk = (Button) findViewById(R.id.ButtonOKText);
		
		//set listeners on buttons
		ButtonListener listener = new ButtonListener(this);
		bSameAs.setOnClickListener(listener);
		bOk.setOnClickListener(listener);
		
		//prefill the edit text with the values in the database
		Uri uriPikoidItem = Uri.withAppendedPath(PikoidItem.CONTENT_URI, this.getIntent().getData().getLastPathSegment());
		String[] projection = new String[] { PikoidItem.TEXT };
		Cursor cursor = getContentResolver().query(uriPikoidItem,projection,null,null,null);
		EditText text = (EditText) findViewById(R.id.Text);
		if (cursor.moveToFirst()) {
			text.setText(cursor.getString(0));
		}
		else {
			text.setText("");
		}
	}
	
	
	/** attribute the same information than an other image **/
	public void sameAs(){
		// put this extra information to distinct the grid's listener type in the GridListener class
		Intent i = new Intent();
		i.putExtra("fr.inrialpes.exmo.pikoid.SameAs", true);
		i.setClassName("fr.inrialpes.exmo.pikoid", "fr.inrialpes.exmo.pikoid.GridPictures");
		startActivityForResult(i,SAME_AS);
	}
	
	
	/** save the text in the database **/
	public void ok(){
		EditText text = (EditText) findViewById(R.id.Text);
		ContentValues cv = new ContentValues();
		cv.put(PikoidItem.TEXT, text.getText().toString());
		getContentResolver().update(Uri.withAppendedPath(PikoidItem.CONTENT_URI, this.getIntent().getData().getLastPathSegment()), cv, null, null);
		
		// finish and return to Pikoid activity
		setResult(Activity.RESULT_OK);
		finish();
	}
	
    
	/** called when an activity, called by this class, is finished **/
	public void onActivityResult(int reqCode, int resultCode, Intent data) {
		switch (reqCode) {
			case (SAME_AS) :
				if (resultCode == Activity.RESULT_OK) {
					// get the information from the "same" image and save it			
					Uri uriSameAs = Uri.withAppendedPath(PikoidItem.CONTENT_URI, data.getData().getLastPathSegment());
					String[] projection = new String[] { PikoidItem.TEXT };
					Cursor cursor = getContentResolver().query(uriSameAs,projection,null,null,null);
					cursor.moveToFirst();
					ContentValues cv = new ContentValues();
					cv.put(PikoidItem.TEXT, cursor.getString(0));
					
					if (getContentResolver().update(Uri.withAppendedPath(PikoidItem.CONTENT_URI, this.getIntent().getData().getLastPathSegment()), cv, null, null)==0) {
						cv.put(PikoidItem._ID, this.getIntent().getData().getLastPathSegment());
						getContentResolver().insert(PikoidItem.CONTENT_URI, cv);
					}
					
					// finish and return to Pikoid activity
					setResult(Activity.RESULT_OK);
					finish();
				}
				else{
					// nothing to do because the user canceled his action
				}
			break;
		}
	}
}

