/**
 *   Copyright 2009-2011 Loic Martin, Yu Yang, Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   ImageProperties.java is part of pikoid.
 *
 *   pikoid is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   pikoid is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with pikoid; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.pikoid;

import java.util.Date;
import fr.inrialpes.exmo.pikoid.provider.PikoidItem;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Contacts.People;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageProperties extends Activity {

	 ImageView iconeContact;
	 ImageView iconeCalendar;
	 ImageView iconeMap;
	 
	 @Override
	 public void onCreate(Bundle savedInstanceState){
		 super.onCreate(savedInstanceState);
	     setContentView(R.layout.imageproperties);
	        
	        // get the views
	       	TextView infoWho = (TextView) findViewById(R.id.InfoWho);
	    	TextView infoWhen = (TextView) findViewById(R.id.InfoWhen);
	    	TextView infoWhere = (TextView) findViewById(R.id.InfoWhere);
	    	TextView infoWhatCaption = (TextView) findViewById(R.id.InfoWhatCaption);
	    	TextView infoWhatComments = (TextView) findViewById(R.id.InfoWhatComments);
	    	TextView infoText = (TextView) findViewById(R.id.InfoText);
	    	iconeContact = (ImageView) findViewById(R.id.IconeContact);
	    	iconeCalendar = (ImageView) findViewById(R.id.IconeCalendar);
	    	iconeMap = (ImageView) findViewById(R.id.IconeMap);

	    	// hide icons
	    	iconeContact.setVisibility(View.INVISIBLE);
	    	iconeCalendar.setVisibility(View.INVISIBLE);
	    	iconeMap.setVisibility(View.INVISIBLE);

	       	// query which get the informations
	    	String pikoidItemId = this.getIntent().getData().getLastPathSegment();//4
	       	Uri uriPikoidItem = Uri.withAppendedPath(PikoidItem.CONTENT_URI, pikoidItemId);// content://fr.inrialpes.exmo.pikoid.provider.semanticannotation/pikoid/4
	       	
	       	String[] projection = new String[] { 
	       			PikoidItem.URI_WHO, PikoidItem.ME, 
	       			PikoidItem.URI_WHEN, PikoidItem.DATE, 
	       			PikoidItem.URI_WHERE, PikoidItem.GPS_LATITUDE, PikoidItem.GPS_LONGITUDE, PikoidItem.ADRESS,
	       			PikoidItem.CAPTION, PikoidItem.COMMENTS,
	       			PikoidItem.TEXT };
	       	Cursor c = managedQuery(uriPikoidItem,projection,null,null,null);
	       
	       	if(c.getCount() > 0){
		       	c.moveToNext();
		       	
				//////////////////////////////////////
				//	Who information	                //
				//////////////////////////////////////
		       	//Log.e("ImageProperties:c.getString(0)", c.getString(0));			
		       	if(c.getString(0)!=null && !c.getString(0).equals("")){ // query to get the contact's name from his uri, c.getString(0):" content://contacts/people/6"

		       		final Uri uriContact = Uri.parse(c.getString(0));
		       	   	Log.e("uriContact", uriContact.toString());	//uriContact:content://contacts/people/6
		       		String[] projectionContact = new String[] { People.NAME };
			       	Cursor cc = managedQuery(uriContact,projectionContact,null,null,null);
			       	if(cc.moveToFirst()){//Julie	
				       	if(cc.getString(0)!= null){
				       		infoWho.setTypeface(Typeface.DEFAULT,Typeface.BOLD);
				       		infoWho.setTextSize(25);
				       		infoWho.setText(cc.getString(0));
				       		infoWho.setOnClickListener(new OnClickListener(){
				       			public void onClick(View arg0) {
				       				Intent i = new Intent();
				       				i.setData(uriContact);
				       				i.setAction(Intent.ACTION_VIEW);
				       				startActivity(i);
				       			}
				       		});
				       		// show the icon
				       		iconeContact.setVisibility(View.VISIBLE);
				       	}
			       	}
		       	}
		       	else if(c.getString(1)!=null &&  !c.getString(1).equals("")){
		       		// get the "me" information
		       		infoWho.setTypeface(Typeface.DEFAULT,Typeface.BOLD);
		       		infoWho.setTextSize(25);
		       		infoWho.setText(this.getResources().getText(R.string.who_me));
		       	}
		       	//////////////////////////////////////
				//	When information	            //
				//////////////////////////////////////

		       	if(c.getString(2)!=null &&  c.getString(2)!=null && !c.getString(2).equals("")){
			       	// query to get the event's title from his uri
		       		Log.e("URI-EVENT", c.getString(2));//content://calendar/events/6
		       		final Uri uriEvent = Uri.parse(c.getString(2));
		       		String[] projectionEvent = new String[] { "title", "dtstart", "dtend"};
			       	Cursor cc = managedQuery(uriEvent,projectionEvent,null,null,null);
			       	if(cc.moveToFirst()){
				       	if(cc.getString(0)!= null)	{
				       		infoWhen.setTextSize(25);
				       		infoWhen.setTypeface(Typeface.DEFAULT,Typeface.BOLD);
				       		infoWhen.setText(cc.getString(0));
				       		final long dtstart = cc.getLong(1);
					    	final long dtend = cc.getLong(2);
				       		infoWhen.setOnClickListener(new OnClickListener(){
				       			public void onClick(View arg0) {
				       				Intent i = new Intent();
				       				i.setData(uriEvent);
				       				i.setAction(Intent.ACTION_VIEW);
				       				// extra informations needed by Calendar application
				       				i.putExtra("beginTime", dtstart);
				       				i.putExtra("endTime", dtend);
				       				startActivity(i);
				       			}
				       		});
				       		// show the icon
				       		iconeCalendar.setVisibility(View.VISIBLE);
				       	}
			       	}
		       	}
		       	else if(c.getLong(3) != 0){
		       		// get the "now" information
		       		Date d = new Date(c.getLong(3));
		       		infoWhen.setTextSize(25);
		       		infoWhen.setTypeface(Typeface.DEFAULT,Typeface.BOLD);
		       		infoWhen.setText(d.toLocaleString());
		       	}
		       	//////////////////////////////////////
				//	Where information	                //
				//////////////////////////////////////

		       	if(c.getString(4)!=null && !c.getString(4).equals("")){
		       		// query to get the place adress from his uri
		    		Log.e("ImagePropertie-URI_WHERE", c.getString(4));
		       	}
		       	else if(c.getDouble(5) > 0 && c.getDouble(6) > 0 && !c.getString(7).equals("")){
		       		Log.e("ImagePropertie-adresse", c.getString(7));
		       		// get the adress from the database
		       		final double latitude = c.getDouble(5);
					final double longitude = c.getDouble(6);
		       		final String adress = c.getString(7);
					infoWhere.setTextSize(25);
			       	infoWhere.setTypeface(Typeface.DEFAULT,Typeface.BOLD);
			       	infoWhere.setText(adress);
			       	infoWhere.setOnClickListener(new OnClickListener(){
			       		public void onClick(View arg0) {
			       			String uri = "geo:"+ latitude + "," + longitude +"?q="+adress;                                                                                                
			       			Log.e("URI-Addresse", uri);
			       			Intent i = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
			       			startActivity(i);
			       		}
			       	});
			       	// show the icon
		       		iconeMap.setVisibility(View.VISIBLE);
			   }
		       	//////////////////////////////////////
				//	What information	                //
				//////////////////////////////////////
		       	
			    if(c.getString(8)!=null && !c.getString(8).equals("")){
			    	infoWhatCaption.setTextSize(25);
			    	infoWhatCaption.setTypeface(Typeface.DEFAULT,Typeface.BOLD);
			    	infoWhatCaption.setText(c.getString(8));
			    }
			    
			    if(c.getString(9)!=null && !c.getString(9).equals("")){
			    	infoWhatComments.setTextSize(20);
			    	infoWhatComments.setText(c.getString(9));
			    	infoWhatComments.setTypeface(Typeface.DEFAULT,Typeface.BOLD);
			    }
			    
			    //////////////////////////////////////
				//	TEXT information	                //
				//////////////////////////////////////
		       
			    if(c.getString(10)!=null && !c.getString(10).equals("")){
			    	infoText.setTextSize(20);
			    	infoText.setText(c.getString(10));
			    	infoText.setTypeface(Typeface.DEFAULT,Typeface.BOLD);
			    }
	       	}
	       	
	 }
		
}

