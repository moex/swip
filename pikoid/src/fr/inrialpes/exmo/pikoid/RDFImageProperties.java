/**
 *   Copyright 2009-2011 Loic Martin, Yu Yang, Jérôme David, Jérôme Euzenat, Université Pierre Mendès France, INRIA
 *   
 *   RDFImageProperties.java is part of pikoid.
 *
 *   pikoid is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   pikoid is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with pikoid; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.pikoid;

import fr.inrialpes.exmo.pikoid.provider.PikoidItem;
import fr.inrialpes.exmo.rdfprovider.RdfCursor;
import fr.inrialpes.exmo.rdfprovider.RdfContentResolverProxy;
import fr.inrialpes.exmo.rdfprovider.Statement;
import android.app.Activity;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Old class from Yu to be removed ???
 * @author jerome
 *
 */
public class RDFImageProperties extends Activity {

	private Statement s;
	private RdfContentResolverProxy rdfResolver;
	private RdfCursor r2;
	private RdfCursor r1;
	private Statement s1;
	private RdfCursor r;
	private RdfCursor r3;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rdfimageproperties);

		LinearLayout contractRDFContainer = (LinearLayout)findViewById(R.id.LinearLayoutLayoutContract);
		LinearLayout pikoidRDFContainer = (LinearLayout)findViewById(R.id.LinearLayoutLayoutpikoid);
		LinearLayout mapRDFContainer=(LinearLayout) findViewById(R.id.LinearLayoutLayoutMap);
		LinearLayout agendaRDFContainer=(LinearLayout)findViewById(R.id.LinearLayoutLayoutAgenda);
		
		String pikoidItemId = this.getIntent().getData().getLastPathSegment();//4
		Uri uriPikoidItem = Uri.withAppendedPath(PikoidItem.CONTENT_URI, pikoidItemId);// content://fr.inrialpes.exmo.pikoid.provider.semanticannotation/pikoid/4
		
		String[] projection = new String[] { PikoidItem.URI_WHO,PikoidItem.URI_WHEN,PikoidItem.URI_WHERE, PikoidItem.GPS_LATITUDE, PikoidItem.GPS_LONGITUDE, PikoidItem.ADRESS,};
       	Cursor c = managedQuery(uriPikoidItem,projection,null,null,null);
       	
       	rdfResolver = new RdfContentResolverProxy(this);
       	
       	Uri uriPikoid=Uri.parse("rdfcontent://fr.inrialpes.exmo.pikoid/pikoidRDFProvider/"+ pikoidItemId);
		r2=rdfResolver.get(uriPikoid);
		while (r2!=null && r2.hasNext()) {
			s = (Statement) r2.next();
			TextView tv1 = new TextView(this);
			tv1.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
			tv1.setTextSize(15);
			tv1.setText(s.toString());
			pikoidRDFContainer.addView(tv1);
		}
		
		Uri uriMapProvider=Uri.parse("rdfcontent://fr.inria.exmo.rdf.map/mapRDFProvider/"+1);
		r3=rdfResolver.get(uriMapProvider);
		while(r3!=null && r3.hasNext()){
			Statement s3 = r3.next();
			TextView tv3 = new TextView(this);
			tv3.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
			tv3.setTextSize(15);
			tv3.setText(s3.toString());
			mapRDFContainer.addView(tv3);
		}
		
		
       	if(c.getCount() > 0){
	       	c.moveToNext();
			if (!c.getString(0).equals("")) { // query to get the contact's name from his uri,c.getString(0):" content://contacts/people/6"
				
				Uri uriContact = Uri.parse(c.getString(0));
				String idPerson = uriContact.getLastPathSegment();
				Uri uriContract = Uri.parse("rdfcontent://fr.inria.exmo.rdf.contract/contractRDFProvider/"+ idPerson);		
				
				r1 = rdfResolver.get(uriContract);
				while (r1!=null && r1.hasNext()) {
					s1 = (Statement) r1.next();
					TextView tv = new TextView(this);
					tv.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
					tv.setTextSize(15);
					tv.setText(s1.toString());
					contractRDFContainer.addView(tv);
				}
			}
			
			if(!c.getString(1).equals("")){
				Uri uriEvent=Uri.parse(c.getString(1));
				String idEvent=uriEvent.getLastPathSegment();
				Uri uriRdfAgendapro=Uri.parse("rdfcontent://fr.inria.exmo.rdf.agenda/agendaRDFProvider/"+idEvent);
				
				r = rdfResolver.get(uriRdfAgendapro);
				while (r!=null && r.hasNext()) {
					Statement s2 = r.next();
					TextView tv4 = new TextView(this);
					tv4.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
					tv4.setTextSize(15);
					tv4.setText(s2.toString());
					agendaRDFContainer.addView(tv4);
				}				
			}
			
       }
   }
}
